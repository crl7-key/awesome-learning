#include <iostream>
#include "message_test.pb.h"
#include <ctime>
#include <time.h>
#include <unistd.h>

using namespace opt;


int main()
{
    int iRecord = 0;
    opt::rsp_msg rsp;
    rsp.set_iret(opt::rsp_msg_RET_SUCCESS);

    auto user_info = rsp.mutable_info();

    user_info->set_uid(1000);

    user_info->set_nickname("test");

    user_info->set_phone(12345678910);

    user_info->set_location("sz");

    for(int i = 0; i < 10; i++) {
        auto record = rsp.add_record();
        record->set_pay(++iRecord);
        time_t now = time(0);
        record->set_paytime(std::string(ctime(&now)));
        usleep(10000);
    }

    // 序列化消息
    std::string buf;
    rsp.SerializeToString(&buf);
    std::cout << "buf is " << buf << std::endl;

    std::cout << "Serialized successfully " << std::endl;

    // 解析消息
    opt::rsp_msg rsp2;

    if (!rsp2.ParseFromString(buf)) {
        std::cout << "parse error" << std::endl;
    }

    auto tmp_user_info = rsp2.info();

    std::cout << "info nickname is " << tmp_user_info.nickname() << std::endl;

    std::cout << "info phone is " << tmp_user_info.phone() << std::endl;

    std::cout << "info uid is " << tmp_user_info.uid() << std::endl;

    std::cout << "info location is " << tmp_user_info.location() << std::endl;

    for(int i = 0; i < 10; i ++) {
        auto record = rsp2.record(i);
        std::cout << "pay : " <<  record.pay() << std::endl;
        std::cout << "paytime " << record.paytime() << std::endl;
    }
    return 0;
}

