#!/bin/bash
protoc --cpp_out=. ./message_test.proto

g++ -o main main.cpp ./message_test.pb.cc -lprotobuf -lpthread

./main
