#  cpp中使用google/protobuf

### 新建一个proto文件
新建一个`proto`文件，命名为`message_test.proto`，并键入内容:
```proto
syntax = "proto3";          // 指定版本信息，不指定会报错

package opt;                // package声明符

option optimize_for = LITE_RUNTIME;

message obj_user_info       // message为关键字，作用为定义一种消息类型

{
    int32 uid        = 1;   // 用户id
    string nickname  = 2;   // 昵称
    int64 phone      = 3;   // 手机号码
    string location  = 4;   // 所属地
}

message obj_user_record
{
    string paytime = 1;     // 支付时间
    uint32 pay     = 2;     // 支付数
}

message rsp_msg
{
    enum RET                    // 枚举消息类型
    {
        SUCCESS        = 0;     // proto3版本中，首成员必须为0，成员不应有相同的值

        ACCOUNT_NULL   = 1;     //账号不存在

        ACCOUNT_LOCK   = 2;     //账号锁定

        PASSWORD_ERROR = 3;     //密码错误

        ERROR          = 4;
    }

    RET iRet                = 1;

    obj_user_info info      = 2;

    repeated  obj_user_record record  = 3; // repeated 数组
}
```
### 运行`protoc`命令:
```sh
protoc --cpp_out=. message_test.proto
```
运行之后将生成2个文件`message_test.pb.cc`和`message_test.pb.h`

### cpp-protobuf应用
将生成的`message_test.pb.h`包含在需要用的文件中
```cpp
#include <iostream>
#include "message_test.pb.h"
#include <ctime>
#include <time.h>
#include <unistd.h>

using namespace opt;


int main()
{
    int iRecord = 0;
    opt::rsp_msg rsp;
    rsp.set_iret(opt::rsp_msg_RET_SUCCESS);

    auto user_info = rsp.mutable_info();

    user_info->set_uid(1000);

    user_info->set_nickname("test");

    user_info->set_phone(12345678910);

    user_info->set_location("sz");

    for(int i = 0; i < 10; i++) {
        auto record = rsp.add_record();
        record->set_pay(++iRecord);
        time_t now = time(0);
        record->set_paytime(std::string(ctime(&now)));
        usleep(10000);
    }

    // 序列化消息
    std::string buf;
    rsp.SerializeToString(&buf);
    std::cout << "buf is " << buf << std::endl;

    std::cout << "Serialized successfully " << std::endl;

    // 解析消息
    opt::rsp_msg rsp2;

    if (!rsp2.ParseFromString(buf)) {
        std::cout << "parse error" << std::endl;
    }

    auto tmp_user_info = rsp2.info();

    std::cout << "info nickname is " << tmp_user_info.nickname() << std::endl;

    std::cout << "info phone is " << tmp_user_info.phone() << std::endl;

    std::cout << "info uid is " << tmp_user_info.uid() << std::endl;

    std::cout << "info location is " << tmp_user_info.location() << std::endl;

    for(int i = 0; i < 10; i ++) {
        auto record = rsp2.record(i);
        std::cout << "pay : " <<  record.pay() << std::endl;
        std::cout << "paytime " << record.paytime() << std::endl;
    }
    return 0;
}
```
执行编译运行命令：
```sh
g++ -o main main.cpp ./message_test.pb.cc -lprotobuf -lpthread 
./main
```
