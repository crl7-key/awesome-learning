## 模板的右尖括号
`C++11`之前是不允许两个右尖括号出现的，会被认为是右移操作符，所以需要中间加个空格进行分割，避免发生编译错误。
```cpp
int main() {
   std::vector<std::vector<int>> a; // error
   std::vector<std::vector<int> > b; // ok
}
```

## 模板的别名
`C++11`引入了`using`，可以轻松的定义别名，而不是使用繁琐的`typedef`。
```cpp

typedef std::vector<std::vector<int>> vvi; // before c++11
using vvi = std::vector<std::vector<int>>; // c++11
```

```cpp
typedef void (*func)(int, int);   //  before c++ 11
using func = void (*)(int, int); // 起码比typedef容易看懂
```
## 函数模板的默认模板参数
`C++11`之前只有类模板支持默认模板参数，函数模板是不支持默认模板参数的，`C++11`后都支持。
```cpp
template <typename T, typename U=int>
class Base {
    T value;  
};

template <typename T=int, typename U> // error
class Base {
    T value;  
};
```
类模板的默认模板参数必须从右往左定义，而函数模板则没有这个限制。
```cpp
template <typename T, typename U=int>
T func1(U val) {
   return val;
}

template <typename T=int, typename U>
T func2(U val) {
   return val;
}

int main() {
   cout << func1<int, double>(99.9) << endl;      // 99
   cout << func1<double, double>(99.9) << endl;   // 99.9
   cout << func1<double>(99.9) << endl;           // 99.9
   cout << func1<int>(99.9) << endl;              // 99
   cout << func2<int, double>(99.9) << endl;      // 99
   cout << func1<double, double>(99.9) << endl;   // 99.9
   cout << func2<double>(99.9) << endl;           // 99.9
   cout << func2<int>(99.9) << endl;              // 99
   return 0;
}
```
对于函数模板，参数的填充顺序是从左到右的。
