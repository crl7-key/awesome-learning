## std::mutex 
`std::mutex`是一种线程同步的手段，用于保存多线程同时操作的共享数据。

`mutex`分为四种：

- `std::mutex`：独占的互斥量，不能递归使用，不带超时功能

- `std::recursive_mutex`：递归互斥量，可重入，不带超时功能

- `std::timed_mutex`：带超时的互斥量，不能递归

- `std::recursive_timed_mutex`：带超时的互斥量，可以递归使用


## std::mutex的使用
```cpp
#include <iostream>
#include <mutex>
#include <thread>
std::mutex g_mutex;

int main()
{
    auto func = [](int n) {
        g_mutex.lock(); // 加锁
        for (int i = 0; i < n; ++i) {
            std::cout << i << " ";
        }
        std::cout << std::endl;
        g_mutex.unlock();
    };

    std::thread threads[5];
    for (int i = 0; i < 5; ++i) {
        threads[i] = std::thread(func,100);
    }

    for (auto &t : threads) {
        t.join();
    }
    return 0;
}
```
## std::timed_mutex的使用
在规定的等待时间内，没有获取锁，线程不会一直阻塞，代码会继续执行
```cpp
#include <iostream>
#include <mutex>
#include <thread>
#include <chrono>
std::timed_mutex g_mutex;

int main()
{
    auto func = [](int k) {
        g_mutex.try_lock_for(std::chrono::microseconds(30)); //30毫秒
        for (int i = 0; i < k ;++i) {
            std::cout << i << " ";
        }
        std::cout << std::endl;
        g_mutex.unlock();
    };

    std::thread threads[5];
    for (int i = 0; i < 5; ++i) {
        threads[i] = std::thread(func, 100);
    }

    for (auto& t : threads) {
        t.join();
    }

    return 0;
}
```
其他几种都是类似的使用方式
