## std::future

`c++11`关于异步操作提供了`future`相关的类，主要有`std::future`、`std::promise`和`std::packaged_task`，`std::future`比`std::thread`高级些，`std::future`作为异步结果的传输通道，通过`get()`可以很方便的获取线程函数的返回值，`std::promise`用来包装一个值，将数据和`future`绑定起来，而`std::packaged_task`则用来包装一个调用对象，将函数和`future`绑定起来，方便异步调用。而`std::future`是不可以复制的，如果需要复制放到容器中可以使用`std::shared_future`。


## `std::promise`与`std::future`配合使用
```cpp
#include <iostream>
#include <future>
#include <functional>
#include <thread>

void func(std::future<int>& f) {
    int x = f.get(); // 获取线程函数的返回值
    std::cout << "value is : " << x << std::endl;
}

int main()
{
    std::promise<int> pr;
    std::future<int> f = pr.get_future();
    std::thread t(func,std::ref(f));
    pr.set_value(10);
    t.join();
    return 0;
}
```
## `std::packaged_task`与`std::future`配合使用
```cpp

#include <functional>
#include <future>
#include <iostream>
#include <thread>

int func(int n) {
    return n + 1;
}

int main() {
    std::packaged_task<int(int)> task(func);
    std::future<int> f = task.get_future();
    std::thread(std::move(task), 5).detach();
    std::cout << "result " << f.get() << std::endl;
    return 0;
}
```
## 三者直接的联系
`std::future`用于访问异步操作的结果，而`std::promise`和`std::packaged_task`在`future`高一层，它们内部都有一个`future`，`promise`包装的是一个值，`packaged_task`包装的是一个函数，当需要获取线程中的某个值，可以使用`std::promise`，当需要获取线程函数返回值，可以使用`std::packaged_task`
