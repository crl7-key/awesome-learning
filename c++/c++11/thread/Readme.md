## `std::thread`
`c++11`之前可能使用`pthread_xxxx`来创建线程和处理线程，繁琐且不易读，`c++11`引入了`std::thread`来创建线程，支持对线程`join`或者`detach`
```cpp
#include <iostream>
#include <thread>

int main()
{
    auto func = []() {
        for (int i = 0; i < 10; ++i) {
            std::cout << i << " ";
        }
        std::cout << std::endl;
    };

    std::thread t(func);

    if (t.joinable()) {
        t.detach();
    }
    
    auto func2 = [](int k) {
        for (int i = 0; i < k; ++i) {
            std::cout << i << " ";
        }  
        std::cout << std::endl;
    };

    std::thread t2(func2,20);
    if (t2.joinable()) {
        t2.join();
    }
    return 0;
}
```
上述代码中，函数`func`和`func2`运行在线程对象`t`和`t2`中，从刚创建对象开始就会新建一个线程用于执行函数，调用`join`函数将会阻塞主线程，直到线程函数执行结束，线程函数的返回值将会被忽略。如果不希望线程被阻塞执行，可以调用线程对象的`detach`函数，表示将线程和线程对象分离。


如果没有调用`join`或者`detach`函数，假如线程函数执行时间较长，此时线程对象的生命周期结束调用析构函数清理资源，这时可能会发生错误，这里有两种解决办法，一个是调用`join()`，保证线程函数的生命周期和线程对象的生命周期相同，另一个是调用`detach()`，将线程和线程对象分离，这里需要注意，如果线程已经和对象分离，那我们就再也无法控制线程什么时候结束了，不能再通过`join`来等待线程执行完。

可以对`thread`进行封装，避免没有调用`join`或者`detach`可导致程序出错的情况出现：
```cpp
#include <iostream>
#include <thread>

class ThreadGuard {
public:
    enum class DesAction{ join,detach };

    ThreadGuard(std::thread &&t,DesAction action) : m_action(action), m_threadObj(std::move(t)) {

    }

    ~ThreadGuard() {
        if (m_threadObj.joinable()) {
            if (m_action == DesAction::detach) {
                m_threadObj.detach();
            }else {
                m_threadObj.join();
            }
        }
    }

    ThreadGuard(ThreadGuard&&) = default;
    ThreadGuard& operator=(ThreadGuard&&) = default;

    std::thread& get() {
        return m_threadObj;
    }
private:
    DesAction m_action;
    std::thread m_threadObj;
};

int main()
{
    auto func = []() {
        for(int i = 0; i < 10; ++i) {
            std::cout << "thread guard " << i << " ";
        }
        std::cout << std::endl;
    };
    ThreadGuard obj(std::thread(func),ThreadGuard::DesAction::join);

    return 0;
}
```

`c++11`还提供了获取线程`id`，或者系统`cpu`个数，获取`thread native_handle`，使得线程休眠等功能
```cpp

std::thread t(func);
std::cout << "当前线程ID " << t.get_id() << std::endl;
std::cout << "当前cpu个数 " << std::thread::hardware_concurrency() << std::endl;
auto handle = t.native_handle();                      // handle可用于pthread相关操作
std::this_thread::sleep_for(std::chrono::seconds(1)); // 线程休眠
```


