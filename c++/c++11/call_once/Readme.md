## std::call_once

`c++11`提供了`std::call_once`来保证某一函数在多线程环境中只调用一次，它需要配合`std::once_flag`使用

```cpp
#include <iostream>
#include <thread>
#include <mutex>

std::once_flag once_flag1;

void CallOnce() {
    std::call_once(once_flag1,[](){
                   std::cout << "call once" << std::endl;
                   });
}

int main()
{
    std::thread threads[5];
    for(int i = 0; i < 5; ++i) {
        threads[i] = std::thread(CallOnce);
    }

    for(auto &t : threads) {
        t.join();
    }
    return 0;
}
```
执行编译运行
```
g++ call_once_test.cpp -lpthread -o call_once_test
./call_once_test
```
输出
```
call once
```
