## std::lock_guard
`RAII`方式的锁封装，可以动态的释放锁资源，防止线程由于编码失误导致一直持有锁。

```cpp
#include <iostream>
#include <mutex>
#include <thread>

std::mutex g_mutex;

int main()
{
    auto func = [](int n) {
        // std::unique_lock<std::mutex> lock(g_mutex);
        std::lock_guard<std::mutex> lock(g_mutex);
        for (int i = 0; i < n; ++i) {
            std::cout << i << " ";
        }
        std::cout << std::endl;
        // lock.unlock();
    };

    std::thread threads[5];
    for (int i = 0; i < 5; ++i) {
        threads[i] = std::thread(func,100);
    }

    for (auto &t : threads) {
        t.join();
    }

    return 0;
}
```
`std::unique_lock`使用方式和`std::lock_guard`使用方式类似

`std::lock_gurad`相比于`std::unique_lock`更加轻量级，少了一些成员函数，`std::unique_lock`类有`unlock`函数，可以手动释放锁，所以条件变量都配合`std::unique_lock`使用，而不是`std::lock_guard`，因为条件变量在`wait`时需要有手动释放锁的能力


