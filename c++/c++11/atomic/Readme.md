## std::atomic
`c++11`提供了原子类型`std::atomic<T>`,这个`T`可以是任意类型,，有这种原子变量已经足够方便，就不需要使用`std::mutex`来保护该变量了
```cpp
class OriginCounter { // 普通的计数器
public:
    void add() {
        std::lock_guard<std::mutex> lock(m_mutex);
        ++m_count;
    }

    void sub() {
        std::lock_guard<std::mutex> lock(m_mutex);
        --m_count;
    }

    int get() {
        std::lock_guard<std::mutex> lock(m_mutex);
        return m_count;
    }

private:
    int m_count;
    std::mutex m_mutex;
};
```

使用原子操作的计数器
```cpp
class AtomicCounter {
public:
    void add() {
        ++m_count;
    }

    void sub() {
        --m_count;
    }

    int get() {
        return m_count.load();
    }

private:
    std::atomic<int> m_count;
};
```
