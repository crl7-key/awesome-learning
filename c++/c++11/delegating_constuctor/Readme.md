## 委托构造函数
委托构造函数允许在同一个类中一个构造函数调用另外一个构造函数，可以在变量初始化时简化操作

不使用委托构造函数：
```cpp
class Base {
public:
   Base(){}
   
   Base(int a) { m_a = a; }

   Base(int a, int b) {         // 重新赋值,好麻烦
       m_a = a;
       m_b = b;
  }

   A(int a, int b, int c) {   // 重新赋值,好麻烦
       m_a = a;
       m_b = b;
       m_c = c;
  }

   int m_a;
   int m_b;
   int m_c;
};
```
使用委托构造函数：
```cpp
class Base {
   Base(){}
   
   Base(int a) { m_a = a; }

   Base(int a, int b) : Base(a) { m_b = b; }

   Base(int a, int b, int c) : Base(a, b) { m_c = c; }

   int m_a;
   int m_b;
   int m_c;
};
```
