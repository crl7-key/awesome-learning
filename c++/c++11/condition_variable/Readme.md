## condition_variable

条件变量是`c++11`引入的一种同步机制，它可以阻塞一个线程或者个线程，直到有线程通知或者超时才会唤醒正在阻塞的线程，条件变量需要和锁配合使用，这里的锁就是`std::unique_lock`。

```cpp
#include <iostream>
#include <mutex>
#include <thread>
#include <condition_variable>
#include <chrono>

class CountDownLatch {
public:
    explicit CountDownLatch(u_int32_t count) : m_count(count) {
    }

    void CountDown() {
        std::unique_lock<std::mutex> lock(m_mutex);
        --m_count;
        if (0 == m_count) {
            m_cv.notify_all();
        }
    }

    void wait(uint32_t time_ms = 0) {
        std::unique_lock<std::mutex> lock(m_mutex);
        while(m_count > 0) {
            if (time_ms > 0) {
                m_cv.wait_for(lock,std::chrono::microseconds(time_ms));
            } else {
                m_cv.wait(lock);
            }
        }
    }

    uint32_t getCount() const {
        std::unique_lock<std::mutex> lock(m_mutex);
        return m_count;
    }

private:
    std::condition_variable m_cv;
    mutable std::mutex m_mutex;
    uint32_t m_count = 0;
};
```
