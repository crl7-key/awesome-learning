# decltype

`decltype`主要用于推导表达式类型，这里只用于编译器分析表达式的类型，表达式实际不会进行运算
```cpp
int func() { return 0; }
decltype(func()) i;  // i为int类型

int x = 0;
decltype(x) y;       // y是int类型
decltype(x + y) z;   // z是int类型
```

注意：`decltype`不会像`auto`一样忽略引用和`cv`属性，`decltype`会保留表达式的引用和`cv`属性
```cpp
cont int &i = 1;
int a = 2;
decltype(i) b = 2;   // b是const int&
```

## decltype推导规则

对于`decltype(exp)`有

- `exp`是表达式，`decltype(exp)`和`exp`类型相同

- `exp`是函数调用，`decltype(exp)`和函数返回值类型相同

- 其它情况，若`exp`是左值，`decltype(exp)`是`exp`类型的左值引用

```cpp
int a = 0, b = 0;
decltype(a + b) c = 0;  // c是int，因为(a+b)返回一个右值
decltype(a += b) d = c; // d是int&，因为(a+=b)返回一个左值

d = 20;
std::cout << "c " << c << std::endl; // 输出c 20
```

## auto和decltype的配合使用
`auto`和`decltype`一般配合使用在推导函数返回值的类型问题上。
```cpp

template<typename T, typename U>
return_value add(T t, U u) { // t和v类型不确定，无法推导出return_value类型
    return t + u;
}
```

上面代码由于`t`和`u`类型不确定，那如何推导出返回值类型呢，我们可能会想到这种
```cpp
template<typename T, typename U>
decltype(t + u) add(T t, U u) { // t和u尚未定义
    return t + u;
}
```

这段代码在`C++11`上是编译不过的，因为在`decltype(t +u)`推导时，`t`和`u`尚未定义，就会编译出错，所以有了下面的叫做**返回类型后置**的配合使用方法：
```cpp
template<typename T, typename U>
auto add(T t, U u) -> decltype(t + u) {
    return t + u;
}
```
返回值后置类型语法就是为了解决函数返回值类型依赖于参数但却难以确定返回值类型的问题。

