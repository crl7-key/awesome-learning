## 基于范围的for循环
```cpp

vector<int> vec;

for (auto iter = vec.begin(); iter != vec.end(); iter++) { // before c++11
   std::cout << *iter << std::endl;
}

for (int i : vec) { // c++11基于范围的for循环
  std::cout << i << std::endl;
}
```
