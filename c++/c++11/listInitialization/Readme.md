
## 列表初始化

在`C++11`中可以直接在变量名后面加上初始化列表来进行对象的初始化

```cpp
class A {
public:
    A(int) {}
private:
    A(const A&) {}
};

int main() {
   A a(123);
   // A b = 123; // error
   A c = { 123 };
   A d{123}; // c++11
   
   int e = {123};
   std::cout << e << std::endl;
   int f{123}; // c++11
   std::cout << f << std::endl;
   return 0;
}
```
列表初始化也可以用在函数的返回值上
```cpp
std::vector<int> func() {
   return {};
}
```

## 列表初始化的一些规则


### 聚合类型可以进行直接列表初始化

聚合类型一般有:
- 类型是一个普通数组，如`int[5]`，`char[]`，`double[]`等

- 类型是一个类，且满足以下条件：

    - 没有用户声明的构造函数

    - 没有用户提供的构造函数(允许显示预置或弃置的构造函数)

    - 没有私有或保护的非静态数据成员

    - 没有基类

    - 没有虚函数

    - 没有`{}`和`=`直接初始化的非静态数据成员

    - 没有默认成员初始化器

```cpp

class A {
public:
   int a;
   int b;
   int c;
   A(int, int){}
};

int main() {
   A a{1, 2, 3};// error，A有自定义的构造函数，不能列表初始化
}
```
上述代码类`A`不是聚合类型，无法进行列表初始化，必须以自定义的构造函数来构造对象。
```cpp
class A {
public:
   int a;
   int b;
   virtual void func() {}   // 含有虚函数，不是聚合类
};

class Base {};

class B : public Base {  // 有基类，不是聚合类
public:
  int a;
  int b;
};

class C {
public:
   int a;
   int b = 10; // 有等号初始化，不是聚合类
};

class D {
public:
   int a;
   int b;
private:
   int c; // 含有私有的非静态数据成员，不是聚合类
};

class E {
public:
   int a;
   int b;
   E() : a(0), b(0) {} // 含有默认成员初始化器，不是聚合类
};
```

对于一个聚合类型，使用列表初始化相当于对其中的每个元素分别赋值；对于非聚合类型，需要先自定义一个对应的构造函数，此时列表初始化将调用相应的构造函数。


## std::initializer_list
`std::initializer_list<T>`，它可以接收任意长度的初始化列表，但是里面必须是相同类型`T`，或者都可以转换为`T`。
```cpp
class CustomVec {
public:
   std::vector<int> data;
   CustomVec(std::initializer_list<int> list) {
       for (auto iter = list.begin(); iter != list.end(); ++iter) {
           data.push_back(*iter);
      }
  }
};
```

**列表初始化的好处**

- 方便，且基本上可以替代括号初始化

- 以使用初始化列表接受任意长度

- 可以防止类型窄化，避免精度丢失的隐式类型转换

类型窄化: 列表初始化通过禁止下列转换，对隐式转化加以限制

- 从浮点类型到整数类型的转换
```cpp
   int a = 1.2;     // ok
   int b = {1.2};   // error
```
- 从 `long double` 到 `double 或 float `的转换，以及从 `double` 到 `float` 的转换，除非源是常量表达式且不发生溢出
```cpp
   float c = 1e70;    // ok
   float d = {1e70};  // error

   float e = (unsigned long long)-1;     // ok
   float f = {(unsigned long long)-1};   // error
   float g = (unsigned long long)1;      // ok
   float h = {(unsigned long long)1};    // ok
```
- 从整数类型到浮点类型的转换，除非源是其值能完全存储于目标类型的常量表达式

- 从整数或无作用域枚举类型到不能表示原类型所有值的整数类型的转换，除非源是其值能完全存储于目标类型的常量表达式
```cpp
   const int i = 1000;
   const int j = 2;
   char k = i;      // ok
   char l = {i};    // error

   char m = j;      // ok
   char m = {j};    // ok，因为是const类型，这里如果去掉const属性，也会报错
```
