## 左值和右值

## 目录
* [左值和右值](左值和右值-1)
* [纯右值将亡值](#纯右值将亡值)
  * [纯右值](#纯右值) 
  * [将亡值](#将亡值)
* [左值引用丶右值引用](#左值引用右值引用)
  * [左值引用](#左值引用)
  * [右值引用](#右值引用)
* [移动语义](#移动语义)
  * [深拷贝浅拷贝](#深拷贝浅拷贝) 
* [完美转发](#完美转发)
* [返回值优化](#返回值优化)


## [左值和右值](https://github.com/crl7-key/awesome-learning/tree/master/c%2B%2B/basic/chapter04#%E5%B7%A6%E5%80%BC%E5%92%8C%E5%8F%B3%E5%80%BC)

概念1：

左值：可以放到等号左边的东西叫左值。

右值：不可以放到等号左边的东西就叫右值。

概念2：

左值：可以取地址并且有名字的东西就是左值。

右值：不能取地址的没有名字的东西就是右值。

举例：
```cpp
int a = b + c; 
```
`a`是左值，有变量名，可以取地址，也可以放到等号左边, 表达式`b+c`的返回值是右值，没有名字且不能取地址，`&(b+c)`不能通过编译，而且也不能放到等号左边。
```cpp
int a = 1; // a是左值，1作为普通字面量是右值
```

左值一般有：

- 函数名和变量名

- 返回左值引用的函数调用

- 前置自增自减表达式`++i`、`--i`

- 由赋值表达式或赋值运算符连接的表达式(`a=b`, `a += b`等)

- 解引用表达式`*p`

- 字符串字面值`"abcd"`


## 纯右值、将亡值
纯右值和将亡值都属于右值。

### 纯右值

运算表达式产生的临时变量、不和对象关联的原始字面量、非引用返回的临时变量、`lambda`表达式等都是纯右值。

举例：

- 除字符串字面值外的字面值

- 返回非引用类型的函数调用

- 后置自增自减表达式`i++`、`i--`

- 算术表达式(`a+b`, `a*b`, `a&&b`, `a==b`等)

- 取地址表达式等(`&a`)

### 将亡值

将亡值是指`C++11`新增的和右值引用相关的表达式，一般有
- 将要被移动的对象

- `T&&`函数的返回值

- `std::move`函数的返回值

- 转换为`T&&`类型转换函数的返回值

将亡值可以理解为即将要销毁的值，通过“盗取”其它变量内存空间方式获取的值，在确保其它变量不再被使用或者即将被销毁时，可以避免内存空间的释放和分配，延长变量值的生命周期，常用来完成移动构造或者移动赋值的特殊任务。


```cpp
class A {
    xxx;
};
A a;
auto c = std::move(a);        // c是将亡值
auto d = static_cast<A&&>(a); // d是将亡值
```

## 左值引用、右值引用
左值引用就是对左值进行引用的类型

右值引用就是对右值进行引用的类型

他们都是引用，都是对象的一个别名，并不拥有所绑定对象的堆存，所以都必须立即初始化。

```cpp
type &name = exp;   // 左值引用
type &&name = exp;  // 右值引用
```
### 左值引用
```cpp
int a = 5;
int &b = a;           // b是左值引用
b = 4;
int &c = 10;          // error，10无法取地址，无法进行引用
const int &d = 10;    // ok，因为是常引用，引用常量数字，这个常量数字会存储在内存中，可以取地址
```
对于左值引用，等号右边的值必须可以取地址，如果不能取地址，则会编译失败，或者可以使用`const`引用形式，但这样就只能通过引用来读取输出，不能修改数组，因为是常量引用。


### 右值引用

如果使用右值引用，那表达式等号右边的值需要时右值，可以使用`std::move`函数强制把左值转换为右值。

```cpp
int a = 4;
int &&b = a;              // error, a是左值
int &&c = std::move(a);   // ok
```
## 移动语义

### 深拷贝、浅拷贝
```cpp

class A {
public:
    A(int size) : size_(size) {
        m_data = new int[size];
    }
    A(){}
    A(const A& a) {
        m_size = a.m_size;
        m_data = a.m_data;
        std::cout << "copy " << std::endl;
    }
    ~A() {
        delete[] m_data;
    }
    int *m_data;
    int m_size;
};
int main() {
    A a(10);
    A b = a;
    std::cout << "b " << b.m_data << std::endl;
    std::cout << "a " << a.m_data << std::endl;
    return 0;
}
```
上面代码中，两个输出的是相同的地址，`a`和`b`的`m_data`指针指向了同一块内存，这就是浅拷贝，只是数据的简单赋值，那再析构时`m_data`内存会被释放两次，导致程序出问题，这里正常会出现`double free`导致程序崩溃的，这样的程序肯定是有隐患的，如何消除这种隐患呢，可以使用如下深拷贝：
```cpp

class A {
public:
    A(int size) : size_(size) {
        m_data = new int[size];
    }
    A(){}
    A(const A& a) {
        m_size = a.m_size;
        m_data = new int[m_size];
        std::cout << "copy " << std::endl;
    }
    ~A() {
        delete[] m_data;
    }
    int *m_data;
    int m_size;
};
int main() {
    A a(10);
    A b = a;
    std::cout << "b " << b.m_data << std::endl;
    std::cout << "a " << a.m_data << std::endl;
    return 0;
}
```
深拷贝就是再拷贝对象时，如果被拷贝对象内部还有指针引用指向其它资源，自己需要重新开辟一块新内存存储资源，而不是简单的赋值。

移动语义，可以理解为转移所有权，之前的拷贝是对于别人的资源，自己重新分配一块内存存储复制过来的资源，而对于移动语义，类似于转让或者资源窃取的意思，对于那块资源，转为自己所拥有，别人不再拥有也不会再使用，通过`C++11`新增的移动语义可以省去很多拷贝负担，怎么利用移动语义呢，可以通过移动构造函数。
```cpp

class A {
public:
    A(int size) : size_(size) {
        m_data = new int[size];
    }
    A(){}
    A(const A& a) {
        m_size = a.m_size;
        m_data = new int[m_size];
        std::cout << "copy " << std::endl;
    }
    A(A&& a) {
        this->m_data = a.m_data;
        a.m_data = nullptr;
        std::cout << "move " << std::endl;
    }
    ~A() {
        if (m_data != nullptr) {
         delete[] m_data;
        }
    }
    int *m_data;
    int m_size;
};
int main() {
    A a(10);
    A b = a;
    A c = std::move(a); // 调用移动构造函数
    return 0;
}
```
如果不使用`std::move()`，会有很大的拷贝代价，使用移动语义可以避免很多无用的拷贝，提供程序性能，`C++`所有的`STL`都实现了移动语义，方便我们使用。例如：
```cpp
std::vector<string> vec1;
...
std::vector<string> vec2 = std::move(vec1); // 调用移动构造,免去很多拷贝
```

> 注意：移动语义仅针对于那些实现了移动构造函数的类的对象，对于那种基本类型`int`、`float`等没有任何优化作用，还是会拷贝，因为它们实现没有对应的移动构造函数。


## 完美转发
完美转发指可以写一个接受任意实参的函数模板，并转发到其它函数，目标函数会收到与转发函数完全相同的实参，转发函数实参是左值那目标函数实参也是左值，转发函数实参是右值那目标函数实参也是右值。`std::forward()`可以实现完美转发
```cpp

void PrintV(int &t) {
    cout << "lvalue" << endl;
}

void PrintV(int &&t) {
    cout << "rvalue" << endl;
}

template<typename T>
void Test(T &&t) {
    PrintV(t);
    PrintV(std::forward<T>(t));

    PrintV(std::move(t));
}

int main() {
    Test(1);                      // lvalue rvalue rvalue
    int a = 1;
    Test(a);                      // lvalue lvalue rvalue
    Test(std::forward<int>(a));   // lvalue rvalue rvalue
    Test(std::forward<int&>(a));  // lvalue lvalue rvalue
    Test(std::forward<int&&>(a)); // lvalue rvalue rvalue
    return 0;
}
```
- `Test(1)`：`1`是右值，模板中`T &&t`这种为万能引用，右值`1`传到`Test`函数中变成了右值引用，但是调用`PrintV()`时候，`t`变成了左值，因为它变成了一个拥有名字的变量，所以打印`lvalue`，而`PrintV(std::forward<T>(t))`时候，会进行完美转发，按照原来的类型转发，所以打印`rvalue`，`PrintV(std::move(t))`毫无疑问会打印`rvalue`。

- `Test(a)`：`a`是左值，模板中`T &&`这种为万能引用，左值`a`传到`Test`函数中变成了左值引用，所以有代码中打印。

- `Test(std::forward<T>(a))`：转发为左值还是右值，依赖于`T`，`T`是左值那就转发为左值，`T`是右值那就转发为右值。



## 返回值优化
返回值优化(`RVO`)是一种`C++`编译优化技术，当函数需要返回一个对象实例时候，就会创建一个临时对象并通过复制构造函数将目标对象复制到临时对象，这里有复制构造函数和析构函数会被多余的调用到，有代价，而通过返回值优化，`C++`标准允许省略调用这些复制构造函数。

那什么时候编译器会进行返回值优化呢?

- `return`的值类型与函数的返回值类型相同

- `return`的是一个局部对象
示例1：
```cpp
std::vector<int> return_vector(void) {
    std::vector<int> tmp {1,2,3,4,5};
    return tmp;
}
std::vector<int> &&rval_ref = return_vector();
```
不会触发`RVO`，拷贝构造了一个临时的对象，临时对象的生命周期和`rval_ref`绑定，等价于下面这段代码：
```cpp
const std::vector<int>& rval_ref = return_vector();
```
示例2：
```cpp
std::vector<int>&& return_vector(void) {
    std::vector<int> tmp {1,2,3,4,5};
    return std::move(tmp);
}

std::vector<int> &&rval_ref = return_vector();
```
这段代码会造成运行时错误，因为`rval_ref`引用了被析构的`tmp`。

示例3：
```cpp
std::vector<int> return_vector(void) {
    std::vector<int> tmp {1,2,3,4,5};
    return std::move(tmp);
}

std::vector<int> &&rval_ref = return_vector();
```
和示例1类似，`std::move`一个临时对象是没有必要的，也会忽略掉返回值优化。

触发`RVO`的代码：
```
std::vector<int> return_vector(void) {
    std::vector<int> tmp {1,2,3,4,5};
    return tmp;
}

std::vector<int> rval_ref = return_vector();
```
这段代码会触发`RVO`，不拷贝也不移动，不生成临时对象。


**[⬆ 返回顶部](#目录)**
