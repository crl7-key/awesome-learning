#ifndef INC_MEMORY_DETECT_H_
#define INC_MEMORY_DETECT_H_

#include <stdio.h>

#include <iostream>

void* operator new(std::size_t size, const char* file, int line);
void* operator new[](std::size_t size, const char* file, int line);

// 让底层程序申请内存时调用重载
#define new new (__FILE__, __LINE__)

int checkLeaks();

#endif
