# 内存泄漏检测工具

编译
```
g++ test.cc MemoryDetect.cc -o test
```
运行
```
> ./test 
memory detect 
Leaked object at 0x555a5e996370 (size 4, test.cc:19)
*** 1 leaks found
```

## AddressSanitizer(ASan)

该工具为`gcc`自带，`4.8`以上版本都可以使用，支持`Linux`、`OS`、`Android`等多种平台，不止可以检测内存泄漏，它其实是一个内存错误检测工具，可以检测的问题有：
- 内存泄漏

- 堆栈和全局内存越界访问

- `free`后继续使用

- 局部内存被外层使用

- 检测静态初始化顺序失败

### 使用Asan检测内存泄漏
测试用例代码:
```cc
#include <iostream>

void func1() {
    malloc(5);
}

void func2() {
    malloc(10);
}

int main()
{
    func1();
    func2();
    return 0;
}
```

```sh
g++ -fsanitize=address -g test_leak.cc -o test_leak
```
运行:
```sh
./test_leak
```

输出:
```sh
=================================================================
==13096==ERROR: LeakSanitizer: detected memory leaks

Direct leak of 10 byte(s) in 1 object(s) allocated from:
    #0 0x7f6e04427330 in __interceptor_malloc (/lib/x86_64-linux-gnu/libasan.so.5+0xe9330)
    #1 0x55920d24c1d3 in func2() /root/code/c++/memoryCheck/test_leak.cc:8
    #2 0x55920d24c1e4 in main /root/code/c++/memoryCheck/test_leak.cc:14
    #3 0x7f6e03e8009a in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x2409a)

Direct leak of 5 byte(s) in 1 object(s) allocated from:
    #0 0x7f6e04427330 in __interceptor_malloc (/lib/x86_64-linux-gnu/libasan.so.5+0xe9330)
    #1 0x55920d24c1c2 in func1() /root/code/c++/memoryCheck/test_leak.cc:4
    #2 0x55920d24c1df in main /root/code/c++/memoryCheck/test_leak.cc:13
    #3 0x7f6e03e8009a in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x2409a)

SUMMARY: AddressSanitizer: 15 byte(s) leaked in 2 allocation(s).
```
编译方式很简单，只需要添加`-fsanitize=address -g`就可以检测出具体产生内存泄漏的位置以及泄漏空间的大小。



### 使用Asan检测堆栈内存越界访问

示例：
```cc

#include <iostream>

int main() {
   int *arr = new int[100];
   arr[0] = 0;
   int res = arr[100];  // out of bounds
   delete[] arr;
   return res;
}
```
编译:
```sh
g++ -fsanitize=address -g test_leak.cc -o test_leak
```
输出:
```sh
./test_leak 
=================================================================
==13251==ERROR: AddressSanitizer: heap-buffer-overflow on address 0x6140000001d0 at pc 0x55bdb2976279 bp 0x7ffc3166aac0 sp 0x7ffc3166aab8
READ of size 4 at 0x6140000001d0 thread T0
    #0 0x55bdb2976278 in main /root/code/c++/memoryCheck/test_leak.cc:7
    #1 0x7f0c21b4209a in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x2409a)
    #2 0x55bdb2976129 in _start (/root/code/c++/memoryCheck/test_leak+0x1129)

0x6140000001d0 is located 0 bytes to the right of 400-byte region [0x614000000040,0x6140000001d0)
allocated by thread T0 here:
    #0 0x7f0c220eaef0 in operator new[](unsigned long) (/lib/x86_64-linux-gnu/libasan.so.5+0xeaef0)
    #1 0x55bdb29761f6 in main /root/code/c++/memoryCheck/test_leak.cc:5
    #2 0x7f0c21b4209a in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x2409a)

SUMMARY: AddressSanitizer: heap-buffer-overflow /root/code/c++/memoryCheck/test_leak.cc:7 in main
Shadow bytes around the buggy address:
  0x0c287fff7fe0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c287fff7ff0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c287fff8000: fa fa fa fa fa fa fa fa 00 00 00 00 00 00 00 00
  0x0c287fff8010: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c287fff8020: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
=>0x0c287fff8030: 00 00 00 00 00 00 00 00 00 00[fa]fa fa fa fa fa
  0x0c287fff8040: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c287fff8050: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c287fff8060: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c287fff8070: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c287fff8080: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
Shadow byte legend (one shadow byte represents 8 application bytes):
  Addressable:           00
  Partially addressable: 01 02 03 04 05 06 07 
  Heap left redzone:       fa
  Freed heap region:       fd
  Stack left redzone:      f1
  Stack mid redzone:       f2
  Stack right redzone:     f3
  Stack after return:      f5
  Stack use after scope:   f8
  Global redzone:          f9
  Global init order:       f6
  Poisoned by user:        f7
  Container overflow:      fc
  Array cookie:            ac
  Intra object redzone:    bb
  ASan internal:           fe
  Left alloca redzone:     ca
  Right alloca redzone:    cb
==13251==ABORTING
```

### 使用Asan检测全局内存越界访问
示例代码:
```cc

#include <iostream>

int global_arr[100] = {0};

int main() {
   int res = global_arr[100];  // out of bounds
   return 0;
}
```
编译：
```sh
g++ -fsanitize=address -g test_leak.cc -o test_leak
```
输出：
```sh
./test_leak 
=================================================================
==13676==ERROR: AddressSanitizer: global-buffer-overflow on address 0x5606a5939370 at pc 0x5606a59361ef bp 0x7ffc82dcb500 sp 0x7ffc82dcb4f8
READ of size 4 at 0x5606a5939370 thread T0
    #0 0x5606a59361ee in main /root/code/c++/memoryCheck/test_leak.cc:6
    #1 0x7fb3c229209a in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x2409a)
    #2 0x5606a59360f9 in _start (/root/code/c++/memoryCheck/test_leak+0x10f9)

0x5606a5939370 is located 0 bytes to the right of global variable 'global_arr' defined in 'test_leak.cc:3:5' (0x5606a59391e0) of size 400
SUMMARY: AddressSanitizer: global-buffer-overflow /root/code/c++/memoryCheck/test_leak.cc:6 in main
Shadow bytes around the buggy address:
  0x0ac154b1f210: 00 00 00 00 00 00 00 00 f9 f9 f9 f9 f9 f9 f9 f9
  0x0ac154b1f220: f9 f9 f9 f9 f9 f9 f9 f9 f9 f9 f9 f9 f9 f9 f9 f9
  0x0ac154b1f230: 00 00 00 00 01 f9 f9 f9 f9 f9 f9 f9 00 00 00 00
  0x0ac154b1f240: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0ac154b1f250: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
=>0x0ac154b1f260: 00 00 00 00 00 00 00 00 00 00 00 00 00 00[f9]f9
  0x0ac154b1f270: f9 f9 f9 f9 00 00 00 00 00 00 00 00 00 00 00 00
  0x0ac154b1f280: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0ac154b1f290: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0ac154b1f2a0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0ac154b1f2b0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
Shadow byte legend (one shadow byte represents 8 application bytes):
  Addressable:           00
  Partially addressable: 01 02 03 04 05 06 07 
  Heap left redzone:       fa
  Freed heap region:       fd
  Stack left redzone:      f1
  Stack mid redzone:       f2
  Stack right redzone:     f3
  Stack after return:      f5
  Stack use after scope:   f8
  Global redzone:          f9
  Global init order:       f6
  Poisoned by user:        f7
  Container overflow:      fc
  Array cookie:            ac
  Intra object redzone:    bb
  ASan internal:           fe
  Left alloca redzone:     ca
  Right alloca redzone:    cb
==13676==ABORTING
```

### 使用Asan检测局部内存被外层使用
示例代码:
```cc
#include <iostream>

volatile int* ptr = 0;

int main()
{
  {
     int x = 0;
     ptr = &x;
  }
  
  *ptr = 1;
  return 0;
}
```

编译:
```sh
g++ -fsanitize=address -g test_leak.cc -o test_leak
```
输出:
```sh
./test_leak 
=================================================================
==13780==ERROR: AddressSanitizer: stack-use-after-scope on address 0x7ffc9f834b20 at pc 0x5567e50612d4 bp 0x7ffc9f834af0 sp 0x7ffc9f834ae8
WRITE of size 4 at 0x7ffc9f834b20 thread T0
    #0 0x5567e50612d3 in main /root/code/c++/memoryCheck/test_leak.cc:11
    #1 0x7f063a7f609a in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x2409a)
    #2 0x5567e5061109 in _start (/root/code/c++/memoryCheck/test_leak+0x1109)

Address 0x7ffc9f834b20 is located in stack of thread T0 at offset 32 in frame
    #0 0x5567e50611d4 in main /root/code/c++/memoryCheck/test_leak.cc:5

  This frame has 1 object(s):
    [32, 36) 'i' <== Memory access at offset 32 is inside this variable
HINT: this may be a false positive if your program uses some custom stack unwind mechanism or swapcontext
      (longjmp and C++ exceptions *are* supported)
SUMMARY: AddressSanitizer: stack-use-after-scope /root/code/c++/memoryCheck/test_leak.cc:11 in main
Shadow bytes around the buggy address:
  0x100013efe910: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x100013efe920: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x100013efe930: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x100013efe940: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x100013efe950: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
=>0x100013efe960: f1 f1 f1 f1[f8]f2 f2 f2 f3 f3 f3 f3 00 00 00 00
  0x100013efe970: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x100013efe980: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x100013efe990: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x100013efe9a0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x100013efe9b0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
Shadow byte legend (one shadow byte represents 8 application bytes):
  Addressable:           00
  Partially addressable: 01 02 03 04 05 06 07 
  Heap left redzone:       fa
  Freed heap region:       fd
  Stack left redzone:      f1
  Stack mid redzone:       f2
  Stack right redzone:     f3
  Stack after return:      f5
  Stack use after scope:   f8
  Global redzone:          f9
  Global init order:       f6
  Poisoned by user:        f7
  Container overflow:      fc
  Array cookie:            ac
  Intra object redzone:    bb
  ASan internal:           fe
  Left alloca redzone:     ca
  Right alloca redzone:    cb
==13780==ABORTING
```
### 使用`Asan`检测`free`后使用
示例代码:
```cc
#include <iostream>

int main() {
    int* arr = new int[100];
    delete[] arr;
    int a = arr[0];  // error
    return 0;
}
```
编译:
```sh
g++ -fsanitize=address -g test_leak.cc -o test_leak
```
输出:
```sh
./test_leak 
=================================================================
==14021==ERROR: AddressSanitizer: heap-use-after-free on address 0x614000000040 at pc 0x561f2cbbd235 bp 0x7ffedc995090 sp 0x7ffedc995088
READ of size 4 at 0x614000000040 thread T0
    #0 0x561f2cbbd234 in main /root/code/c++/memoryCheck/test_leak.cc:8
    #1 0x7f4c6c66f09a in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x2409a)
    #2 0x561f2cbbd119 in _start (/root/code/c++/memoryCheck/test_leak+0x1119)

0x614000000040 is located 0 bytes inside of 400-byte region [0x614000000040,0x6140000001d0)
freed by thread T0 here:
    #0 0x7f4c6cc18c40 in operator delete[](void*) (/lib/x86_64-linux-gnu/libasan.so.5+0xebc40)
    #1 0x561f2cbbd1fd in main /root/code/c++/memoryCheck/test_leak.cc:7
    #2 0x7f4c6c66f09a in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x2409a)

previously allocated by thread T0 here:
    #0 0x7f4c6cc17ef0 in operator new[](unsigned long) (/lib/x86_64-linux-gnu/libasan.so.5+0xeaef0)
    #1 0x561f2cbbd1e6 in main /root/code/c++/memoryCheck/test_leak.cc:6
    #2 0x7f4c6c66f09a in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x2409a)

SUMMARY: AddressSanitizer: heap-use-after-free /root/code/c++/memoryCheck/test_leak.cc:8 in main
Shadow bytes around the buggy address:
  0x0c287fff7fb0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c287fff7fc0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c287fff7fd0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c287fff7fe0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c287fff7ff0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
=>0x0c287fff8000: fa fa fa fa fa fa fa fa[fd]fd fd fd fd fd fd fd
  0x0c287fff8010: fd fd fd fd fd fd fd fd fd fd fd fd fd fd fd fd
  0x0c287fff8020: fd fd fd fd fd fd fd fd fd fd fd fd fd fd fd fd
  0x0c287fff8030: fd fd fd fd fd fd fd fd fd fd fa fa fa fa fa fa
  0x0c287fff8040: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c287fff8050: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
Shadow byte legend (one shadow byte represents 8 application bytes):
  Addressable:           00
  Partially addressable: 01 02 03 04 05 06 07 
  Heap left redzone:       fa
  Freed heap region:       fd
  Stack left redzone:      f1
  Stack mid redzone:       f2
  Stack right redzone:     f3
  Stack after return:      f5
  Stack use after scope:   f8
  Global redzone:          f9
  Global init order:       f6
  Poisoned by user:        f7
  Container overflow:      fc
  Array cookie:            ac
  Intra object redzone:    bb
  ASan internal:           fe
  Left alloca redzone:     ca
  Right alloca redzone:    cb
==14021==ABORTING
```

### 使用Asan检测静态初始化顺序失败
示例代码`memory_test1.cc`:
```cc
#include <iostream>
extern int extern_global;
int __attribute__((noinline)) read_extern_global() {
  return extern_global;
}
int x = read_extern_global() + 1;

int main() {
  printf("%d\n", x);
  return 0;
}
```

`memory_test2.cc`:
```cc
int foo() { return 42; }
int extern_global = foo();
```
第一种编译方式输出如下：
```sh
g++ memory_test1.cc memory_test2.cc && ./a.out

1
```
第二种编译方式输出如下：
```sh
g++ memory_test2.cc memory_test1.cc && ./a.out

43
```
这种问题平时编程过程中可以都不会太注意，然而通过`ASan`可以检测出这种潜在的`bug`：

编译：
```sh
g++ memory_test1.cc memory_test2.cc  -fsanitize=address -g
```
运行：
```sh
ASAN_OPTIONS=check_initialization_order=true:strict_init_order=true ./a.out
```

输出:
```sh
ASAN_OPTIONS=check_initialization_order=true:strict_init_order=true ./a.out
=================================================================
==14569==ERROR: AddressSanitizer: initialization-order-fiasco on address 0x55b555714300 at pc 0x55b555711213 bp 0x7ffd1f006910 sp 0x7ffd1f006908
READ of size 4 at 0x55b555714300 thread T0
    #0 0x55b555711212 in read_extern_global() /root/code/c++/memoryCheck/memory_test1.cc:6
    #1 0x55b5557112cb in __static_initialization_and_destruction_0 /root/code/c++/memoryCheck/memory_test1.cc:9
    #2 0x55b555711322 in _GLOBAL__sub_I__Z18read_extern_globalv /root/code/c++/memoryCheck/memory_test1.cc:14
    #3 0x55b555711474 in __libc_csu_init (/root/code/c++/memoryCheck/a.out+0x1474)
    #4 0x7f3208d30029 in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x24029)
    #5 0x55b555711119 in _start (/root/code/c++/memoryCheck/a.out+0x1119)

0x55b555714300 is located 0 bytes inside of global variable 'extern_global' defined in 'memory_test2.cc:4:5' (0x55b555714300) of size 4
  registered at:
    #0 0x7f3209223a20  (/lib/x86_64-linux-gnu/libasan.so.5+0x35a20)
    #1 0x55b555711422 in _GLOBAL__sub_I_00099_1__Z3foov (/root/code/c++/memoryCheck/a.out+0x1422)
    #2 0x55b555711474 in __libc_csu_init (/root/code/c++/memoryCheck/a.out+0x1474)

SUMMARY: AddressSanitizer: initialization-order-fiasco /root/code/c++/memoryCheck/memory_test1.cc:6 in read_extern_global()
Shadow bytes around the buggy address:
  0x0ab72aada810: 00 00 00 00 00 00 00 00 00 00 00 00 f9 f9 f9 f9
  0x0ab72aada820: f9 f9 f9 f9 f9 f9 f9 f9 f9 f9 f9 f9 f9 f9 f9 f9
  0x0ab72aada830: f9 f9 f9 f9 f9 f9 f9 f9 f9 f9 f9 f9 00 00 00 00
  0x0ab72aada840: f9 f9 f9 f9 f9 f9 f9 f9 00 00 00 00 01 f9 f9 f9
  0x0ab72aada850: f9 f9 f9 f9 04 f9 f9 f9 f9 f9 f9 f9 00 00 00 00
=>0x0ab72aada860:[f6]f6 f6 f6 f6 f6 f6 f6 00 00 00 00 00 00 00 00
  0x0ab72aada870: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0ab72aada880: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0ab72aada890: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0ab72aada8a0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0ab72aada8b0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
Shadow byte legend (one shadow byte represents 8 application bytes):
  Addressable:           00
  Partially addressable: 01 02 03 04 05 06 07 
  Heap left redzone:       fa
  Freed heap region:       fd
  Stack left redzone:      f1
  Stack mid redzone:       f2
  Stack right redzone:     f3
  Stack after return:      f5
  Stack use after scope:   f8
  Global redzone:          f9
  Global init order:       f6
  Poisoned by user:        f7
  Container overflow:      fc
  Array cookie:            ac
  Intra object redzone:    bb
  ASan internal:           fe
  Left alloca redzone:     ca
  Right alloca redzone:    cb
==14569==ABORTING
```

`ASan`是个很好的检测内存问题的工具，不需要配置环境，使用还方便，编译时只需要`-fsanitize=address -g`就可以，运行程序时候可以选择添加对应的`ASAN_OPTIONS`环境变量就可以检测出很多内存问题。它的错误信息也很有用，明确指出当前是什么类型的内存错误，如：

- `detected memory leaks`

- `heap-buffer-overflow`

- `stack-buffer-overflow`

- `global-buffer-overflow`

- `heap-use-after-free`

- `initialization-order-fiasco`

具体可以看`google`的[官方文档](https://github.com/google/sanitizers/wiki/AddressSanitizer)

