## simple privilege_promotion


添加普通用户
`centos`添加用户
```sh
useradd test
passwd test
```
`ubuntu`丶`debian`添加用户

`ubuntu`，`debian`系统创建用户需要指定用户登录目录，才能用此普通用户登录
```
useradd -m test -s /bin/bash -d /home/test
passwd test
```

编译
```
gcc privilege-promotion.c -o privilege-promotion  
```

`root`用户：
```sh
chmod u+x privilege-promotion  
chmod u+s privilege-promotion  
```

普通用户:
```
su test
./privilege-promotion  
```
