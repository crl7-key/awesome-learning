## C++设计模式之外观模式
![](https://img.shields.io/badge/standard--readme-MIT-green?style=flat&logo=appveyor)   

## 目录
* [什么是外观模式?](#什么是外观模式)
* [什么时候使用?](#什么时候使用)
* [代码实现](#代码实现)
* [与其他模式的关系](#与其他模式的关系)

## 什么是外观模式?
外观模式是一种结构型设计模式,能为程序库、 框架或其他复杂类提供一个简单的接口。

为子系统中的一组接口提供统一的接口。`Facade`定义了一个更高级别的接口,该接口使子系统更易于使用。外观模式又称为门面模式。

## 什么时候使用?
- 想为复杂的子系统提供一个简单的界面
- 客户端和抽象的实现类之间有很多依赖关系
- 想要对子系统进行分层,可以使用外观来定义每个子系统级别的入口点

比如我们每天打开电脑时都需要做三件事：

- 打开浏览器
- 打开 IDE
- 打开微信

每天下班时关机前需要做三件事：
- 关闭浏览器
- 关闭 IDE
- 关闭微信
## 代码实现
用程序模拟如下：
```c++
#include <iostream>

class Browser {
public:
    void open() {
        std::cout << "打开浏览器" << std::endl;
    }

    void close() {
        std::cout << "关闭浏览器" << std::endl;
    }
};

class IDE {
public:
    void open() {
        std::cout << "打开IDE" << std::endl;
    }

    void close() {
        std::cout << "关闭IDE" << std::endl;
    }
};

class Wechat {
public:
    void open() {
        std::cout << "打开微信" << std::endl;
    }

    void close() {
        std::cout << "关闭微信" << std::endl;
    }
};

class Facade {
public:

    Facade():m_browser(),m_ide(),m_wechat(){}

    void open() {
        m_browser->open();
        m_ide->open();
        m_wechat->open();
    }

    void close() {
        m_browser->close();
        m_ide->close();
        m_wechat->close();
    }

private:
    Browser* m_browser;
    IDE* m_ide;
    Wechat* m_wechat;
};

int main()
{
    Facade* facade = new Facade();
    std::cout << "上班:" << std::endl;
    facade->open();
    std::cout << "下班:" << std::endl;
    facade->close();
    delete facade;

    return 0;
}
```


## 与其他模式的关系
- 外观模式为现有对象定义了一个新接口, 适配器模式则会试图运用已有的接口。 适配器通常只封装一个对象, 外观通常会作用于整个对象子系统上。

- 当只需对客户端代码隐藏子系统创建对象的方式时,可以使用抽象工厂模式来代替外观。

- 享元模式展示了如何生成大量的小型对象,外观则展示了如何用一个对象来代表整个子系统。

- 外观和中介者模式的职责类似： 它们都尝试在大量紧密耦合的类中组织起合作。

- 外观为子系统中的所有对象定义了一个简单接口, 但是它不提供任何新功能。 子系统本身不会意识到外观的存在。 子系统中的对象可以直接进行交流。

- 中介者将系统中组件的沟通行为中心化。 各组件只知道中介者对象, 无法直接相互交流。

- 外观类通常可以转换为单例模式类, 因为在大部分情况下一个外观对象就足够了。

- 外观与代理模式的相似之处在于它们都缓存了一个复杂实体并自行对其进行初始化。 代理与其服务对象遵循同一接口,使得自己和服务对象可以互换, 在这一点上它与外观不同。

**[⬆ 返回顶部](#目录)**
