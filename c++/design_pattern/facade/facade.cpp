#include <iostream>

class SubsytemA {
public:
    SubsytemA(){
        std::cout << "SubstemA()" << std::endl;
    }

    virtual ~SubsytemA(){
        std::cout << "~SubstemA()" << std::endl;
    }

    void subOperation() {
        std::cout << "SubstemA A method" << std::endl;
        // ...
    }
};


class SubsytemB {
public:
    SubsytemB() {
        std::cout << "SubsytemB()" << std::endl;
    }

    virtual ~SubsytemB() {
        std::cout << "~SubstemB()" << std::endl;
    }

    void subOperation() {
        std::cout << "Substyem B method" << std::endl;
        // ...
    }
};

class SubsytemC {
public:
    SubsytemC() {
        std::cout << "SubsytemC()" << std::endl;
    }

    virtual ~SubsytemC() {
        std::cout << "~SubsytemC()" << std::endl;
    }

    void subOperation() {
        std::cout << "Subsytem C method" <<std::endl;
        // ...
    }
};


class Facade: public SubsytemA,SubsytemB,SubsytemC {
public:
    Facade():m_subSytemA(),m_subSytemB(),m_subsytemC() {
    }

    virtual ~Facade() {}

    void operationA() {
        m_subSytemA->subOperation();
    }

    void operationB() {
        m_subSytemB->subOperation();
    }

    void operationC() {
        m_subsytemC->subOperation();
    }

    void operation2() {
        m_subSytemA->subOperation();
        m_subSytemB->subOperation();
        // ...
    }
private:
    SubsytemA* m_subSytemA;
    SubsytemB* m_subSytemB;
    SubsytemC* m_subsytemC;
};

int main()
{
    Facade* facade = new Facade();

    facade->operationA();
    facade->operationB();
    facade->operationC();
    facade->operation2();

    delete facade;
    facade = nullptr;
    return 0;
}

