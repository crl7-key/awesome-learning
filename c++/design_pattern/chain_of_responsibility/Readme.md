# C++设计模式之责任链模式
![](https://img.shields.io/badge/standard--readme-MIT-green?style=flat&logo=appveyor)  


## 目录

* [什么是责任链模式？](#什么是责任链模式)
* [什么时候使用?](#什么时候使用)
* [代码实现](#代码实现)
* [责任链优缺点](#责任链优缺点)
* [与其他模式的关系](#与其他模式的关系)


## 什么是责任链模式?
责任链模式是一种行为设计模式,该模式链接接收对象并沿链传递请求,直到对象处理它。责任链模式通过为多个对象提供处理请求的机会来避免将请求的发送者与其接收者耦合。

## 什么时候使用?
- 多个对象可以处理一个请求,并且应该自动确定处理程序

- 想在不明确指定接收者的情况下向多个对象之一发出请求

- 应该动态指定可以处理请求的对象集

## 代码实现
```c++
#include <iostream>


// 定义处理请求的接口
class Handler{
public:
    virtual ~Handler() {}

    virtual void setHandler(Handler* successor) {
        m_successor = successor;
    }

    virtual void handlerRequest() {
        if (m_successor != nullptr) {
            m_successor->handlerRequest();
        }
    }

private:
    Handler* m_successor;
};

//  处理他们负责的请求
class ConcreteHandler1 : public Handler {
public:
    virtual ~ConcreteHandler1() {}

    bool canHandle() {
        return false;
    }

    virtual void handlerRequest() {
        if (canHandle()){
            std::cout << "Handled by Concrete Handler 1" << std::endl;
        }
        else {
            std::cout << "Cannot be handled by Handler 1" << std::endl;
            Handler::handlerRequest();
        }
        // ...
    }
};

class ConcreteHandler2 : public Handler {
public:
    virtual ~ConcreteHandler2() {}

    bool canHandle() {
        return true;
    }

    virtual void handlerRequest() {
        if (canHandle()) {
            std::cout << "Handled by Handler 2" << std::endl;
        }
        else {
            std::cout <<  "Cannot be handled by Handler 2" << std::endl;
        }
        // ...
    }
};

int main()
{
    ConcreteHandler1 handler1;
    ConcreteHandler2 handler2;

    handler1.setHandler(&handler2);
    handler1.handlerRequest();

    return 0;
}


```

## 责任链优缺点

其主要优点有：

- 可以控制请求处理的顺序。

- 降低了对象之间的耦合度。在责任链模式中,客户只需要将请求发送到责任链上即可,无须关心请求的处理细节和请求的传递过程,所以责任链将请求的发送者和请求的处理者解耦了。

- 扩展性强,满足开闭原则。可以根据需要增加新的请求处理类。

- 灵活性强。可以动态地改变链内的成员或者改变链的次序来适应流程的变化。

- 简化了对象之间的连接。每个对象只需保持一个指向其后继者的引用,不需保持其他所有处理者的引用,这避免了使用众多的条件判断语句。

- 责任分担。每个类只需要处理自己该处理的工作,不该处理的传递给下一个对象完成,明确各类的责任范围,符合类的单一职责原则。


缺点:

- 不能保证每个请求一定被处理,该请求可能一直传到链的末端都得不到处理。

- 如果责任链过长,请求的处理可能涉及多个处理对象,系统性能将受到一定影响。

- 责任链建立的合理性要靠客户端来保证,增加了客户端的复杂性,可能会由于责任链拼接次序错误而导致系统出错,比如可能出现循环调用。


## 与其他模式的关系
- 责任链模式、 命令模式、 中介者模式和观察者模式用于处理请求发送者和接收者之间的不同连接方式：

  -  责任链按照顺序将请求动态传递给一系列的潜在接收者， 直至其中一名接收者对请求进行处理。
  
  - 命令在发送者和请求者之间建立单向连接。
  
  - 中介者清除了发送者和请求者之间的直接连接， 强制它们通过一个中介对象进行间接沟通。
  
  - 观察者允许接收者动态地订阅或取消接收请求。

- 责任链通常和组合模式结合使用。 在这种情况下， 叶组件接收到请求后， 可以将请求沿包含全体父组件的链一直传递至对象树的底部。

- 责任链的管理者可使用命令模式实现。 在这种情况下， 你可以对由请求代表的同一个上下文对象执行许多不同的操作。

还有另外一种实现方式， 那就是请求自身就是一个命令对象。 在这种情况下， 你可以对由一系列不同上下文连接而成的链执行相同的操作。

- 责任链和装饰模式的类结构非常相似。 两者都依赖递归组合将需要执行的操作传递给一系列对象。 但是， 两者有几点重要的不同之处。

- 责任链的管理者可以相互独立地执行一切操作， 还可以随时停止传递请求。 另一方面， 各种装饰可以在遵循基本接口的情况下扩展对象的行为。 此外， 装饰无法中断请求的传递。



**[⬆ 返回顶部](#目录)**
