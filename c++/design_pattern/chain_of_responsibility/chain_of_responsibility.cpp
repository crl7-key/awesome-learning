#include <iostream>


// 定义处理请求的接口
class Handler{
public:
    virtual ~Handler() {}

    virtual void setHandler(Handler* successor) {
        m_successor = successor;
    }

    virtual void handlerRequest() {
        if (m_successor != nullptr) {
            m_successor->handlerRequest();
        }
    }

private:
    Handler* m_successor;
};

//  处理他们负责的请求
class ConcreteHandler1 : public Handler {
public:
    virtual ~ConcreteHandler1() {}

    bool canHandle() {
        return false;
    }

    virtual void handlerRequest() {
        if (canHandle()){
            std::cout << "Handled by Concrete Handler 1" << std::endl;
        }
        else {
            std::cout << "Cannot be handled by Handler 1" << std::endl;
            Handler::handlerRequest();
        }
        // ...
    }
};

class ConcreteHandler2 : public Handler {
public:
    virtual ~ConcreteHandler2() {}

    bool canHandle() {
        return true;
    }

    virtual void handlerRequest() {
        if (canHandle()) {
            std::cout << "Handled by Handler 2" << std::endl;
        }
        else {
            std::cout <<  "Cannot be handled by Handler 2" << std::endl;
        }
        // ...
    }
};

int main()
{
    ConcreteHandler1 handler1;
    ConcreteHandler2 handler2;

    handler1.setHandler(&handler2);
    handler1.handlerRequest();

    return 0;
}


