
# C++设计模式之中介者模式
![](https://img.shields.io/badge/standard--readme-MIT-green?style=flat&logo=appveyor)  


## 目录

* [什么是中介者模式？](#什么是中介者模式)
* [什么时候使用?](#什么时候使用)
* [代码实现](#代码实现)
* [中介者模式的优缺点](#中介者模式的优缺点)


## 什么是中介者模式？
中介者模式（`Mediator Pattern`）：定义一个中介对象来封装一系列对象之间的交互，使原有对象之间的耦合松散，且可以独立地改变它们之间的交互。

中介者模式就是用于将类与类之间的`多对多关系`简化成`多对一`、`一对多关系`的设计模式


## 什么时候使用?
- 一组对象以定义明确但复杂的方式进行通信

- 重用一个对象很困难，因为它引用了许多其他对象并与之通信

- 分布在多个类之间的行为应该是可定制的，无需大量子类化


## 代码实现
```c++
#include <iostream>
#include <vector>

class Mediator;

class BaseComponent {
public:
    BaseComponent(Mediator* const mediator,const unsigned int id) : m_mediator(mediator),m_id(id) {}

    virtual ~BaseComponent() {}

    unsigned int getId() {
        return m_id;
    }

    virtual void send(std::string) = 0;

    virtual void receive(std::string) = 0;

protected:
    unsigned int m_id;
    Mediator* m_mediator;
};

class ConcreteComponent : public BaseComponent {
public:
    ConcreteComponent(Mediator* const mediator,unsigned int id) : BaseComponent(mediator,id) {}

    ~ConcreteComponent() {}

    void send(std::string msg);

    // 接收
    void receive(std::string msg) {
        std::cout << "message is " << msg << " received by id is " << m_id << std::endl;
    }
};

//  定义与基础组件对象通信的接口
class Mediator {
public:
    virtual ~Mediator() {}

    virtual void add(BaseComponent* const base) = 0;

    virtual void distribute(BaseComponent* const sender, std::string msg) = 0;

protected:
    Mediator() {}
};


// 通过协调对象并了解其他组件对象来实现合作行为
class ConcreteMediator : public Mediator {
public:
    ~ConcreteMediator() {
        for (unsigned int i = 0; i < m_base.size(); i++) {
            delete m_base[ i ];
        }
        m_base.clear();
    }

    void add(BaseComponent* const c) {
        m_base.push_back(c);
    }

    void distribute(BaseComponent* const sender, std::string msg) {
        for (unsigned int i = 0; i < m_base.size(); i++ ) {
            if (m_base.at(i)->getId() != sender->getId()) {
                m_base.at(i)->receive(msg);
            }
        }
    }

private:
    std::vector<BaseComponent*> m_base;
};


void ConcreteComponent::send(std::string msg){
    std::cout << "message '"<< msg << "' sent by Compnent " << m_id << std::endl;
    m_mediator->distribute(this, msg);
}


int main()
{
    Mediator *mediator = new ConcreteMediator();

    BaseComponent* c1 = new ConcreteComponent(mediator,1);
    BaseComponent* c2 = new ConcreteComponent(mediator,2);
    BaseComponent* c3 = new ConcreteComponent(mediator,3);

    mediator->add(c1);
    mediator->add(c2);
    mediator->add(c3);

    c1->send( "Hi!" );
    c3->send( "Hello!" );

    delete mediator;
    mediator = nullptr;

    return 0;
```
输出结果:
```sh
message 'Hi!' sent by Compnent 1
message is Hi! received by id is 2
message is Hi! received by id is 3
message 'Hello!' sent by Compnent 3
message is Hello! received by id is 1
message is Hello! received by id is 2
```

## 中介者模式的优缺点
-  单一职责原则。 可以将多个组件间的交流抽取到同一位置， 使其更易于理解和维护。
- 开闭原则。 无需修改实际组件就能增加新的中介者。
- 可以减轻应用中多个组件间的耦合情况。
- 可以更方便地复用各个组件。




**[⬆ 返回顶部](#目录)**


