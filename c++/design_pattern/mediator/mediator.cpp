#include <iostream>
#include <vector>

class Mediator;

class BaseComponent {
public:
    BaseComponent(Mediator* const mediator,const unsigned int id) : m_mediator(mediator),m_id(id) {}

    virtual ~BaseComponent() {}

    unsigned int getId() {
        return m_id;
    }

    virtual void send(std::string) = 0;

    virtual void receive(std::string) = 0;

protected:
    unsigned int m_id;
    Mediator* m_mediator;
};

class ConcreteComponent : public BaseComponent {
public:
    ConcreteComponent(Mediator* const mediator,unsigned int id) : BaseComponent(mediator,id) {}

    ~ConcreteComponent() {}

    void send(std::string msg);

    // 接收
    void receive(std::string msg) {
        std::cout << "message is " << msg << " received by id is " << m_id << std::endl;
    }
};

//  定义与基础组件对象通信的接口
class Mediator {
public:
    virtual ~Mediator() {}

    virtual void add(BaseComponent* const base) = 0;

    virtual void distribute(BaseComponent* const sender, std::string msg) = 0;

protected:
    Mediator() {}
};


// 通过协调对象并了解其他组件对象来实现合作行为
class ConcreteMediator : public Mediator {
public:
    ~ConcreteMediator() {
        for (unsigned int i = 0; i < m_base.size(); i++) {
            delete m_base[ i ];
        }
        m_base.clear();
    }

    void add(BaseComponent* const c) {
        m_base.push_back(c);
    }

    void distribute(BaseComponent* const sender, std::string msg) {
        for (unsigned int i = 0; i < m_base.size(); i++ ) {
            if (m_base.at(i)->getId() != sender->getId()) {
                m_base.at(i)->receive(msg);
            }
        }
    }

private:
    std::vector<BaseComponent*> m_base;
};


void ConcreteComponent::send(std::string msg){
    std::cout << "message '"<< msg << "' sent by Compnent " << m_id << std::endl;
    m_mediator->distribute(this, msg);
}


int main()
{
    Mediator *mediator = new ConcreteMediator();

    BaseComponent* c1 = new ConcreteComponent(mediator,1);
    BaseComponent* c2 = new ConcreteComponent(mediator,2);
    BaseComponent* c3 = new ConcreteComponent(mediator,3);

    mediator->add(c1);
    mediator->add(c2);
    mediator->add(c3);

    c1->send( "Hi!" );
    c3->send( "Hello!" );

    delete mediator;
    mediator = nullptr;

    return 0;
}
