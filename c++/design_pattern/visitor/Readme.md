

# C++设计模式之访问者模式
![](https://img.shields.io/badge/standard--readme-MIT-green?style=flat&logo=appveyor)  


## 目录

* [什么是访问者模式？](#什么是访问者模式)
* [什么时候使用?](#什么时候使用)
* [代码实现](#代码实现)
* [访问者模式的优缺点](#访问者模式的优缺点)
* [与其他模式的关系](#与其他模式的关系)


## 什么是访问者模式？


访问者模式（`Visitor Pattern`）：表示一个作用于某对象结构中的各元素的操作。它使你可以在不改变各元素的类的前提下定义作用于这些元素的新操作。


访问者模式的核心思想：将数据的结构和对数据的操作分离。

## 什么时候使用？
- 一个对象结构包含许多具有不同接口的对象类，并且希望对这些对象执行依赖于它们具体类的操作

- 需要对对象结构中的对象执行许多不同且不相关的操作，并且希望避免使用这些操作“污染”它们的类

- 定义对象结构的类很少改变，但经常想在结构上定义新的操作

## 代码实现
```c++
#include <iostream>


class ConcreteElementA;
class ConcreteElementB;

class Visitor {
public:
    virtual ~Visitor() {}

    virtual void visitElementA(ConcreteElementA* const element) = 0;

    virtual void visitElementB(ConcreteElementB* const element) = 0;
};


class ConcreteVisitor1 : public Visitor {
public:
    ~ConcreteVisitor1() {}

    void visitElementA(ConcreteElementA* const) {
        std::cout << "Concrete Visitor 1: Element A visited." << std::endl;
    }

    void visitElementB(ConcreteElementB* const) {
        std::cout << "Concrete Visitor 1: Element B visited." << std::endl;
    }
    // ...
};


class ConcreteVisitor2 : public Visitor {
public:
    ~ConcreteVisitor2() {}

    void visitElementA(ConcreteElementA* const) {
        std::cout << "Concrete Visitor 2: Element A visited." << std::endl;
    }

    void visitElementB(ConcreteElementB* const) {
        std::cout << "Concrete Visitor 2: Element B visited." << std::endl;
    }
    // ...
};

class Element {
public:
    virtual ~Element() {}

    virtual void accept(Visitor& visitor) = 0;
    // ...
};

class ConcreteElementA : public Element {
public:
    ~ConcreteElementA() {}

    void accept(Visitor &visitor) {
        visitor.visitElementA(this);
    }
    // ...
};

class ConcreteElementB : public Element {
public:
    ~ConcreteElementB() {}

    void accept( Visitor &visitor ) {
        visitor.visitElementB(this);
    }
    // ...
};


int main()
{
    ConcreteElementA elementA;
    ConcreteElementB elementB;

    ConcreteVisitor1 visitor1;
    ConcreteVisitor2 visitor2;

    elementA.accept(visitor1);
    elementA.accept(visitor2);

    elementB.accept(visitor1);
    elementB.accept(visitor2);
    return 0;
}
```

## 访问者模式的优缺点
优点:
- 开闭原则。 可以引入在不同类对象上执行的新行为， 且无需对这些类做出修改。

- 单一职责原则。 可将同一行为的不同版本移到同一个类中。

- 访问者对象可以在与各种对象交互时收集一些有用的信息。 当想要遍历一些复杂的对象结构 （例如对象树）， 并在结构中的每个对象上应用访问者时， 这些信息可能会有所帮助。

缺点:

- 每次在元素层次结构中添加或移除一个类时， 都要更新所有的访问者。

- 访问者同某个元素进行交互时， 它们可能没有访问元素私有成员变量和方法的必要权限。

## 与其他模式的关系
- 可以将访问者模式视为命令模式的加强版本， 其对象可对不同类的多种对象执行操作。

- 可以使用访问者对整个组合模式树执行操作。

- 可以同时使用访问者和迭代器模式来遍历复杂数据结构， 并对其中的元素执行所需操作， 即使这些元素所属的类完全不同。



**[⬆ 返回顶部](#目录)**
