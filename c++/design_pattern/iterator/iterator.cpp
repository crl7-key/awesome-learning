#include <iostream>
#include <vector>

// 提供所有迭代器必须实现的接口和一组遍历元素的方法
class Iterator {
public:
    virtual ~Iterator() { /* ... */ }

    virtual void first() = 0;

    virtual void next() = 0;

    virtual bool isDone() const = 0;

    virtual int currentItem() const = 0;
};


// 定义聚合接口，并将客户端与对象集合的实现分离
class Aggregate {
public:
    virtual ~Aggregate() {}
    virtual Iterator *createIterator() = 0;
};

// 有对象集合并实现方法为其集合返回一个迭代器
class ConcreteAggregate : public Aggregate {
public:
    ConcreteAggregate(const unsigned int size) {
        m_list = new int[size]();
        m_cnt = size;
    }

    ~ConcreteAggregate() {
        delete [] m_list;
    }

    Iterator* createIterator();

    unsigned int size() const {
        return m_cnt;
    }

    int at(unsigned int index) {
        return m_list[index];
    }

private:
    int* m_list;
    unsigned int m_cnt;
};


// 实现接口并负责管理 迭代器的当前位置
class ConcreteIterator : public Iterator {
public:
    ConcreteIterator(ConcreteAggregate* l) : m_list(l),m_index(0) {}

    ~ConcreteIterator() {}

    void first() {
        m_index = 0;
    }

    void next() {
        m_index++;
    }

    bool isDone() const {
        return (m_index >= m_list->size());
    }

    int currentItem() const {
        if (isDone()) {
            return -1;
        }
        return m_list->at(m_index);
    }

private:
    ConcreteAggregate* m_list;
    unsigned int m_index;
};

Iterator *ConcreteAggregate::createIterator() {
  return new ConcreteIterator( this );
}

int main()
{
  unsigned int size = 10;
  ConcreteAggregate list = ConcreteAggregate(size);

  Iterator *it = list.createIterator();
  for ( ; !it->isDone(); it->next()) {
    std::cout << "Item value: " << it->currentItem() << std::endl;
  }

  delete it;
  return 0;
}
