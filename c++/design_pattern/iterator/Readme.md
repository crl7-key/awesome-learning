# C++设计模式之迭代器模式
![](https://img.shields.io/badge/standard--readme-MIT-green?style=flat&logo=appveyor)  


## 目录

* [什么是迭代器模式？](#什么是迭代器模式)
* [什么时候使用?](#什么时候使用)
* [代码实现](#代码实现)
* [迭代器模式优缺点](#迭代器模式优缺点)
* [与其他模式的关系](#与其他模式的关系)


## 什么是迭代器模式？

迭代器模式（`Iterator Pattern`）：提供一种方法访问一个容器对象中各个元素，而又不需暴露该对象的内部细节。

## 什么时候使用?
- 在不暴露其内部表示的情况下访问聚合对象的内容

- 支持聚合对象的多次遍历

- 为遍历不同的聚合结构提供统一的接口（以支持多态迭代）

## 代码实现
```c++
#include <iostream>
#include <vector>

// 提供所有迭代器必须实现的接口和一组遍历元素的方法
class Iterator {
public:
    virtual ~Iterator() { /* ... */ }

    virtual void first() = 0;

    virtual void next() = 0;

    virtual bool isDone() const = 0;

    virtual int currentItem() const = 0;
};


// 定义聚合接口，并将客户端与对象集合的实现分离
class Aggregate {
public:
    virtual ~Aggregate() {}
    virtual Iterator *createIterator() = 0;
};

// 有对象集合并实现方法为其集合返回一个迭代器
class ConcreteAggregate : public Aggregate {
public:
    ConcreteAggregate(const unsigned int size) {
        m_list = new int[size]();
        m_cnt = size;
    }

    ~ConcreteAggregate() {
        delete [] m_list;
    }

    Iterator* createIterator();

    unsigned int size() const {
        return m_cnt;
    }

    int at(unsigned int index) {
        return m_list[index];
    }

private:
    int* m_list;
    unsigned int m_cnt;
};


// 实现接口并负责管理 迭代器的当前位置
class ConcreteIterator : public Iterator {
public:
    ConcreteIterator(ConcreteAggregate* l) : m_list(l),m_index(0) {}

    ~ConcreteIterator() {}

    void first() {
        m_index = 0;
    }

    void next() {
        m_index++;
    }

    bool isDone() const {
        return (m_index >= m_list->size());
    }

    int currentItem() const {
        if (isDone()) {
            return -1;
        }
        return m_list->at(m_index);
    }

private:
    ConcreteAggregate* m_list;
    unsigned int m_index;
};

Iterator *ConcreteAggregate::createIterator() {
  return new ConcreteIterator( this );
}

int main()
{
  unsigned int size = 10;
  ConcreteAggregate list = ConcreteAggregate(size);

  Iterator *it = list.createIterator();
  for ( ; !it->isDone(); it->next()) {
    std::cout << "Item value: " << it->currentItem() << std::endl;
  }

  delete it;
  return 0;
}
```
## 迭代器模式优缺点
优点：
- 单一职责原则。 通过将体积庞大的遍历算法代码抽取为独立的类， 可对客户端代码和集合进行整理。

- 开闭原则。 可实现新型的集合和迭代器并将其传递给现有代码， 无需修改现有代码。

- 可以并行遍历同一集合， 因为每个迭代器对象都包含其自身的遍历状态。

- 相似的， 可以暂停遍历并在需要时继续。


缺点：

- 如果程序只与简单的集合进行交互， 应用该模式可能会矫枉过正。
 
- 对于某些特殊集合， 使用迭代器可能比直接遍历的效率低

## 与其他模式的关系
- 可以使用迭代器模式来遍历组合模式树。

- 可以同时使用工厂方法模式和迭代器来让子类集合返回不同类型的迭代器， 并使得迭代器与集合相匹配。

- 可以同时使用备忘录模式和迭代器来获取当前迭代器的状态， 并且在需要的时候进行回滚。

- 可以同时使用访问者模式和迭代器来遍历复杂数据结构， 并对其中的元素执行所需操作， 即使这些元素所属的类完全不同。


**[⬆ 返回顶部](#目录)**
