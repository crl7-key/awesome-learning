#include <iostream>
#include <vector>


// 存储发起者对象的内部状态并防止被发起者以外的对象访问
class Memento {
public:
    Memento(const int state) : m_state(state) {}

    void setState(const int state) {
        m_state = state;
    }

    int getState() {
        return m_state;
    }

private:
    int m_state;
};

// 创建一个包含其当前内部状态快照的备忘录，并使用该备忘录恢复其内部状态
class Originator {
public:
    void setState(const int state) {
        std::cout << "Set state to " << state << " ." << std::endl;
        m_state = state;
    }


    int getState() {
        return m_state;
    }

    void setMemento(Memento* const m) {
        m_state = m->getState();
    }

    Memento* createMemento() {
        return new Memento(m_state);
    }
private:
    int m_state;
};


class CareTaker {
public:
    CareTaker(Originator* const originator) : m_originator(originator) {}

    ~CareTaker() {
        for(unsigned int i = 0; i < m_history.size(); i++) {
            delete m_history.at(i);
        }
        m_history.clear();
    }

    void save() {
        std::cout << "Save state. " << std::endl;
        m_history.push_back(m_originator->createMemento());
    }

    void undo() {
        if (m_history.empty()) {
            std::cout << "Unable to undo state." << std::endl;
            return;
        }
        Memento* m = m_history.back();
        m_originator->setMemento(m);
        std::cout << "Unable state." << std::endl;
        m_history.pop_back();
        delete m;
        m = nullptr;
    }

private:
    Originator* m_originator;
    std::vector<Memento*> m_history;
};

int main()
{
    Originator *originator = new Originator();
    CareTaker *caretaker = new CareTaker(originator);

    originator->setState(1);
    caretaker->save();

    originator->setState(2);
    caretaker->save();

    originator->setState(3);
    caretaker->undo();

    std::cout << "Actual state is " << originator->getState() << "." << std::endl;

    delete originator;
    delete caretaker;

    return 0;
}
