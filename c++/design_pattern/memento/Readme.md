# C++设计模式之备忘录模式
![](https://img.shields.io/badge/standard--readme-MIT-green?style=flat&logo=appveyor)  


## 目录

* [什么是备忘录模式？](#什么是备忘录模式)
* [什么时候使用?](#什么时候使用)
* [代码实现](#代码实现)
* [备忘录模式的优缺点](#备忘录模式的优缺点)
* [与其他模式的关系](#与其他模式的关系)



## 什么是备忘录模式？
备忘录模式：在不破坏封装的条件下，通过备忘录对象存储另外一个对象内部状态的快照，在将来合适的时候把这个对象还原到存储起来的状态。

## 什么时候使用?
- 必须保存对象状态的快照，以便稍后可以将其恢复到该状态

- 获取状态的直接接口会暴露实现细节并破坏对象的封装

## 代码实现
```c++
#include <iostream>
#include <vector>


// 存储发起者对象的内部状态并防止被发起者以外的对象访问
class Memento {
public:
    Memento(const int state) : m_state(state) {}

    void setState(const int state) {
        m_state = state;
    }

    int getState() {
        return m_state;
    }

private:
    int m_state;
};

// 创建一个包含其当前内部状态快照的备忘录，并使用该备忘录恢复其内部状态
class Originator {
public:
    void setState(const int state) {
        std::cout << "Set state to " << state << " ." << std::endl;
        m_state = state;
    }


    int getState() {
        return m_state;
    }

    void setMemento(Memento* const m) {
        m_state = m->getState();
    }

    Memento* createMemento() {
        return new Memento(m_state);
    }
private:
    int m_state;
};


class CareTaker {
public:
    CareTaker(Originator* const originator) : m_originator(originator) {}

    ~CareTaker() {
        for(unsigned int i = 0; i < m_history.size(); i++) {
            delete m_history.at(i);
        }
        m_history.clear();
    }

    void save() {
        std::cout << "Save state. " << std::endl;
        m_history.push_back(m_originator->createMemento());
    }

    void undo() {
        if (m_history.empty()) {
            std::cout << "Unable to undo state." << std::endl;
            return;
        }
        Memento* m = m_history.back();
        m_originator->setMemento(m);
        std::cout << "Unable state." << std::endl;
        m_history.pop_back();
        delete m;
        m = nullptr;
    }

private:
    Originator* m_originator;
    std::vector<Memento*> m_history;
};

int main()
{
    Originator *originator = new Originator();
    CareTaker *caretaker = new CareTaker(originator);

    originator->setState(1);
    caretaker->save();

    originator->setState(2);
    caretaker->save();

    originator->setState(3);
    caretaker->undo();

    std::cout << "Actual state is " << originator->getState() << "." << std::endl;

    delete originator;
    delete caretaker;

    return 0;
}
```


## 备忘录模式的优缺点

备忘录模式的优点是：

- 给用户提供了一种可以恢复状态的机制，使用户能够比较方便的回到某个历史的状态
- 实现了信息的封装，使得用户不需要关心状态的保存细节

缺点：

- 消耗资源，如果类的成员变量过多，势必会占用比较大的资源，而且每一次保存都会消耗一定的内存。

## 与其他模式的关系
- 可以同时使用命令模式和备忘录模式来实现 “撤销”。 在这种情况下， 命令用于对目标对象执行各种不同的操作， 备忘录用来保存一条命令执行前该对象的状态。

- 可以同时使用备忘录和迭代器模式来获取当前迭代器的状态， 并且在需要的时候进行回滚。

- 有时候原型模式可以作为备忘录的一个简化版本， 其条件是需要在历史记录中存储的对象的状态比较简单， 不需要链接其他外部资源， 或者链接可以方便地重建。

**[⬆ 返回顶部](#目录)**
