# C++设计模式之模板方法模式
![](https://img.shields.io/badge/standard--readme-MIT-green?style=flat&logo=appveyor)  


## 目录

* [什么是模板方法模式？](#什么是模板方法模式)
* [什么时候使用?](#什么时候使用)
* [代码实现](#代码实现)
* [模板方法模式的优缺点](#模板方法模式的优缺点)
* [与其他模式的关系](#与其他模式的关系)


## 什么是模板方法模式？
模板方法模式（`Template Method Pattern`）：定义一个操作中的算法的骨架，而将一些步骤延迟到子类中。模板方法使得子类可以不改变一个算法的结构即可重定义该算法的某些特定步骤。

通俗地说，模板方法模式就是一个关于继承的设计模式。

每一个被继承的父类都可以认为是一个模板，它的某些步骤是稳定的，某些步骤被延迟到子类中实现。


## 什么时候使用？
- 将算法的不变部分实现一次，并将其留给子类来实现可以变化的行为

- 当子类之间的共同行为应该在一个共同的类中分解和本地化以避免代码重复时

- 控制子类扩展

## 代码实现
```c++
#include <iostream>


class AbstractClass {
public:
    virtual ~AbstractClass() {}

    void templateMethod() {
        primitiveOperation1();

        primitiveOperation2();

    }

    virtual void primitiveOperation1() = 0;

    virtual void primitiveOperation2() = 0;
};



class ConcreteClass : public AbstractClass {
public:
    ~ConcreteClass() {}

    void primitiveOperation1() {
        std::cout << "Primitive operation 1" << std::endl;
        // ...
    }

    void primitiveOperation2() {
        std::cout << "Primitive operation 2" << std::endl;
        // ...
    }
    // ...
};

int main()
{

    AbstractClass* base = new ConcreteClass();

    base->templateMethod();

    delete base;
    base = nullptr;
    return 0;
}
```

## 模板方法模式的优缺点
优点:

- 可仅允许客户端重写一个大型算法中的特定部分， 使得算法其他部分修改对其所造成的影响减小。

- 可将重复代码提取到一个超类中。

缺点:

- 部分客户端可能会受到算法框架的限制。

- 通过子类抑制默认步骤实现可能会导致违反_里氏替换原则_。

- 模板方法中的步骤越多， 其维护工作就可能会越困难。

## 与其他模式的关系
- 工厂方法模式是模板方法模式的一种特殊形式。 同时， 工厂方法可以作为一个大型模板方法中的一个步骤。

- 模板方法基于继承机制： 它允许通过扩展子类中的部分内容来改变部分算法。 策略模式基于组合机制： 可以通过对相应行为提供不同的策略来改变对象的部分行为。 模板方法在类层次上运作， 因此它是静态的。 策略在对象层次上运作， 因此允许在运行时切换行为。




**[⬆ 返回顶部](#目录)**
