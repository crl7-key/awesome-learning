#include <iostream>

class State {
public:
    virtual ~State() {}

    virtual void handler()=0;
};


class ConcreteStateA : public State {
public:
    virtual ~ConcreteStateA() { /* ... */ }

    void handler() {
        std::cout << "State A handled." << std::endl;
    }
};

class ConcreteStateB : public State {
public:
    virtual ~ConcreteStateB() { /* ... */ }

    void handler() {
        std::cout << "State B handled." << std::endl;
    }
    // ...
};


class Context {
public:
    virtual ~Context() {
        if(m_state) {
            delete m_state;
            m_state = nullptr;
        }
    }

    Context():m_state() { /* ... */ }

    void setState(State* const s) {
        if (m_state) {
            delete m_state;
        }
        m_state = s;
    }

    void request() {
        m_state->handler();
    }

private:
    State* m_state;
};

int main()
{
    Context *context = new Context();

    context->setState(new ConcreteStateA());
    context->request();

    context->setState(new ConcreteStateB());
    context->request();

    delete context;

    return 0;
}

