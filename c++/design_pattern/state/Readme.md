
# C++设计模式之状态模式
![](https://img.shields.io/badge/standard--readme-MIT-green?style=flat&logo=appveyor)  


## 目录

* [什么是状态模式？](#什么是状态模式)
* [什么时候使用?](#什么时候使用)
* [代码实现](#代码实现)
* [状态模式的优缺点](#状态模式的优缺点)
* [与其他模式的关系](#与其他模式的关系)

## 什么是状态模式？
状态模式（`State Pattern`）：当一个对象的内在状态改变时允许改变其行为，这个对象看起来像是改变了其类。

## 什么时候使用?
- 当一个对象的行为取决于它的状态，并且它必须在运行时根据那个状态改变它的行为

- 操作具有依赖于对象状态的大型、多部分条件语句

## 代码实现
```c++
#include <iostream>

class State {
public:
    virtual ~State() {}

    virtual void handler()=0;
};


class ConcreteStateA : public State {
public:
    virtual ~ConcreteStateA() { /* ... */ }

    void handler() {
        std::cout << "State A handled." << std::endl;
    }
};

class ConcreteStateB : public State {
public:
    virtual ~ConcreteStateB() { /* ... */ }

    void handler() {
        std::cout << "State B handled." << std::endl;
    }
    // ...
};


class Context {
public:
    virtual ~Context() {
        if(m_state) {
            delete m_state;
            m_state = nullptr;
        }
    }

    Context():m_state() { /* ... */ }

    void setState(State* const s) {
        if (m_state) {
            delete m_state;
        }
        m_state = s;
    }

    void request() {
        m_state->handler();
    }

private:
    State* m_state;
};

int main()
{
    Context *context = new Context();

    context->setState(new ConcreteStateA());
    context->request();

    context->setState(new ConcreteStateB());
    context->request();

    delete context;

    return 0;
}
```
## 状态模式的优缺点
优点:

- 单一职责原则。 将与特定状态相关的代码放在单独的类中。

- 开闭原则。 无需修改已有状态类和上下文就能引入新状态。

- 通过消除臃肿的状态机条件语句简化上下文代码。

缺点:
- 如果状态机只有很少的几个状态， 或者很少发生改变， 那么应用该模式可能会显得小题大作。


## 与其他模式的关系
- 桥接模式、 状态模式和策略模式 （在某种程度上包括适配器模式） 模式的接口非常相似。 实际上， 它们都基于组合模式——即将工作委派给其他对象， 不过也各自解决了不同的问题。 模式并不只是以特定方式组织代码的配方， 还可以使用它们来和其他开发者讨论模式所解决的问题。

- 状态可被视为策略的扩展。 两者都基于组合机制： 它们都通过将部分工作委派给 “帮手” 对象来改变其在不同情景下的行为。 策略使得这些对象相互之间完全独立， 它们不知道其他对象的存在。 但状态模式没有限制具体状态之间的依赖， 且允许它们自行改变在不同情景下的状态。


**[⬆ 返回顶部](#目录)**
