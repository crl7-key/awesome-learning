# C++设计模式之策略模式
![](https://img.shields.io/badge/standard--readme-MIT-green?style=flat&logo=appveyor)  


## 目录

* [什么是策略模式？](#什么是策略模式)
* [什么时候使用?](#什么时候使用)
* [代码实现](#代码实现)
* [策略模式的优缺点](#策略模式的优缺点)
* [与其他模式的关系](#与其他模式的关系)


## 什么是策略模式？

策略模式（`Strategy Pattern`）：定义了一系列算法，并将每一个算法封装起来，而且使它们还可以相互替换。策略模式让算法独立于使用它的客户而独立变化。

策略模式用一个成语就可以概括 —— 殊途同归。当我们做同一件事有多种方法时，就可以将每种方法封装起来，在不同的场景选择不同的策略，调用不同的方法。

## 什么时候使用？
- 许多相关的类仅在行为上有所不同

- 需要算法的不同变体

- 算法使用客户不应该知道的数据

## 代码实现
```c++
#include <iostream>

class Strategy {
public:
    virtual ~Strategy() {}

    virtual void algorithmInterface() = 0;
};


class ConcreteStrategyA : public Strategy {
public:
    virtual ~ConcreteStrategyA() { /*...*/ }

    void algorithmInterface() {
        std::cout << " ConcreteStrategyA " << std::endl;
    }
};

class ConcreteStrategyB : public Strategy {
public:
    virtual ~ConcreteStrategyB() { /*...*/  }

    void algorithmInterface() {
        std::cout << " ConcreteStrategyB " << std::endl; 
    }
};

class ConcreteStrategyC : public Strategy {
public:
    virtual ~ConcreteStrategyC() {  /*...*/  }

    void algorithmInterface() {
        std::cout << " ConcreteStrategyC " << std::endl;
    }
};


class Context {
public:
    Context(Strategy* const strategy) : m_strategy(strategy) {}

    ~Context() {
        if (m_strategy) {
            delete m_strategy;
            m_strategy = nullptr;
        }
    }

    void contextInterface() {
        m_strategy->algorithmInterface();
    }

private:
    Strategy* m_strategy;
};

int main()
{

    Context context(new ConcreteStrategyA());
    Context ctx(new ConcreteStrategyB());

    context.contextInterface();
    ctx.contextInterface();

    return 0;
}
```
## 策略模式的优缺点
优点:
- 可以在运行时切换对象内的算法。
 
- 可以将算法的实现和使用算法的代码隔离开来。
 
- 可以使用组合来代替继承。

- 开闭原则。 无需对上下文进行修改就能够引入新的策略。

缺点:
- 如果算法极少发生改变， 那么没有任何理由引入新的类和接口。 使用该模式只会让程序过于复杂。
 
- 客户端必须知晓策略间的不同——它需要选择合适的策略。

## 与其他模式的关系
- 桥接模式、 状态模式和策略模式 （在某种程度上包括适配器模式） 模式的接口非常相似。 实际上， 它们都基于组合模式——即将工作委派给其他对象， 不过也各自解决了不同的问题。 模式并不只是以特定方式组织代码的配方， 你还可以使用它们来和其他开发者讨论模式所解决的问题。

- 命令模式和策略看上去很像， 因为两者都能通过某些行为来参数化对象。 但是， 它们的意图有非常大的不同。

   - 可以使用命令来将任何操作转换为对象。 操作的参数将成为对象的成员变量。 你可以通过转换来延迟操作的执行、 将操作放入队列、 保存历史命令或者向远程服务发送命令等。

   - 另一方面， 策略通常可用于描述完成某件事的不同方式， 让你能够在同一个上下文类中切换算法。

- 装饰模式可让你更改对象的外表， 策略则让你能够改变其本质。

- 模板方法模式基于继承机制： 它允许通过扩展子类中的部分内容来改变部分算法。 策略基于组合机制： 可以通过对相应行为提供不同的策略来改变对象的部分行为。 模板方法在类层次上运作， 因此它是静态的。 策略在对象层次上运作， 因此允许在运行时切换行为。

- 状态可被视为策略的扩展。 两者都基于组合机制： 它们都通过将部分工作委派给 “帮手” 对象来改变其在不同情景下的行为。 策略使得这些对象相互之间完全独立， 它们不知道其他对象的存在。 但状态模式没有限制具体状态之间的依赖， 且允许它们自行改变在不同情景下的状态。

- 使用策略模式时，程序只需选择一种策略就可以完成某件事。也就是说每个策略类都是完整的，都能独立完成这件事情，如上文所言，强调的是殊途同归。

- 使用状态模式时，程序需要在不同的状态下不断切换才能完成某件事，每个状态类只能完成这件事的一部分，需要所有的状态类组合起来才能完整的完成这件事，强调的是随势而动。



**[⬆ 返回顶部](#目录)**
