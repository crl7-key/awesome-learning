#include <iostream>

class Isort {
public:
    virtual ~Isort() {}

    virtual void algorithmInterface(int arr[],int len) = 0;
};

class BubbleSort : public Isort {
public:
    ~BubbleSort() {}

    void algorithmInterface(int arr[],int len) {
        bool swaped;
        do
        {
            swaped = false;
            for(int i = 1; i < len; i++) {
                if(arr[i-1] > arr[i]) {
                    std::swap(arr[i-1],arr[i]);
                    swaped = true;
                }
            }
        }while(swaped);
    }
};

class SelectionSort : public Isort {
public:
    ~SelectionSort() {}

    void algorithmInterface(int arr[],int len) {
        for(int i = 0;i < len;i++ ) {
            // 寻找[i,leng]中区间的最小值
            int minIndex = i;
            for(int j = i;j < len;j++)
                if (arr[j] < arr[minIndex])
                    minIndex = j;

            // swap(arr[i],arr[minIndex]);
            std::swap(arr[i],arr[minIndex]);
        }
    }
};

class InsertionSort : public Isort {
public:
    ~InsertionSort() {}

    void algorithmInterface(int arr[],int len) {
        // 第1个数肯定是有序的,从第2个数开始遍历,依次插入有序序列
        for(int i = 1;i < len;i++) {
            // 取出第i个数,和前i-1个数比较后,插入合适位置
            int tmp = arr[i];
            int j;
            for( j = i;j > 0 && arr[j-1] > tmp;j--)
                arr[j] = arr[j-1];
            arr[j] = tmp;
        }

    }
};

class Context {
public:
    Context(Isort* const sort) : m_sort(sort) {}


    ~Context() {
        if (m_sort) {
            delete m_sort;
            m_sort = nullptr;
        }
    }

    void contextInterface(int arr[],int len) {
        m_sort->algorithmInterface(arr,len);
    }
private:
    Isort* m_sort;
};


int main()
{

    int arr[] =  {6, 1, 2, 3, 5, 4};

    Context ctx(new BubbleSort());
    ctx.contextInterface(arr,6);

    for(unsigned int i = 0; i < 7; i++) {
        std::cout << i << " ";
    }
    std::cout << std::endl;

    return 0;
}
