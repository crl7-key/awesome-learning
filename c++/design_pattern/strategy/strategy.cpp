#include <iostream>

class Strategy {
public:
    virtual ~Strategy() {}

    virtual void algorithmInterface() = 0;
};


class ConcreteStrategyA : public Strategy {
public:
    virtual ~ConcreteStrategyA() { /*...*/ }

    void algorithmInterface() {
        std::cout << " ConcreteStrategyA " << std::endl;
    }
};

class ConcreteStrategyB : public Strategy {
public:
    virtual ~ConcreteStrategyB() { /*...*/  }

    void algorithmInterface() {
        std::cout << " ConcreteStrategyB " << std::endl; 
    }
};

class ConcreteStrategyC : public Strategy {
public:
    virtual ~ConcreteStrategyC() {  /*...*/  }

    void algorithmInterface() {
        std::cout << " ConcreteStrategyC " << std::endl;
    }
};


class Context {
public:
    Context(Strategy* const strategy) : m_strategy(strategy) {}

    ~Context() {
        if (m_strategy) {
            delete m_strategy;
            m_strategy = nullptr;
        }
    }

    void contextInterface() {
        m_strategy->algorithmInterface();
    }

private:
    Strategy* m_strategy;
};

int main()
{

    Context context(new ConcreteStrategyA());
    Context ctx(new ConcreteStrategyB());

    context.contextInterface();
    ctx.contextInterface();

    return 0;
}
