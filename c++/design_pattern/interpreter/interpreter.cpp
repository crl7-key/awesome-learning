#include <iostream>
#include <map>

// 在解释器模式中，将不可拆分的最小单元称之为终结表达式，可以被拆分的表达式称之为非终结表达式。

// 包含对解释器全局的信息
class Context {
public:
    void set(const std::string& var,const bool value) {
        m_vars.insert(std::pair<std::string,bool>(var ,value));
    }

    bool get(const std::string& exp) {
        return m_vars[exp];
    }

private:
    std::map<std::string,bool> m_vars;
};


// 声明抽象表达式 所有节点通用的抽象解释操作
class AbstractExpression {
public:
    virtual ~AbstractExpression() {}

    virtual bool interpret(Context* const){
        return false;
    }
};


// 终结表达式 : 不可拆分的最小单元 
class TerminalExpression : public AbstractExpression {
public:
    TerminalExpression(const std::string& value) {
        m_value = value;
    }

    virtual ~TerminalExpression() {}

    bool interpret(Context* const ctx) {
        return ctx->get(m_value);
    }

private:
    std::string m_value;
};

// 可以被拆分的表达式
class NonterminalExpression : public AbstractExpression {
public:

    NonterminalExpression(AbstractExpression* left, AbstractExpression* right): m_right(right), m_left(left) {}

    ~NonterminalExpression() {
        delete m_left;
        delete m_right;
        m_left = nullptr;
        m_right = nullptr;
    }

    bool interpret(Context* const ctx) {
        return m_left->interpret(ctx) && m_right->interpret(ctx);
    }

private:
    AbstractExpression* m_right;
    AbstractExpression* m_left;
};

int main() 
{
    AbstractExpression* ptrA = new TerminalExpression("A");
    AbstractExpression *ptrB = new TerminalExpression("B");
    AbstractExpression *exp = new NonterminalExpression(ptrA, ptrB);

    Context ctx;
    ctx.set( "A", true );
    ctx.set( "B", false );

    std::cout << ctx.get( "A" ) << " AND " << ctx.get( "B" );
    std::cout << " = " << exp->interpret(&ctx) << std::endl;

    delete exp;

    return 0;
}
