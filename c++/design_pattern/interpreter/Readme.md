
# C++设计模式之解释器模式
![](https://img.shields.io/badge/standard--readme-MIT-green?style=flat&logo=appveyor)  


## 目录

* [什么是解释器模式？](#什么是解释器模式)
* [什么时候使用?](#什么时候使用)
* [代码实现](#代码实现)
* [解释器模式的优缺点](#解释器模式的优缺点)


## 什么是解释器模式？
解释器模式（`Interpreter Pattern`）：给定一门语言，定义它的文法的一种表示，并定义一个解释器，该解释器使用该表示来解释语言中的句子。


## 什么时候使用?
- 当语法很简单时（如果语法复杂，有更好的选择）

- 效率不是关键问题


## 代码实现
```c++
#include <iostream>
#include <map>

// 在解释器模式中，将不可拆分的最小单元称之为终结表达式，可以被拆分的表达式称之为非终结表达式。

// 包含对解释器全局的信息
class Context {
public:
    void set(const std::string& var,const bool value) {
        m_vars.insert(std::pair<std::string,bool>(var ,value));
    }

    bool get(const std::string& exp) {
        return m_vars[exp];
    }

private:
    std::map<std::string,bool> m_vars;
};


// 声明抽象表达式 所有节点通用的抽象解释操作
class AbstractExpression {
public:
    virtual ~AbstractExpression() {}

    virtual bool interpret(Context* const){
        return false;
    }
};


// 终结表达式 : 不可拆分的最小单元 
class TerminalExpression : public AbstractExpression {
public:
    TerminalExpression(const std::string& value) {
        m_value = value;
    }

    virtual ~TerminalExpression() {}

    bool interpret(Context* const ctx) {
        return ctx->get(m_value);
    }

private:
    std::string m_value;
};

// 可以被拆分的表达式
class NonterminalExpression : public AbstractExpression {
public:

    NonterminalExpression(AbstractExpression* left, AbstractExpression* right): m_right(right), m_left(left) {}

    ~NonterminalExpression() {
        delete m_left;
        delete m_right;
        m_left = nullptr;
        m_right = nullptr;
    }

    bool interpret(Context* const ctx) {
        return m_left->interpret(ctx) && m_right->interpret(ctx);
    }

private:
    AbstractExpression* m_right;
    AbstractExpression* m_left;
};

int main() 
{
    AbstractExpression* ptrA = new TerminalExpression("A");
    AbstractExpression *ptrB = new TerminalExpression("B");
    AbstractExpression *exp = new NonterminalExpression(ptrA, ptrB);

    Context ctx;
    ctx.set( "A", true );
    ctx.set( "B", false );

    std::cout << ctx.get( "A" ) << " AND " << ctx.get( "B" );
    std::cout << " = " << exp->interpret(&ctx) << std::endl;

    delete exp;

    return 0;
}
```
## 解释器模式的优缺点

优点:
- 易于实现文法：在解释器模式中，一条语法规则用一个解释器对象来解释执行。对于解释器的实现来讲，功能就变得比较简单，只需要考虑这一条语法规则的实现就可以了，其他的都不用管。

- 易于扩展新的语法。由于解释器采用类来描述语法规则，因此可以通过继承等机制创建相应的解释器对象，在创建抽象语法树的时候使用这个新的解释器对象就可以了。

缺点:
- 执行效率较低。由于在解释器模式中使用了大量的循环和递归调用，因此在解释较为复杂的句子时其速度很慢，而且代码的调试过程也比较麻烦。

- 对于复杂文法难以维护。在解释器模式中，每一条规则至少需要定义一个类，因此如果一个语言包含太多文法规则，类的个数将会急剧增加，导致系统难以管理和维护，此时可以考虑使用语法分析程序等方式来取代解释器模式。

**[⬆ 返回顶部](#目录)**
