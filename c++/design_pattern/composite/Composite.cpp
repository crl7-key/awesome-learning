#include <iostream>
#include <vector>

// 为合成中的所有对象定义一个接口,复合节点和叶节点,在适当情况下实现所有类共有接口的默认行为
class Componet{
public:
    virtual ~Componet() {}

    virtual Componet* getChild(int) {
        return 0;
    }
    virtual void add(Componet*) { /*...*/ }

    virtual void remove(int) { /*...*/ }

    virtual void operation() = 0;
};

// 合成: 定义具有子元素的组件的行为并存储子组件
class Composite: public Componet {
public:
    virtual ~Composite() {
        for (unsigned int i = 0; i < m_children.size(); i++) {
            delete m_children[i];
            m_children[i] = nullptr;
        }
    }

    Componet* getChild(const int index){
        return m_children[index];
    }

    void add(Componet* componet){
        m_children.push_back(componet);
    }

    void remove(const int index) {
        Componet* child = m_children[index];
        m_children.erase(m_children.begin()+index);
        delete child;
        child = nullptr;
    }

    void operation() {
        for(unsigned int i = 0;i < m_children.size();i++) {
            m_children[i]->operation();
        }
    }

private:
    std::vector<Componet*> m_children;
};

// 叶子
// 定义合成中元素的行为,它没有孩子
class Leaf:public Componet {
public:
    ~Leaf(){}

    Leaf(const int i):m_id(i) {}

    void operation() override {
        std::cout << "Leaf " << m_id << " operation" << std::endl;
    }

private:
    int m_id;
};

int main()
{
    Composite composite;

    for(unsigned int i = 0; i < 5; i ++) {
        composite.add(new Leaf(i));
    }

    composite.remove(1);
    composite.operation();
    return 0;
}

