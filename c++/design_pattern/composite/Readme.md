#  C++设计模式之组合模式
![](https://img.shields.io/badge/standard--readme-MIT-green?style=flat&logo=appveyor)   

## 目录
* [什么是组合模式?](#什么是组合模式)
* [什么时候使用?](#什么时候使用)
* [代码实现](#代码实现)
* [与其他模式的关系](#与其他模式的关系)


## 什么是组合模式?

又叫部分整体模式,将对象组合成树状结构,并且能像使用独立对象一样使用他们,将对象组合到树结构中以表示部分以及整体层次结构。复合可以使客户统一对待单个对象和对象组成。组合模式用于整体与部分的结构,当整体与部分有相似的结构,在操作时可以被一致对待时,就可以使用组合模式。

例如：

- 文件夹和子文件夹的关系：文件夹中可以存放文件,也可以新建文件夹,子文件夹也一样。
- 总公司子公司的关系：总公司可以设立部门,也可以设立分公司,子公司也一样。
- 树枝和分树枝的关系：树枝可以长出叶子,也可以长出树枝,分树枝也一样。


## 什么时候使用?
- 想要表示对象的整体层次结构
- 希望客户能够忽略对象组成和单个对象之间的差异


## 代码实现
```c++
#include <iostream>
#include <vector>

// 为合成中的所有对象定义一个接口,复合节点和叶节点
class Componet{
public:
    virtual ~Componet() {}

    virtual Componet* getChild(int) {
        return 0;
    }
    virtual void add(Componet*) { /*...*/ }

    virtual void remove(int) { /*...*/ }

    virtual void operation() = 0;
};

// 定义具有子元素的组件的行为并存储子组件
class Composite: public Componet {
public:
    virtual ~Composite() {
        for (unsigned int i = 0; i < m_children.size(); i++) {
            delete m_children[i];
            m_children[i] = nullptr;
        }
    }

    Componet* getChild(const int index){
        return m_children[index];
    }

    void add(Componet* componet){
        m_children.push_back(componet);
    }

    void remove(const int index) {
        Componet* child = m_children[index];
        m_children.erase(m_children.begin()+index);
        delete child;
        child = nullptr;
    }

    void operation() {
        for(unsigned int i = 0;i < m_children.size();i++) {
            m_children[i]->operation();
        }
    }

private:
    std::vector<Componet*> m_children;
};

// 叶子
// 定义合成中元素的行为,它没有孩子
class Leaf:public Componet {
public:
    ~Leaf(){}

    Leaf(const int i):m_id(i) {}

    void operation() override {
        std::cout << "Leaf " << m_id << " operation" << std::endl;
    }

private:
    int m_id;
};

int main()
{
    Composite composite;

    for(unsigned int i = 0; i < 5; i ++) {
        composite.add(new Leaf(i));
    }

    composite.remove(1);
    composite.operation();
    return 0;
}


```

## 与其他模式的关系
- 桥接模式、 状态模式和策略模式 （在某种程度上包括适配器模式） 模式的接口非常相似。 实际上它们都基于组合模式——即将工作委派给其他对象, 不过也各自解决了不同的问题。 模式并不只是以特定方式组织代码的配方, 还可以使用它们来和其他开发者讨论模式所解决的问题。

- 可以在创建复杂组合树时使用生成器模式,因为这可使其构造步骤以递归的方式运行。

- 责任链模式通常和组合模式结合使用。 在这种情况下, 叶组件接收到请求后,可以将请求沿包含全体父组件的链一直传递至对象树的底部。

- 可以使用迭代器模式来遍历组合树。

- 可以使用访问者模式对整个组合树执行操作。

- 可以使用享元模式实现组合树的共享叶节点以节省内存。

- 组合和装饰模式的结构图很相似, 因为两者都依赖递归组合来组织无限数量的对象。

- 装饰类似于组合,但其只有一个子组件。 此外还有一个明显不同： 装饰为被封装对象添加了额外的职责, 组合仅对其子节点的结果进行了 “求和”。

但是,模式也可以相互合作： 可以使用装饰来扩展组合树中特定对象的行为。

- 大量使用组合和装饰的设计通常可从对于原型模式的使用中获益。 可以通过该模式来复制复杂结构,而非从零开始重新构造。


**[⬆ 返回顶部](#目录)**
