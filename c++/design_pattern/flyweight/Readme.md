#  C++设计模式之享元模式
![](https://img.shields.io/badge/standard--readme-MIT-green?style=flat&logo=appveyor)   

## 目录
* [什么是享元模式?](#什么是享元模式)
* [什么时候使用?](#什么时候使用)
* [代码实现](#代码实现)
* [与其他模式的关系](#与其他模式的关系)

## 什么是享元模式?
运用共享技术有效地支持大量细粒度对象的复用。系统只使用少量的对象,而这些对象都很相似,状态变化很小,可以实现对象的多次复用。由于享元模式要求能够共享的对象必须是细粒度对象,因此它又称为轻量级模式。当需要创建大量相似的对象时,可以使用该模式来减少内存使用量。

## 什么时候使用?
- 当一个类的一个实例可用于提供许多“虚拟实例”时
- 当满足以下所有条件时
  - 应用程序使用大量对象
  - 由于对象数量巨大,因此存储成本很高
  - 大多数对象状态可以是外部的
  - 一旦删除了外部状态，则可以用相对较少的共享对象替换许多对象组
  - 该应用程序不依赖于对象标识

## 代码实现
```c++
#include <iostream>
#include <map>

class Flyweight {
public:
    virtual ~Flyweight() {}
    virtual void operation()=0;
};

// 并非所有子类都需要共享
class UnsharedConcreteFlyweight : public Flyweight {
public:
    UnsharedConcreteFlyweight(int state):m_state(state) {}
    
    virtual ~UnsharedConcreteFlyweight() {}

    void operation() override {
        std::cout << "Unshared Flyweight with state " << m_state << std::endl;
    }
private:
    int m_state;
};

// 实现Flyweight接口
class ConcreteFlyweight : public Flyweight {
public:
    ConcreteFlyweight(const int all_state):m_state(all_state) {}

    virtual ~ConcreteFlyweight() {}

    void operation() override {
         std::cout << "Concrete Flyweight with state " << m_state << std::endl;
    }
private:
    int m_state;
};


// 创建和管理对象并确保正确共享
class FlyweightFactory {
public:
    ~FlyweightFactory() {
        for(auto iter = m_files.begin(); iter != m_files.end(); iter++) {
            delete iter->second;
        }
        m_files.clear();
    }

    Flyweight* getFlyweight(const int key) {
        if (m_files.find(key) != m_files.end()) {
            return m_files[key];
        }
        Flyweight* fly = new ConcreteFlyweight(key);
        m_files.insert(std::pair<int,Flyweight*>(key,fly));
        return fly;
    }

private:
    std::map<int,Flyweight*> m_files;
};

int main()
{

    FlyweightFactory* factory = new FlyweightFactory;
    factory->getFlyweight(1)->operation();
    factory->getFlyweight(2)->operation();

    delete factory;

    UnsharedConcreteFlyweight*uf = new UnsharedConcreteFlyweight(3);
    uf->operation();
    delete uf;

    return 0;
}
```


## 与其他模式的关系
- 可以使用享元模式实现组合模式树的共享叶节点以节省内存。

- 享元展示了如何生成大量的小型对象,外观模式则展示了如何用一个对象来代表整个子系统。

- 如果你能将对象的所有共享状态简化为一个享元对象, 那么享元就和单例模式类似了。 但这两个模式有两个根本性的不同。

- 单例只会有一个单例实体, 但是享元类可以有多个实体, 各实体的内在状态也可以不同。单例对象可以是可变的。 享元对象是不可变的。


**[⬆ 返回顶部](#目录)**
