#include <iostream>
#include <map>

class Flyweight {
public:
    virtual ~Flyweight() {}
    virtual void operation()=0;
};

// 并非所有子类都需要共享
class UnsharedConcreteFlyweight : public Flyweight {
public:
    UnsharedConcreteFlyweight(int state):m_state(state) {}
    
    virtual ~UnsharedConcreteFlyweight() {}

    void operation() override {
        std::cout << "Unshared Flyweight with state " << m_state << std::endl;
    }
private:
    int m_state;
};

// 实现Flyweight接口
class ConcreteFlyweight : public Flyweight {
public:
    ConcreteFlyweight(const int all_state):m_state(all_state) {}

    virtual ~ConcreteFlyweight() {}

    void operation() override {
         std::cout << "Concrete Flyweight with state " << m_state << std::endl;
    }
private:
    int m_state;
};


// 创建和管理对象并确保正确共享
class FlyweightFactory {
public:
    ~FlyweightFactory() {
        for(auto iter = m_files.begin(); iter != m_files.end(); iter++) {
            delete iter->second;
        }
        m_files.clear();
    }

    Flyweight* getFlyweight(const int key) {
        if (m_files.find(key) != m_files.end()) {
            return m_files[key];
        }
        Flyweight* fly = new ConcreteFlyweight(key);
        m_files.insert(std::pair<int,Flyweight*>(key,fly));
        return fly;
    }

private:
    std::map<int,Flyweight*> m_files;
};

int main()
{

    FlyweightFactory* factory = new FlyweightFactory;
    factory->getFlyweight(1)->operation();
    factory->getFlyweight(2)->operation();

    delete factory;

    UnsharedConcreteFlyweight*uf = new UnsharedConcreteFlyweight(3);
    uf->operation();
    delete uf;

    return 0;
}

