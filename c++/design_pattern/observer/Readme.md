# C++设计模式之观察者模式
![](https://img.shields.io/badge/standard--readme-MIT-green?style=flat&logo=appveyor)  


## 目录

* [什么是观察者模式？](#什么是观察者模式)
* [什么时候使用?](#什么时候使用)
* [代码实现](#代码实现)
* [观察者模式的优缺点](#观察者模式的优缺点)


## 什么是观察者模式？
观察者模式（`Observer Pattern`）：定义对象间的一种一对多的依赖关系，当一个对象的状态发生改变时，所有依赖于它的对象都得到通知并被自动更新。


## 什么时候使用？
- 当一个抽象有两个方面，一个依赖于另一个

- 当对一个对象的更改需要更改其他对象，而不知道需要更改多少个对象时

- 当一个对象应该能够通知其他对象而不假设这些对象是谁

## 代码实现

举个例子，比如警察一直观察着张三的一举一动，只要张三有什么违法行为，警察马上行动，抓捕张三。

这个过程中：

- 警察称之为观察者（`Observer`）
- 张三称之为被观察者（`Observable`，可观察的）
- 警察观察张三的这个行为称之为订阅（`subscribe`），或者注册（`register`）
- 张三违法后，警察抓捕张三的行动称之为响应（`update`）


观察者的接口：
```c++
class Observer {
public:
    virtual void update(std::string event) {
        m_event = event;
    }

    virtual ~Observer() {}

private:
    std::string m_event;
};
```
接口中只有一个`update`方法，用于对被观察者发出的事件做出响应。

被观察者的父类：
```c++
class Observable {
public:

    // 将 observer 对象添加到观察者列表中
    void addObserver(Observer *obj) {
        m_observer.push_back(obj);
    }

    // 将 observer 对象从观察者列表中移除
    void removeObserver(const int index) {
        m_observer.erase(m_observer.begin() + index);
    }

    // 通知所有观察者有事件发生，具体实现是调用所有观察者的 update 方法
    void notifyObserver(std::string event) {
        for(unsigned int i = 0; i < m_observer.size(); i++) {
            m_observer.at(i)->update(event);
        }
    }

    virtual ~Observable() {
        for(unsigned int i = 0; i < m_observer.size(); i ++) {
            delete m_observer.at(i);
        }
        m_observer.clear();
    }
private:
    std::vector<Observer*> m_observer;
};
```
被观察者中维护了一个观察者列表，提供了三个方法：

`addObserver`：将`observer`对象添加到观察者列表中

`removeObserver`：将`observer`对象从观察者列表中移除

`notifyObservers`：通知所有观察者有事件发生，具体实现是调用所有观察者的`update`方法

有了这两个基类，我们就可以定义出具体的罪犯与警察类。

警察属于观察者：
```c++
class PoliceObserver : public Observer {
public:
    void update(std::string event) {
        std::cout << "警察收到消息，罪犯在" << event << std::endl;
    }
    ~PoliceObserver() {}
};
```
警察实现了观察者接口，当警察收到事件后，做出响应，这里的响应就是简单的打印了一条日志。

罪犯属于被观察者：
```c++
class  CriminalObservable : public Observable {
public:
    void crime(std::string event) {
        std::cout << "罪犯正在" << event << std::endl;
        notifyObserver(event);
    }

    ~CriminalObservable() {}
};
```

[demo](observer_demo.cpp)

```c++
#include <iostream>
#include <vector>

// 观察者的接口
class Observer {
public:
    virtual void update(std::string event) {
        m_event = event;
    }

    virtual ~Observer() {}

private:
    std::string m_event;
};

// 被观察者的父类：
class Observable {
public:

    // 将 observer 对象添加到观察者列表中
    void addObserver(Observer *obj) {
        m_observer.push_back(obj);
    }

    // 将 observer 对象从观察者列表中移除
    void removeObserver(const int index) {
        m_observer.erase(m_observer.begin() + index);
    }

    // 通知所有观察者有事件发生，具体实现是调用所有观察者的 update 方法
    void notifyObserver(std::string event) {
        for(unsigned int i = 0; i < m_observer.size(); i++) {
            m_observer.at(i)->update(event);
        }
    }

    virtual ~Observable() {
        for(unsigned int i = 0; i < m_observer.size(); i ++) {
            delete m_observer.at(i);
        }
        m_observer.clear();
    }
private:
    std::vector<Observer*> m_observer;
};


// 警察属于观察者：
class PoliceObserver : public Observer {
public:
    void update(std::string event) {
        std::cout << "警察收到消息，罪犯在" << event << std::endl;
    }
    ~PoliceObserver() {}
};

// 罪犯继承自被观察者类
class  CriminalObservable : public Observable {
public:
    void crime(std::string event) {
        std::cout << "罪犯正在" << event << std::endl;
        notifyObserver(event);
    }

    ~CriminalObservable() {}
};


int main()
{
    CriminalObservable* zhangSan = new CriminalObservable();
    PoliceObserver* police1 = new PoliceObserver();
    PoliceObserver* police2 = new PoliceObserver();
    PoliceObserver* police3 = new PoliceObserver();

    zhangSan->addObserver(police1);
    zhangSan->addObserver(police2);
    zhangSan->addObserver(police3);
    zhangSan->crime("放狗咬人");

    return 0;
}
```
输出结果:
```sh
罪犯正在放狗咬人
警察收到消息，罪犯在放狗咬人
警察收到消息，罪犯在放狗咬人
警察收到消息，罪犯在放狗咬人
```

可以看到，所有的观察者都被通知到了。当某个观察者不需要继续观察时，调用`removeObserver`即可。

这就是观察者模式，它并不复杂，由于生活中一对多的关系非常常见，所以观察者模式应用广泛。


## 观察者模式的优缺点？
- 开闭原则。 无需修改发布者代码就能引入新的订阅者类 （如果是发布者接口则可轻松引入发布者类）。

- 可以在运行时建立对象之间的联系



**[⬆ 返回顶部](#目录)**
