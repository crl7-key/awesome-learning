#include <iostream>
#include <vector>

// 观察者的接口
class Observer {
public:
    virtual void update(std::string event) {
        m_event = event;
    }

    virtual ~Observer() {}

private:
    std::string m_event;
};

// 被观察者的父类：
class Observable {
public:

    // 将 observer 对象添加到观察者列表中
    void addObserver(Observer *obj) {
        m_observer.push_back(obj);
    }

    // 将 observer 对象从观察者列表中移除
    void removeObserver(const int index) {
        m_observer.erase(m_observer.begin() + index);
    }

    // 通知所有观察者有事件发生，具体实现是调用所有观察者的 update 方法
    void notifyObserver(std::string event) {
        for(unsigned int i = 0; i < m_observer.size(); i++) {
            m_observer.at(i)->update(event);
        }
    }

    virtual ~Observable() {
        for(unsigned int i = 0; i < m_observer.size(); i ++) {
            delete m_observer.at(i);
        }
        m_observer.clear();
    }
private:
    std::vector<Observer*> m_observer;
};


// 警察属于观察者：
class PoliceObserver : public Observer {
public:
    void update(std::string event) {
        std::cout << "警察收到消息，罪犯在" << event << std::endl;
    }
    ~PoliceObserver() {}
};

// 罪犯继承自被观察者类
class  CriminalObservable : public Observable {
public:
    void crime(std::string event) {
        std::cout << "罪犯正在" << event << std::endl;
        notifyObserver(event);
    }

    ~CriminalObservable() {}
};


int main()
{
    CriminalObservable* zhangSan = new CriminalObservable();
    PoliceObserver* police1 = new PoliceObserver();
    PoliceObserver* police2 = new PoliceObserver();
    PoliceObserver* police3 = new PoliceObserver();

    zhangSan->addObserver(police1);
    zhangSan->addObserver(police2);
    zhangSan->addObserver(police3);
    zhangSan->crime("放狗咬人");

    return 0;
}
