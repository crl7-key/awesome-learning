#include <iostream>
#include <vector>

class Observed;

// 为应该通知主题变化的对象定义更新接口
class Observer {
public:

    virtual ~Observer() {}

    virtual int getState() = 0;

    virtual void update(Observed* obj) = 0;
};

// 将状态存储到 ConcreteObserver 对象，并在其状态发生变化时向其观察者发送通知
class ConcreteObserver : public Observer {
public:
    ConcreteObserver(int state) : m_state(state) {}

    ~ConcreteObserver() {}

    int getState() {
        return m_state;
    }

    void update(Observed* obj); 

private:
    int m_state;
};

// 知道它的观察者并提供一个接口来附加和分离观察者
class Observed {
public:
    virtual ~Observed() {}

    void attach(Observer* observer) {
        m_observers.push_back(observer);
    }

    void detach(const int index) {
        m_observers.erase(m_observers.begin()+index);
    }

    void notify() {
        for(unsigned int i = 0; i < m_observers.size(); i++) {
            m_observers.at(i)->update(this);
        }
    }

    virtual int getState() = 0;

    virtual void setState(const int state) = 0;

private:
    std::vector<Observer*> m_observers;
};

class ConcreteObserved : public Observed {
public:

    ~ConcreteObserved() {}

    int getState() {
        return m_state;
    }

    void setState(const int state) {
        m_state = state;
    }
private:
    int m_state;
};

void ConcreteObserver::update(Observed* obj) {
    m_state = obj->getState();
    std::cout << "Observer state updated." << std::endl;
}


int main()
{
    ConcreteObserver observer1(1);
    ConcreteObserver observer2(2);

    std::cout << "Observer 1 state: " << observer1.getState() << std::endl;
    std::cout << "Observer 2 state: " << observer2.getState() << std::endl;

    Observed*obj = new ConcreteObserved();
    obj->attach(&observer1);
    obj->attach(&observer2);

    obj->setState(10);
    obj->notify();

    std::cout << "Observer 1 state: " << observer1.getState() << std::endl;
    std::cout << "Observer 2 state: " << observer2.getState() << std::endl;

    delete obj;

    return 0;
}
