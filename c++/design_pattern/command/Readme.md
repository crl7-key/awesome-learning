# C++设计模式之命令模式
![](https://img.shields.io/badge/standard--readme-MIT-green?style=flat&logo=appveyor)  


## 目录

* [什么是命令模式？](#什么是命令模式)
* [什么时候使用?](#什么时候使用)
* [代码实现](#代码实现)
* [命令模式优缺点](#命令模式优缺点)
* [与其他模式的关系](#与其他模式的关系)

## 什么是命令模式？
命令模式是一种行为设计模式， 它可将请求转换为一个包含与请求相关的所有信息的独立对象。 将请求封装为一个对象，从而可以将具有不同请求、队列或日志请求的客户端参数化，并支持可撤销的操作。

## 什么时候使用?

- 想要通过要执行的操作来参数化对象
- 想要在不同时间指定、排队和执行请求
- 支持撤销
- 支持日志更改，以便在系统崩溃时可以重新应用它们
- 围绕基于原语操作的高级操作构建系统


## 代码实现
```c++
#include <iostream>

// 执行请求
// 执行相关的操作
class Receiver {
public:
    void action() {
        std::cout << "Receiver: execute action " << std::endl;
    }
};

// 为所有命令声明一个接口
class Command {
public:
    virtual ~Command() {}

    virtual void execute() = 0;

protected:
    Command() {}
};

// 具体命令
// 通过调用相应的来实现execute
// Receiver 上的操作
class ConcreteCommand : public Command {
public:
    ConcreteCommand(Receiver* receiver) : m_receiver(receiver) {}

    virtual ~ConcreteCommand() {
        if (m_receiver) {
            delete m_receiver;
            m_receiver = nullptr;
        }
    }

    void execute()override {
        m_receiver->action();
    }
private:
    Receiver* m_receiver;
};

// 调用者
// 请求命令执行请求
class Invoker {
public:
    void set(Command* command) {
        m_command = command;
    }

    void confirm() {
        if (m_command) {
            m_command->execute();
        }
    }
private:
    Command* m_command;
};

int main()
{

    ConcreteCommand command(new Receiver());
    Invoker invoker;

    invoker.set(&command);
    invoker.confirm();

    return 0;
}
```


## 命令模式优缺点

命令模式可以说将封装发挥得淋漓尽致。在我们平时的程序设计中，最常用的封装是将拥有一类职责的对象封装成类，而命令对象的唯一职责就是通过 `execute` 去调用一个方法，也就是说它将 “方法调用” 这个步骤封装起来了，使得我们可以对 “方法调用” 进行排队、撤销等处理。

命令模式的主要优点：

- 降低系统的耦合度。将 “行为请求者” 和 ”行为实现者“ 解耦。

- 扩展性强。增加或删除命令非常方便，并且不会影响其他类。

- 封装 “方法调用”，方便实现 `action` 操作。

- 灵活性强

主要缺点：

- 会产生大量命令类。增加了系统的复杂性

## 与其他模式的关系
- 责任链模式、 命令模式、 中介者模式和观察者模式用于处理请求发送者和接收者之间的不同连接方式：

    - 责任链按照顺序将请求动态传递给一系列的潜在接收者， 直至其中一名接收者对请求进行处理。
    
    - 命令在发送者和请求者之间建立单向连接。
    
    - 中介者清除了发送者和请求者之间的直接连接， 强制它们通过一个中介对象进行间接沟通。
   
    - 观察者允许接收者动态地订阅或取消接收请求。

- 责任链的管理者可使用命令模式实现。 在这种情况下， 可以对由请求代表的同一个上下文对象执行许多不同的操作。

还有另外一种实现方式， 那就是请求自身就是一个命令对象。 在这种情况下， 可以对由一系列不同上下文连接而成的链执行相同的操作。

- 可以同时使用命令和备忘录模式来实现 “撤销”。 在这种情况下， 命令用于对目标对象执行各种不同的操作， 备忘录用来保存一条命令执行前该对象的状态。

- 命令和策略模式看上去很像， 因为两者都能通过某些行为来参数化对象。 但是， 它们的意图有非常大的不同。

  - 可以使用命令来将任何操作转换为对象。 操作的参数将成为对象的成员变量。 可以通过转换来延迟操作的执行、 将操作放入队列、 保存历史命令或者向远程服务发送命令等。

  - 另一方面， 策略通常可用于描述完成某件事的不同方式， 让你能够在同一个上下文类中切换算法。

- 原型模式可用于保存命令的历史记录。

- 可以将访问者模式视为命令模式的加强版本， 其对象可对不同类的多种对象执行操作。

**[⬆ 返回顶部](#目录)**
