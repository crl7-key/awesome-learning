#include <iostream>

// 执行请求
// 执行相关的操作
class Receiver {
public:
    void action() {
        std::cout << "Receiver: execute action " << std::endl;
    }
};

// 为所有命令声明一个接口
class Command {
public:
    virtual ~Command() {}

    virtual void execute() = 0;

protected:
    Command() {}
};

// 具体命令
// 通过调用相应的来实现execute
// Receiver 上的操作
class ConcreteCommand : public Command {
public:
    ConcreteCommand(Receiver* receiver) : m_receiver(receiver) {}

    virtual ~ConcreteCommand() {
        if (m_receiver) {
            delete m_receiver;
            m_receiver = nullptr;
        }
    }

    void execute()override {
        m_receiver->action();
    }
private:
    Receiver* m_receiver;
};

// 调用者
// 请求命令执行请求
class Invoker {
public:
    void set(Command* command) {
        m_command = command;
    }

    void confirm() {
        if (m_command) {
            m_command->execute();
        }
    }
private:
    Command* m_command;
};

int main()
{

    ConcreteCommand command(new Receiver());
    Invoker invoker;

    invoker.set(&command);
    invoker.confirm();

    return 0;
}

