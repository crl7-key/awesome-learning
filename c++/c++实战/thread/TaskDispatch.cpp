#include <iostream>
#include <thread> /* for thread*/
#include <memory> /* for unique_ptr  */
#include <atomic> /* for atomic */ 
#include <queue> /* for queue */
#include <functional> /* for functional */
#include <future> /* for future */
#include <mutex> /* for mutex */
#include <condition_variable> /* for condition_variable */

class TaskDispatch {
public:
    static TaskDispatch& getInstance() {
        static TaskDispatch t;
        return t;
    }

    bool start() {
        auto func = [this]() {
            while(!m_interrupt.load()) {
                std::function<void()> task;

                {
                    std::unique_lock<std::mutex> lock(this->m_lock);

                    this->m_cv.wait(lock,[this]() {
                                    return this->m_interrupt.load() || !this->m_tasks.empty();
                                    });
 
                    if (this->m_interrupt.load()) {
                        continue;
                    }

                    task = std::move(this->m_tasks.front());
                    this->m_tasks.pop();

                }

                task();
            }
        };
        m_thread = std::make_unique<std::thread>(func);
        return true;
    }


    bool stop() {
        m_interrupt.store(true);

        this->m_cv.notify_all();

        // 检查线程是否可被join
        if (m_thread && m_thread->joinable()) {
            m_thread->join();
        }
        return true;
    }


    // 返回智能指针
    template<typename T,typename... Args>
        auto run(T &&func, Args &&... args)
        -> std::shared_ptr<std::future<std::result_of_t<T(Args...)>>>
        {
            using returnType = std::result_of_t<T(Args...)>;

            auto task = std::make_shared<std::packaged_task<returnType()>>(
                                                                           std::bind(std::forward<T>(func), std::forward<Args>(args)...));

            std::future<returnType> ret = task->get_future();

            {
                std::lock_guard<std::mutex> lock(this->m_lock);
                this->m_tasks.emplace([task]() { (*task)(); });
            }

            this->m_cv.notify_all();
            return std::make_shared<std::future<std::result_of_t<T(Args...)>>>(std::move(ret));
        }

private:
    TaskDispatch() {}

    std::unique_ptr<std::thread> m_thread = nullptr;

    std::atomic<bool> m_interrupt{false};

    std::queue<std::function<void()>> m_tasks;

    std::mutex m_lock;

    std::condition_variable m_cv;
};

void func1() {
    for (int i = 0; i < 20; i++) {
        std::cout << "func1 " << i << "\n";
    }
}

int func2() {
    for (int i = 0; i < 20; i++) {
        std::cout << "func2 " << i << "\n";
    }
    return 39;
}

int main()
{
    TaskDispatch &t = TaskDispatch::getInstance();
    t.start();
    t.run(func1)->get();
    std::cout << "func1 return" << std::endl;;

    int d = t.run(func2)->get();

    std::cout << "return " << d << std::endl;
    std::this_thread::sleep_for(std::chrono::seconds(2));
    t.stop();

    return 0;
}

