#include <sstream>
#include "mysql_dbpool.h"
#include <glog/logging.h>

static void InitLog(const char *cmd)
{
    FLAGS_logbufsecs = 0;
    // 初始化应用程序名
    google::InitGoogleLogging(cmd);
#ifdef DEBUG_MODE
    google::SetStderrLogging(google::GLOG_INFO); // 设置级别高>
#else
    google::SetStderrLogging(google::GLOG_FATAL);   // 设置级别高
#endif
    // google::LogToStderr();                       // 只输出到标>
    FLAGS_log_dir = "./log";                        // 设置保存log
    FLAGS_colorlogtostderr = true;                  // 设置输出到>
    FLAGS_logbufsecs = 0;                           // 缓冲日志输>
    FLAGS_max_log_size = 100;                       // 最大日志大>
    FLAGS_stop_logging_if_full_disk = true;         // 当磁盘被写>
}

static std::string int2string(uint32_t user_id)
{
    std::stringstream ss;
    ss << user_id;
    return ss.str();
}

#define DROP_USER_TABLE	"DROP TABLE IF EXISTS User"     /* if EXISTS 好处 是如果表不存在,执行不会报错 */


 #define CREATE_USER_TABLE "CREATE TABLE User (     \
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户id',   \
  `sex` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '1男2女0未知', \
  `name` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '用户名',  \
  `domain` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '拼音',  \
  `nick` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '别名,绰号等', \
  `password` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '密码',    \
  `salt` varchar(4) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '混淆码',   \
  `phone` varchar(11) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '手机号码',   \
  `email` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT 'email',  \
  `company` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '公司名称', \
  `address` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '所在地区', \
  `avatar` varchar(255) COLLATE utf8mb4_bin DEFAULT '' COMMENT '自定义用户头像',    \
  `validateMethod` tinyint(2) unsigned DEFAULT '1' COMMENT '好友验证方式',  \
  `departId` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '所属部门Id',    \
  `status` tinyint(2) unsigned DEFAULT '0' COMMENT '1. 试用期 2. 正式 3. 离职 4.实习',  \
  `created` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',   \
  `updated` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',   \
  `push_shield_status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0关闭勿扰 1开启勿扰',  \
  `sign_info` varchar(128) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '个性签名',  \
  PRIMARY KEY (`id`),   \
  KEY `idx_domain` (`domain`),  \
  KEY `idx_name` (`name`),  \
  KEY `idx_phone` (`phone`) \
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;"     

#define INSERT_SAMPLE		"INSERT INTO user(name,email,phone) VALUES(?,?,?)"
#define SELECT_SAMPLE 		"SELECT name,email,phone FROM user"


// 创建数据库mysql_pool_test:  create database mysql_pool_test;
// show databases;    查看数据库
// show tables;       查看有哪些表
// desc table_name;   查看表结构

static uint32_t Uid = 0;

struct UserInfo
{
    uint32_t uid;                       // 用户ID
    uint8_t nSex;                       // 用户性别 1.男;2.女
    uint8_t nStatus;                    // 用户状态0 正常， 1 离职
    uint32_t nValidateMethod;           // 好友验证方式
    uint32_t nDeptId;                   // 所属部门
    std::string strNick;                // 别名
    std:: string strDomain;             // domain 
    std:: string strName;               // 姓名
    std:: string strTel;                // 手机号码
    std:: string strEmail;              // Email
    std:: string strAvatar;             // 头像
    std:: string sign_info;             // 签名
    std:: string strPass;               // 密码
    std:: string strCompany;            // 公司
    std:: string strAddress;            // 地址
};

bool insertUser(DBConn* pDBConn)
{
    bool bRet = false;
    std::string strSql;
    strSql = "insert into User(`salt`,`sex`,`nick`,`password`,`domain`,`name`,`phone`,`email`,`company`,`address`,`avatar`,`sign_info`,`departId`,`status`,`created`,`updated`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    PrepareStatement* stmt = new PrepareStatement();
    if (stmt->Init(pDBConn->GetMysql(), strSql))
    {
        uint32_t nNow = (uint32_t) time(NULL);
        uint32_t index = 0;
        std::string strOutPass = "987654321";
        std::string strSalt = "abcd";

        int nSex = 1;                               // 用户性别 1.男;2.女
        int nStatus = 0;                            // 用户状态0 正常， 1 离职
        uint32_t nValidateMethod = 1;               // 好友验证方式
        uint32_t nDeptId = 0;                       // 所属部门
        std::string strNick = "别名";               // 别名
        std::string strDomain = "AF_INET";          // domain
        std::string strName = "姓名";               // 姓名
        std::string strTel = "12345678910";         // 手机号码
        std::string strEmail = "123456789@qq.com";  // Email
        std::string strAvatar = "111";              // 头像
        std::string sign_info = "签名信息";          // 签名
        std::string strPass = "123456";             // 密码
        std::string strCompany = "密码";            // 公司
        std::string strAddress = "地址信息";        // 地址

        stmt->SetParam(index++, strSalt);
        stmt->SetParam(index++, nSex);
        stmt->SetParam(index++, strNick);
        stmt->SetParam(index++, strOutPass);
        stmt->SetParam(index++, strDomain);
        stmt->SetParam(index++, strName);
        stmt->SetParam(index++, strTel);
        stmt->SetParam(index++, strEmail);
        stmt->SetParam(index++, strCompany);
        stmt->SetParam(index++, strAddress);
        stmt->SetParam(index++, strAvatar);
        stmt->SetParam(index++, sign_info);
        stmt->SetParam(index++, nDeptId);
        stmt->SetParam(index++, nStatus);
        stmt->SetParam(index++, nNow);
        stmt->SetParam(index++, nNow);
        bRet = stmt->ExecuteUpdate();
        
        if (!bRet)
        {
            printf("insert user failed: %s\n", strSql.c_str());
        }
        else
        {
            Uid = stmt->GetInsertId();
            printf("register then get user_id:%d\n", Uid);
        }
    }
    delete stmt;

    return true;
}

bool queryUser(DBConn* pDBConn)
{
    std::string strSql = "select * from User where id =" + int2string(Uid);
    ResultSet* pResultSet = pDBConn->ExecuteQuery(strSql.c_str());
    bool bRet = false;
    if(pResultSet)
    {
        if(pResultSet->Next())
        {
            UserInfo info;
            info.uid = pResultSet->GetInt("id");
            info.nSex = pResultSet->GetInt("sex");
            info.strNick = pResultSet->GetString("nick");
            info.strDomain = pResultSet->GetString("domain");
            info.strName = pResultSet->GetString("name");
            info.strTel = pResultSet->GetString("phone");
            info.strEmail = pResultSet->GetString("email");
            info.strAvatar = pResultSet->GetString("avatar");
            info.sign_info = pResultSet->GetString("sign_info");
            info.nDeptId = pResultSet->GetInt("departId");
            info.nValidateMethod = pResultSet->GetInt("validateMethod");
            info.nStatus = pResultSet->GetInt("status");

            std::cout << "uid is : " << info.uid << std::endl;
            std::cout << "sex is : " << info.nSex << std::endl;
            std::cout << "nick is : " << info.strNick << std::endl;
            std::cout << "domain is : " << info.strDomain << std::endl;
            std::cout << "name is : " << info.strName << std::endl;
            std::cout << "phone is : " << info.strTel << std::endl;
            
            bRet = true;
        }
        delete pResultSet;
    }
    else
    {
        printf("no result set for sql:%s\n", strSql.c_str());
    }

    return bRet;
}

bool updateUser(DBConn* pDBConn)
{
     bool bRet = false;
    if (pDBConn) {
        UserInfo info;
        info.nSex =              2; 
        info.nStatus =           0;
        info.nValidateMethod =   1;
        info.strNick =           "nickName";
        info.strDomain =         "127.0.0.1";
        info.strName =           "test_name";
        info.strTel =            "9876543210";
        info.strEmail =          "test@qq.com";
        info.strAvatar =         "./test.jpg";
        info.sign_info =         "千里之行始于足下";
        info.strPass =           "password";
        info.strCompany =        "tencent";
        info.strAddress =        "shenzhen";

        uint32_t nNow = (uint32_t)time(NULL);
        std::string strSql = "update User set `sex`=" + int2string(info.nSex) 
            + ",`nick`='" + info.strNick 
            + "', `domain`='" + info.strDomain 
            + "', `name`='" + info.strName 
            + "', `phone`='" + info.strTel
            + "', `email`='" + info.strEmail 
            + "', `avatar`='" + info.strAvatar 
            + "', `sign_info`='" + info.sign_info
            +"', `departId`='" + int2string(info.nDeptId) 
            + "', `status`=" + int2string(info.nStatus) 
            + ", `updated`="+int2string(nNow)
            +", `company`='" + info.strCompany 
            + "', `address`='" + info.strAddress 
            + "' where id = " + int2string(Uid);

        bRet = pDBConn->ExecuteUpdate(strSql.c_str());
        if (!bRet) {
            LOG(ERROR) << "execute update error";
        }

    }
    else {
        LOG(INFO) << "no db connection";
    }
    return bRet;
}
// 创建（Create）、更新（Update）、读取（Retrieve）和删除（Delete）操作。
// 测试增删改查
void testCURD()
{
    char *db_pool_name = "mypool";
    char* db_host = "127.0.0.1";
    int   db_port =  3306;
    char* db_dbname = "mysql_pool_test";
    char* db_username = "root";
    char* db_password = "123456";
    int db_maxconncnt = 4;
    DBPool* pDBPool = new DBPool(db_pool_name, db_host, db_port, 
        db_username, db_password, db_dbname, db_maxconncnt);
    if (pDBPool->Init()) {
        printf("init db instance failed: %s", db_pool_name);
        return;
    }

    DBConn* pDBConn = pDBPool->GetDBConn();
    if(pDBConn)
    {
        bool ret = pDBConn->ExecuteDrop(DROP_USER_TABLE);
        if(ret)
        {
            printf("DROP_USER_TABLE ok\n");
        }
        // 1. 创建表
        ret = pDBConn->ExecuteCreate(CREATE_USER_TABLE);
        if(ret)
        {
            printf("CREATE_USER_TABLE ok\n");
        }

        // 2. 插入内容
        ret = insertUser(pDBConn);
        if(ret)
        {
            printf("insertUser ok -------\n\n");
        }
        // 3. 查询内容
        ret = queryUser(pDBConn);
        if(ret)
        {
            printf("queryUser ok -------\n\n");
        }
        // 4. 修改内容
        ret = updateUser(pDBConn);
        if(ret)
        {
            printf("updateUser ok -------\n\n");
        }
        ret = queryUser(pDBConn);
        if(ret)
        {
            printf("queryUser ok -------\n\n");
        }
        // 5. 删除表
        pDBPool->ReleaseDBConn(pDBConn);
    }
    else
    {
        printf("pDBConn is null\n");
    }
    delete pDBPool;
}

// 默认端口 3306
// 测试一次连接和端口的情况： tcpdump -i any port 3306  
int main(int argc, char* argv[])
{
    InitLog(argv[0]);
    printf("test testCURD begin\n");
    testCURD();
    printf("test testCURD finish\n");
    return 0;
}