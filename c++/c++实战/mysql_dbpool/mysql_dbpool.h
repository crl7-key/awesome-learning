#ifndef INC_MYSQL_DBPOOL_H_
#define INC_MYSQL_DBPOOL_H_

#include <iostream>
#include <list>
#include <map>
#include <stdint.h>

#include "thread_notify.h"
#include "/usr/include/mysql/mysql.h"

#define MAX_ESCAPE_STRING_LEN 10240

// 返回结果 select的时候用
class ResultSet
{
public:
	ResultSet(MYSQL_RES *res);
	virtual ~ResultSet();

	bool Next();
	int GetInt(const char *key);
	char *GetString(const char *key);

private:
	int GetIndex(const char *key);

	MYSQL_RES *m_res;
	MYSQL_ROW m_row;
	std::map<std::string, int> m_key_map;
};

// 插入数据用
class PrepareStatement
{
public:
	PrepareStatement();
	virtual ~PrepareStatement();

	bool Init(MYSQL *mysql, std::string &sql);

	void SetParam(uint32_t index, int &value);
	void SetParam(uint32_t index, uint32_t &value);
	void SetParam(uint32_t index, std::string &value);
	void SetParam(uint32_t index, const std::string &value);

	bool ExecuteUpdate();
	uint32_t GetInsertId();

private:
	MYSQL_STMT *m_stmt;
	MYSQL_BIND *m_param_bind;
	uint32_t m_param_cnt;
};

class DBPool;

class DBConn
{
public:
	DBConn(DBPool *pDBPool);
	virtual ~DBConn();
	int Init();

	// 创建表
	bool ExecuteCreate(const char *sql);
	// 删除表
	bool ExecuteDrop(const char *sql);
	// 查询
	ResultSet *ExecuteQuery(const char *sql);

	/**
    *  执行DB更新，修改
    *
    *  @param sql     sql
    *  @param care_affected_rows  是否在意影响的行数，false:不在意；true:在意
    *
    *  @return 成功返回true 失败返回false
    */
	bool ExecuteUpdate(const char *sql, bool care_affected_rows = true);
	uint32_t GetInsertId();

	// 开启事务
	bool StartTransaction();
	// 提交事务
	bool Commit();
	// 回滚事务
	bool Rollback();
	// 获取连接池名
	const char *GetPoolName();
	MYSQL *GetMysql() { return m_mysql; }

private:
	DBPool *m_pDBPool; // to get MySQL server information
	MYSQL *m_mysql;	   // 对应一个连接
	char m_escape_string[MAX_ESCAPE_STRING_LEN + 1];
};

class DBPool
{
public:
	DBPool() {}
	DBPool(const char *pool_name, const char *db_server_ip, uint16_t db_server_port,
		   const char *username, const char *password, const char *db_name, int max_conn_cnt);
	virtual ~DBPool();

	int Init();					   				// 连接数据库，创建连接
	DBConn *GetDBConn();		   				// 获取连接资源
	void ReleaseDBConn(DBConn *pConn);  // 归还连接资源

	const char *GetPoolName() { return m_pool_name.c_str(); }
	const char *GetDBServerIP() { return m_db_host.c_str(); }
	uint16_t GetDBServerPort() { return m_db_server_port; }
	const char *GetUsername() { return m_username.c_str(); }
	const char *GetPasswrod() { return m_password.c_str(); }
	const char *GetDBName() { return m_db_name.c_str(); }

private:
	std::string m_pool_name;		 // 连接池名称
	std::string m_db_host;		 	 // 数据库ip
	uint16_t m_db_server_port;		 // 数据库端口
	std::string m_username;			 // 用户名
	std::string m_password;			 // 用户密码
	std::string m_db_name;			 // db名称
	int m_db_cur_conn_cnt;			 // 当前启用的连接数量
	int m_db_max_conn_cnt;			 // 最大连接数量
	std::list<DBConn *> m_free_list; // 空闲的连接
	ThreadNotify m_cond;	         // 信号量
};

#endif /* INC_MYSQL_DBPOOL_H_ */
