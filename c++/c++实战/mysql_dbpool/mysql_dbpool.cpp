#include "mysql_dbpool.h"
#include <string.h>
#include <glog/logging.h>

const int MIN_DB_CONN_CNT = 1;

ResultSet::ResultSet(MYSQL_RES *res)
{
	m_res = res;

	// 将表字段键索引映射到map中
	int num_fields = mysql_num_fields(m_res);
	MYSQL_FIELD *fields = mysql_fetch_fields(m_res);
	for (int i = 0; i < num_fields; i++)
	{
		// 多行
		m_key_map.insert(std::make_pair(fields[i].name, i));
	}
}

ResultSet::~ResultSet()
{
	if (m_res)
	{
		mysql_free_result(m_res);
		m_res = NULL;
	}
}

bool ResultSet::Next()
{
	m_row = mysql_fetch_row(m_res);
	if (m_row)
	{
		return true;
	}
	else
	{
		return false;
	}
}

int ResultSet::GetIndex(const char *key)
{
	std::map<std::string, int>::iterator it = m_key_map.find(key);
	if (it == m_key_map.end())
	{
		return -1;
	}
	else
	{
		return it->second;
	}
}

int ResultSet::GetInt(const char *key)
{
	int idx = GetIndex(key);
	if (idx == -1)
	{
		return 0;
	}
	else
	{
		return atoi(m_row[idx]); // 有索引
	}
}

char *ResultSet::GetString(const char *key)
{
	int idx = GetIndex(key);
	if (idx == -1)
	{
		return NULL;
	}
	else
	{
		return m_row[idx];		// 列
	}
}

/////////////////////////////////////////
PrepareStatement::PrepareStatement()
{
	m_stmt = NULL;
	m_param_bind = NULL;
	m_param_cnt = 0;
}

PrepareStatement::~PrepareStatement()
{
	if (m_stmt)
	{
		mysql_stmt_close(m_stmt);
		m_stmt = NULL;
	}

	if (m_param_bind)
	{
		delete[] m_param_bind;
		m_param_bind = NULL;
	}
}

bool PrepareStatement::Init(MYSQL *mysql, std::string &sql)
{
	mysql_ping(mysql);	// 当mysql连接丢失的时候，使用mysql_ping能够自动重连数据库

	m_stmt = mysql_stmt_init(mysql);
	if (!m_stmt)
	{
		LOG(ERROR) <<"mysql_stmt_init failed";
		return false;
	}

	if (mysql_stmt_prepare(m_stmt, sql.c_str(), sql.size()))
	{
		LOG(ERROR) <<"mysql_stmt_prepare failed: " << mysql_stmt_error(m_stmt);
		return false;
	}

	m_param_cnt = mysql_stmt_param_count(m_stmt);
	if (m_param_cnt > 0)
	{
		m_param_bind = new MYSQL_BIND[m_param_cnt];
		if (!m_param_bind)
		{
			LOG(ERROR) <<"new failed";
			return false;
		}

		memset(m_param_bind, 0, sizeof(MYSQL_BIND) * m_param_cnt);
	}

	return true;
}

void PrepareStatement::SetParam(uint32_t index, int &value)
{
	if (index >= m_param_cnt)
	{
		LOG(ERROR) <<"index too large:"<< index ;
		return;
	}

	m_param_bind[index].buffer_type = MYSQL_TYPE_LONG;
	m_param_bind[index].buffer = &value;
}

void PrepareStatement::SetParam(uint32_t index, uint32_t &value)
{
	if (index >= m_param_cnt)
	{
		LOG(ERROR) <<"index too large: "<< index;
		return;
	}

	m_param_bind[index].buffer_type = MYSQL_TYPE_LONG;
	m_param_bind[index].buffer = &value;
}

void PrepareStatement::SetParam(uint32_t index, std::string &value)
{
	if (index >= m_param_cnt)
	{
		LOG(ERROR) <<"index too large: " << index;
		return;
	}

	m_param_bind[index].buffer_type = MYSQL_TYPE_STRING;
	m_param_bind[index].buffer = (char *)value.c_str();
	m_param_bind[index].buffer_length = value.size();
}

void PrepareStatement::SetParam(uint32_t index, const std::string &value)
{
	if (index >= m_param_cnt)
	{
		LOG(ERROR) <<"index too large: " << index;
		return;
	}

	m_param_bind[index].buffer_type = MYSQL_TYPE_STRING;
	m_param_bind[index].buffer = (char *)value.c_str();
	m_param_bind[index].buffer_length = value.size();
}

bool PrepareStatement::ExecuteUpdate()
{
	if (!m_stmt)
	{
		LOG(ERROR) <<"no m_stmt";
		return false;
	}

	if (mysql_stmt_bind_param(m_stmt, m_param_bind))
	{
		LOG(ERROR) <<"mysql_stmt_bind_param failed: " <<  mysql_stmt_error(m_stmt);
		return false;
	}

	if (mysql_stmt_execute(m_stmt))
	{
		LOG(ERROR) <<"mysql_stmt_execute failed: " <<  mysql_stmt_error(m_stmt);
		return false;
	}

	if (mysql_stmt_affected_rows(m_stmt) == 0)
	{
		LOG(ERROR) <<"ExecuteUpdate have no effect";
		return false;
	}

	return true;
}

uint32_t PrepareStatement::GetInsertId()
{
	return mysql_stmt_insert_id(m_stmt);
}

DBConn::DBConn(DBPool *pPool)
{
	m_pDBPool = pPool;
	m_mysql = NULL;
}

DBConn::~DBConn()
{
	if (m_mysql)
	{
		mysql_close(m_mysql);
	}
}

int DBConn::Init()
{
	m_mysql = mysql_init(NULL);
	if (!m_mysql)
	{
		LOG(ERROR) <<"mysql_init failed";
		return 1;
	}

	bool reconnect = true;
	mysql_options(m_mysql, MYSQL_OPT_RECONNECT, &reconnect);	// 配合mysql_ping实现自动重连
	mysql_options(m_mysql, MYSQL_SET_CHARSET_NAME, "utf8mb4");

	if (!mysql_real_connect(m_mysql, m_pDBPool->GetDBServerIP(), m_pDBPool->GetUsername(), m_pDBPool->GetPasswrod(),
							m_pDBPool->GetDBName(), m_pDBPool->GetDBServerPort(), NULL, 0))
	{
		LOG(ERROR) <<"mysql_real_connect failed: " <<  mysql_error(m_mysql);
		return 2;
	}

	return 0;
}

const char *DBConn::GetPoolName()
{
	return m_pDBPool->GetPoolName();
}

bool DBConn::ExecuteCreate(const char *sql_query)
{
	mysql_ping(m_mysql);
	// mysql_real_query 实际就是执行了SQL
	if (mysql_real_query(m_mysql, sql_query, strlen(sql_query)))
	{
		LOG(ERROR) <<"mysql_real_query failed:" <<  mysql_error(m_mysql) << " sql: start transaction"; 
		return false;
	}

	return true;
}
bool DBConn::ExecuteDrop(const char *sql_query)
{
	mysql_ping(m_mysql);

	if (mysql_real_query(m_mysql, sql_query, strlen(sql_query)))
	{
		LOG(ERROR) <<"mysql_real_query failed:" << mysql_error(m_mysql) << " sql: start transaction";
		return false;
	}

	return true;
}

ResultSet *DBConn::ExecuteQuery(const char *sql_query)
{
	mysql_ping(m_mysql);

	if (mysql_real_query(m_mysql, sql_query, strlen(sql_query)))
	{
		LOG(ERROR) <<"mysql_real_query failed: "<< mysql_error(m_mysql) << " sql: " <<  sql_query;
		return NULL;
	}
	// 返回结果
	MYSQL_RES *res = mysql_store_result(m_mysql);	// 返回结果
	if (!res)
	{
		LOG(ERROR) <<"mysql_store_result failed: "<< mysql_error(m_mysql);
		return NULL;
	}

	ResultSet *result_set = new ResultSet(res);	// 存储到ResultSet
	return result_set;
}

bool DBConn::ExecuteUpdate(const char *sql_query, bool care_affected_rows)
{
	mysql_ping(m_mysql);

	if (mysql_real_query(m_mysql, sql_query, strlen(sql_query)))
	{
		LOG(ERROR) <<"mysql_real_query failed: " <<  mysql_error(m_mysql) << " sql: " <<  sql_query;
		return false;
	}

	if (mysql_affected_rows(m_mysql) > 0)
	{
		return true;
	}
	else
	{ 	// 影响的行数为0时
		if (care_affected_rows)
		{ // 如果在意影响的行数时, 返回false, 否则返回true
			LOG(ERROR) <<"mysql_real_query failed:" << mysql_error(m_mysql) <<  " sql: " << sql_query;
			return false;
		}
		else
		{
			LOG(ERROR) << "affected_rows=0, sql: " <<  sql_query;
			return true;
		}
	}
}

bool DBConn::StartTransaction()
{
	mysql_ping(m_mysql);

	if (mysql_real_query(m_mysql, "start transaction\n", 17))
	{
		LOG(ERROR) <<"mysql_real_query failed:"<< mysql_error(m_mysql) << ", sql: start transaction";
		return false;
	}

	return true;
}

bool DBConn::Rollback()
{
	mysql_ping(m_mysql);

	if (mysql_real_query(m_mysql, "rollback\n", 8))
	{
		LOG(ERROR) <<"mysql_real_query failed: "<<  mysql_error(m_mysql) << " sql: rollback\n";
		return false;
	}

	return true;
}

bool DBConn::Commit()
{
	mysql_ping(m_mysql);

	if (mysql_real_query(m_mysql, "commit\n", 6))
	{
		LOG(ERROR) <<"mysql_real_query failed: " << mysql_error(m_mysql) <<"sql: commit";
		return false;
	}

	return true;
}
uint32_t DBConn::GetInsertId()
{
	return (uint32_t)mysql_insert_id(m_mysql);
}

////////////////
DBPool::DBPool(const char *pool_name, const char *db_server_ip, uint16_t db_server_port,
				 const char *username, const char *password, const char *db_name, int max_conn_cnt)
{
	m_pool_name = pool_name;
	m_db_host = db_server_ip;
	m_db_server_port = db_server_port;
	m_username = username;
	m_password = password;
	m_db_name = db_name;
	m_db_max_conn_cnt = max_conn_cnt;	  // 最大连接数量 
	m_db_cur_conn_cnt = MIN_DB_CONN_CNT;  // 最小连接数量
}

DBPool::~DBPool()
{
	std::list<DBConn *>::iterator it = m_free_list.begin();
	for (; it != m_free_list.end(); it++)
	{
		DBConn *pConn = *it;
		delete pConn;
	}

	m_free_list.clear();
}

int DBPool::Init()
{
	// 创建固定最小的连接数量
	for (int i = 0; i < m_db_cur_conn_cnt; i++)
	{
		DBConn *pDBConn = new DBConn(this);
		int ret = pDBConn->Init();
		if (ret)
		{
			delete pDBConn;
			return ret;
		}

		m_free_list.push_back(pDBConn);
	}
	return 0;
}

DBConn *DBPool::GetDBConn()
{
	m_cond.Lock();

	while (m_free_list.empty())
	{
		// m_db_cur_conn_cnt是总共多少个连接，不是指空闲的
		if (m_db_cur_conn_cnt >= m_db_max_conn_cnt)
		{
			m_cond.Wait();
		}
		else
		{
			DBConn *pDBConn = new DBConn(this);
			int ret = pDBConn->Init();
			if (ret)
			{
				LOG(ERROR) <<"Init DBConnecton failed";
				delete pDBConn;
				m_cond.Unlock();
				return NULL;
			}
			else
			{
				m_free_list.push_back(pDBConn);
				m_db_cur_conn_cnt++;
				LOG(ERROR) << "new db connection: "<< m_pool_name.c_str() <<", conn_cnt: "<< m_db_cur_conn_cnt;
			}
		}
	}

	DBConn *pConn = m_free_list.front();	// 获取连接
	m_free_list.pop_front();			    // STL 吐出连接，从空闲队列删除

	m_cond.Unlock();

	return pConn;
}

void DBPool::ReleaseDBConn(DBConn *pConn)
{
	m_cond.Lock();

	std::list<DBConn *>::iterator it = m_free_list.begin();
	for (; it != m_free_list.end(); it++)
	{
		if (*it == pConn)
		{
			break;
		}
	}

	if (it == m_free_list.end())
	{
		m_free_list.push_back(pConn);
	}

	m_cond.Signal();
	m_cond.Unlock();
}
