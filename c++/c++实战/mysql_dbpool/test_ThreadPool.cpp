#include "thread_pool.h"
#include "mysql_dbpool.h"
#include "HeapQueue.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <signal.h>
#include <setjmp.h>
#include <stdarg.h>
#include <sys/time.h>
#include <unistd.h>
#include <sstream>
#include <glog/logging.h>

#define TASK_NUMBER 1000

static void InitLog(const char *cmd)
{
    FLAGS_logbufsecs = 0;
    // 初始化应用程序名
    google::InitGoogleLogging(cmd);
#ifdef DEBUG_MODE
    google::SetStderrLogging(google::GLOG_INFO); // 设置级别高>
#else
    google::SetStderrLogging(google::GLOG_FATAL);   // 设置级别高
#endif
    // google::LogToStderr();                       // 只输出到标>
    FLAGS_log_dir = "./log";                        // 设置保存log
    FLAGS_colorlogtostderr = true;                  // 设置输出到>
    FLAGS_logbufsecs = 0;                           // 缓冲日志输>
    FLAGS_max_log_size = 100;                       // 最大日志大>
    FLAGS_stop_logging_if_full_disk = true;         // 当磁盘被写>
}

static std::string int2string(uint32_t user_id)
{
    std::stringstream ss;
    ss << user_id;
    return ss.str();
}

#define DROP_USER_TABLE "DROP TABLE IF EXISTS User" /* if EXISTS 好处 是如果表不存在,执行不会报错 */

#define CREATE_USER_TABLE "CREATE TABLE User (\
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户id',   \
`sex` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '1男2女0未知', \
`name` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '用户名',  \
`domain` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '拼音',  \
`nick` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '别名,绰号等', \
`password` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '密码',    \
`salt` varchar(4) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '混淆码',   \
`phone` varchar(11) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '手机号码',   \
`email` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT 'email',  \
`company` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '公司名称', \
`address` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '所在地区', \
`avatar` varchar(255) COLLATE utf8mb4_bin DEFAULT '' COMMENT '自定义用户头像',    \
`validateMethod` tinyint(2) unsigned DEFAULT '1' COMMENT '好友验证方式',  \
`departId` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '所属部门Id',    \
`status` tinyint(2) unsigned DEFAULT '0' COMMENT '1. 试用期 2. 正式 3. 离职 4.实习',  \
`created` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',   \
`updated` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',   \
`push_shield_status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0关闭勿扰 1开启勿扰',  \
`sign_info` varchar(128) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '个性签名',  \
PRIMARY KEY (`id`),   \
KEY `idx_domain` (`domain`),  \
KEY `idx_name` (`name`),  \
KEY `idx_phone` (`phone`) \
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;"
#define INSERT_SAMPLE "INSERT INTO user(name,email,phone) VALUES(?,?,?)"
#define SELECT_SAMPLE "SELECT name,email,phone FROM user"

static unsigned int Uid = 0;

static uint64_t get_tick_count()
{
    struct timeval tval;
    uint64_t ret_tick;

    gettimeofday(&tval, NULL);

    ret_tick = tval.tv_sec * 1000L + tval.tv_usec / 1000L;
    return ret_tick;
}

struct UserInfo
{
    uint32_t uid;             // 用户ID
    uint8_t nSex;             // 用户性别 1.男;2.女
    uint8_t nStatus;          // 用户状态0 正常， 1 离职
    uint32_t nValidateMethod; // 好友验证方式
    uint32_t nDeptId;         // 所属部门
    std::string strNick;      // 别名
    std::string strDomain;    // domain
    std::string strName;      // 姓名
    std::string strTel;       // 手机号码
    std::string strEmail;     // Email
    std::string strAvatar;    // 头像
    std::string sign_info;    // 签名
    std::string strPass;      // 密码
    std::string strCompany;   // 公司
    std::string strAddress;   // 地址
};

bool insertUser(DBConn *pDBConn)
{
    bool bRet = false;
    std::string strSql;
    strSql = "insert into User(`salt`,`sex`,`nick`,`password`,`domain`,`name`,`phone`,`email`,`company`,`address`,`avatar`,`sign_info`,`departId`,`status`,`created`,`updated`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    PrepareStatement *stmt = new PrepareStatement();
    if (stmt->Init(pDBConn->GetMysql(), strSql))
    {
        uint32_t nNow = (uint32_t)time(NULL);
        uint32_t index = 0;
        std::string strOutPass = "11111111";
        std::string strSalt = "ace";

        int nSex = 1;                              // 用户性别 1.男;2.女
        int nStatus = 0;                           // 用户状态0 正常， 1 离职
        uint32_t nValidateMethod = 1;              // 好友验证方式
        uint32_t nDeptId = 0;                      // 所属部门
        std::string strNick = "别名";              // 别名
        std::string strDomain = "AF_INET";         // domain
        std::string strName = "姓名";              // 真名
        std::string strTel = "12345678910";        // 手机号码
        std::string strEmail = "123456789@qq.com"; // Email
        std::string strAvatar = "111";             // 头像
        std::string sign_info = "签名信息";        // 签名
        std::string strPass = "123456";            // 密码
        std::string strCompany = "密码";           // 公司
        std::string strAddress = "地址信息";       // 地址

        stmt->SetParam(index++, strSalt);
        stmt->SetParam(index++, nSex);
        stmt->SetParam(index++, strNick);
        stmt->SetParam(index++, strOutPass);
        stmt->SetParam(index++, strDomain);
        stmt->SetParam(index++, strName);
        stmt->SetParam(index++, strTel);
        stmt->SetParam(index++, strEmail);
        stmt->SetParam(index++, strCompany);
        stmt->SetParam(index++, strAddress);
        stmt->SetParam(index++, strAvatar);
        stmt->SetParam(index++, sign_info);
        stmt->SetParam(index++, nDeptId);
        stmt->SetParam(index++, nStatus);
        stmt->SetParam(index++, nNow);
        stmt->SetParam(index++, nNow);
        bRet = stmt->ExecuteUpdate();

        if (!bRet)
        {
            LOG(INFO) << "insert user failed: " << strSql;
        }
        else
        {
            uint32_t nId = stmt->GetInsertId();
        }
    }
    delete stmt;

    return true;
}

void *workUsePool(void *arg)
{
    DBPool *pDBPool = (DBPool *)arg;
    DBConn *pDBConn = pDBPool->GetDBConn();
    if (pDBConn)
    {
        bool ret = insertUser(pDBConn);
        if (!ret)
        {
            LOG(INFO) << "insertUser failed";
        }
    }
    else
    {
        LOG(INFO) << "GetDBConn failed";
    }
    pDBPool->ReleaseDBConn(pDBConn);
    return NULL;
}

void *workNotUsePool(void *arg)
{
    // LOG(INFO)<< "workNotUsePool";
    char *db_pool_name = "mypool";
    char *db_host = "127.0.0.1";
    int db_port = 3306;
    char *db_dbname = "mysql_pool_test";
    char *db_username = "root";
    char *db_password = "123456";
    int db_maxconncnt = 1;
    DBPool *pDBPool = new DBPool(db_pool_name, db_host, db_port,
                                 db_username, db_password, db_dbname, db_maxconncnt);
    if (!pDBPool)
    {
        LOG(INFO) << "workNoPool new DBPool failed";
        return NULL;
    }
    if (pDBPool->Init())
    {
        LOG(INFO) << "init db instance failed: " << db_pool_name;
        return NULL;
    }

    DBConn *pDBConn = pDBPool->GetDBConn();
    if (pDBConn)
    {
        bool ret = insertUser(pDBConn);
        if (!ret)
        {
            LOG(INFO) << "insertUser failed";
        }
    }
    else
    {
        LOG(INFO) << "GetDBConn failed";
    }
    pDBPool->ReleaseDBConn(pDBConn);
    delete pDBPool;
    return NULL;
}

// 查看最大连接数 show variables like '%max_connections%';
// 修改最大连接数 set GLOBAL max_connections = 200;
// 如果是root帐号，你能看到所有用户的当前连接。如果是其它普通帐号，只能看到自己占用的连接。
// 命令： show processlist;
int main(int argc, char *argv[])
{
    InitLog(argv[0]);
    int thread_num = 1;    // 线程池线程数量
    int db_maxconncnt = 4; // 连接池最大连接数量(核数*2 + 磁盘数量)
    int use_pool = 1;      // 是否使用连接池
    if (argc != 4)
    {
        std::cout << "usage:  ./test_ThreadPool thread_num db_maxconncnt use_pool" << std::endl;
        std::cout << "example: ./test_ThreadPool 4  4 1" << std::endl;

        return 1;
    }
    thread_num = atoi(argv[1]);
    db_maxconncnt = atoi(argv[2]);
    use_pool = atoi(argv[3]);

    char *db_pool_name = "mypool";
    char *db_host = "127.0.0.1";
    int db_port = 3306;
    char *db_dbname = "mysql_pool_test";
    char *db_username = "root";
    char *db_password = "123456";

    DBPool *pDBPool = new DBPool(db_pool_name, db_host, db_port,
                                 db_username, db_password, db_dbname, db_maxconncnt);

    if (pDBPool->Init())
    {
        LOG(ERROR) << "init db instance failed:" << db_pool_name;
        return 1;
    }

    DBConn *pDBConn = pDBPool->GetDBConn();
    if (pDBConn)
    {
        bool ret = pDBConn->ExecuteDrop(DROP_USER_TABLE);
        if (ret)
        {
            LOG(INFO) << "DROP_USER_TABLE ok";
        }
        // 1. 创建表
        ret = pDBConn->ExecuteCreate(CREATE_USER_TABLE);
        if (ret)
        {
            LOG(INFO) << "CREATE_USER_TABLE ok";
        }
        else
        {
            LOG(INFO) << "ExecuteCreate failed";
            return -1;
        }
    }
    pDBPool->ReleaseDBConn(pDBConn);

    LOG(INFO) << "thread_num = " << thread_num;
    ThreadPool<HeapQueue<Task *>> pool(TASK_NUMBER);
    uint64_t start_time = get_tick_count();
    for (int i = 0; i < TASK_NUMBER; i++)
    {
        if (use_pool)
        {
            pool.addOneTask(new Task(workUsePool, (void *)pDBPool));
        }
        else
        {
            pool.addOneTask(new Task(workNotUsePool, (void *)pDBPool));
        }
    }
    uint64_t end_time = get_tick_count();
    printf("threadpool need time:%lums\n", end_time - start_time);
    sleep(5);
    delete pDBPool;
    return 0;
}