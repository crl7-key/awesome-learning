## 内存池

## 目录
* [为什么需要使用内存池?](#为什么需要使用内存池)
* [内存池的实现方案](#内存池的实现方案)
* [内存池的具体实现](#内存池的具体实现)
* [代码实现](#代码实现)
* [使用示例](#使用示例)

## 为什么需要使用内存池?
在`C/C++`中通常使用`malloc`,`free`或`new`,`delete`来动态分配内存。

一方面,因为这些函数涉及到了系统调用,所以频繁的调用必然会导致程序性能的损耗;

另一方面,频繁的分配和释放小块内存会导致大量的内存碎片的产生,当碎片积累到一定的量之后,将无法分配到连续的内存空间,系统不得不进行碎片整理来满足分配到连续的空间,这样不仅会导致系统性能损耗,而且会导致程序对内存的利用率低下。

如果程序不需要频繁的分配和释放小块内存,那就没有使用内存池的必要,直接使用`malloc`,`free`或`new`,`delete`函数即可。


## 内存池的实现方案

**内存池的实现原理大致如下**：
- 提前申请一块大内存由内存池自己管理,并分成小片供给程序使用。
- 程序使用完之后将内存归还到内存池中（并没有真正的从系统释放）,当程序再次从内存池中请求内存时,内存池将池子中的可用内存片返回给程序使用。


在设计内存池的实现方案时,需要考虑到以下问题：

- 内存池是否可以自动增长？

如果内存池的最大空间是固定的（也就是非自动增长）,那么当内存池中的内存被请求完之后,程序就无法再次从内存池请求到内存。所以需要根据程序对内存的实际使用情况来确定是否需要自动增长。

- 内存池的总内存占用是否只增不减？

如果内存池是自动增长的,就涉及到了“内存池的总内存占用是否是只增不减”这个问题了。试想,程序从一个自动增长的内存池中请求了`1000`个大小为`100KB`的内存片,并在使用完之后全部归还给了内存池,而且假设程序之后的逻辑最多之后请求`10`个`100KB`的内存片,那么该内存池中的`900`个`100KB`的内存片就一直处于闲置状态,程序的内存占用就一直不会降下来。对内存占用大小有要求的程序需要考虑到这一点。


- 内存池中内存片的大小是否固定？

如果每次从内存池中的请求的内存片的大小如果不固定,那么内存池中的每个可用内存片的大小就不一致,程序再次请求内存片的时候,内存池就需要在“匹配最佳大小的内存片”和“匹配操作时间”上作出衡量。“最佳大小的内存片”虽然可以减少内存的浪费,但可能会导致“匹配时间”变长。

- 内存池是否是线程安全的？

是否允许在多个线程中同时从同一个内存池中请求和归还内存片？这个线程安全可以由内存池来实现,也可以由使用者来保证。

- 内存片分配出去之前和归还到内存池之后,其中的内容是否需要被清除？

程序可能出现将内存片归还给内存池之后,仍然使用内存片的地址指针进行内存读写操作,这样就会导致不可预期的结果。将内容清零只能尽量的（也不一定能）将问题抛出来,但并不能解决任何问题,而且将内容清零会消耗一定的CPU时间。所以最终最好还是需要由内存池的使用者来保证这种安全性。

- 是否兼容std::allocator？

`STL`标准库中的大多类都支持用户提供一个自定义的内存分配器,默认使用的是`std::allocator`,如`std::string`：
```c++
typedef basic_string<char, char_traits<char>, allocator<char> > string;
```
如果我们的内存池兼容`std::allocator`,那么就可以使用我们自己的内存池来替换默认的`std::allocator`分配器，如：
```c++
typedef basic_string<char, char_traits<char>, MemoryPoll<char> > mystring;
```

## 内存池的具体实现
实现一个内存池管理的类`MemoryPool`,它具有如下特性：

- 内存池的总大小自动增长。

- 内存池中内存片的大小固定。

- 支持线程安全。
 
- 在内存片被归还之后,清除其中的内容。

- 兼容`std::allocator`。

因为内存池的内存片的大小是固定的,不涉及到需要匹配最合适大小的内存片,由于会频繁的进行插入、移除的操作,但查找比较少,故选用链表数据结构来管理内存池中的内存片。

`MemoryPool`中有`2`个链表,它们都是双向链表（设计成双向链表主要是为了在移除指定元素时,能够快速定位该元素的前后元素,从而在该元素被移除后,将其前后元素连接起来,保证链表的完整性）：

- `m_data_element` : 记录分配出去的内存片。

- `m_free_element`: 记录未被分配出去的内存片。

## 代码实现
```c++
#ifndef INC_MEMEORY_POOL_H_
#define INC_MEMEORY_POOL_H_

#include <climits>
#include <cstddef>
#include <mutex>
#include <string.h>

namespace base {
template<typename T, size_t BlockSize = 4096,bool ZeroOnDeallocate = true>
    class MemoryPool {
    public:
        // 构造函数
        MemoryPool() noexcept;

        // 拷贝构造函数
        MemoryPool(const MemoryPool&)noexcept;

        // 移动构造
        MemoryPool(MemoryPool&&)noexcept;

        template<typename U> MemoryPool(const MemoryPool<U>&) noexcept;

        virtual ~MemoryPool()noexcept;

        MemoryPool& operator=(const MemoryPool& memoryPool) = delete;

        MemoryPool& operator=(MemoryPool&& memoryPool) noexcept;

        T* address(T& x) const noexcept;

        const T* address(const T& x)const noexcept;

        // 一次只能分配一个对象。 
        T* allocate(size_t n = 1, const T* hint = nullptr);

        // 取消分配,一只取消一个对象
        void deallocate(T* ptr, size_t n = 1);

        size_t max_size() const noexcept;

        template <class U, class... Args> void construct(U* p, Args&&... args);
        template <class U> void destroy(U* p);

	// 新增元素
        template <class... Args> T* newElement(Args&&... args);
		
	// 删除元素
        void deleteElement(T* p);

    private:
        // 双向链表节点
        struct ElementListNode {
            ElementListNode *pre;
            ElementListNode *next;
        };

        ElementListNode* m_data_element; // 记录分配出去的内存片
        ElementListNode* m_free_element; // 记录未分配出去的内存片

        // 递归锁,主要用在可能被 连续多次上锁（期间未解锁）的情形
        std::recursive_mutex m_mt;

        size_t padPointer(char* ptr, size_t align) const noexcept;
        void allocateBlock();

        // 静态断言
        static_assert(BlockSize >= 2 * sizeof(ElementListNode), "BlockSize too small.");
    };

// 构造函数实现
template <typename T,size_t BlockSize, bool ZeroOnDeallocate>
    MemoryPool<T,BlockSize,ZeroOnDeallocate>::MemoryPool() 
    noexcept {
        m_data_element = nullptr;
        m_free_element = nullptr;
    }

// 拷贝构造函数实现
template <typename T,size_t BlockSize, bool ZeroOnDeallocate>
    MemoryPool<T,BlockSize,ZeroOnDeallocate>::MemoryPool(const MemoryPool&) 
    noexcept :
    MemoryPool() 
    {}

// 移动构造函数实现
template <typename T, size_t BlockSize, bool ZeroOnDeallocate>
    MemoryPool<T, BlockSize, ZeroOnDeallocate>::MemoryPool(MemoryPool&& memoryPool) 
    noexcept {
        std::lock_guard<std::recursive_mutex> lock(m_mt);

        m_data_element = memoryPool.m_data_element;
        memoryPool.m_data_element = nullptr;
        m_free_element = memoryPool.m_free_element;
        memoryPool.m_free_element = nullptr;
    }

template <typename T, size_t BlockSize, bool ZeroOnDeallocate>
    template<class U>
    MemoryPool<T, BlockSize,ZeroOnDeallocate>::MemoryPool(const MemoryPool<U>&)
    noexcept :
    MemoryPool()
    {}

// 析构函数实现
template <typename T, size_t BlockSize, bool ZeroOnDeallocate>
    MemoryPool<T, BlockSize, ZeroOnDeallocate>::~MemoryPool()
    noexcept {
        std::lock_guard<std::recursive_mutex> lock(m_mt);

        ElementListNode* pcurr = m_data_element;
        while(pcurr != nullptr) {
            ElementListNode* prev = pcurr->next;
            operator delete(reinterpret_cast<void*>(pcurr));
            pcurr = nullptr;
            pcurr = prev;
        }

        pcurr = m_free_element;
        while(pcurr != nullptr) {
            ElementListNode* prev = pcurr->next;
            operator delete(reinterpret_cast<void*>(pcurr));
            pcurr = nullptr;
            pcurr = prev;

        }
    }

// 赋值函数实现
template <typename T, size_t BlockSize, bool ZeroOnDeallocate>
    MemoryPool<T, BlockSize,ZeroOnDeallocate>&
    MemoryPool<T, BlockSize, ZeroOnDeallocate>::operator=(MemoryPool&& memoryPool) 
    noexcept{
        std::lock_guard<std::recursive_mutex> lock(m_mt);

        if (this != &memoryPool) {
            std::swap(m_data_element,memoryPool.m_data_element);
            std::swap(m_free_element,memoryPool.m_free_element);
        }
        return *this;
    }


template <typename T, size_t BlockSize, bool ZeroOnDeallocate>
    inline T*
    MemoryPool<T, BlockSize,ZeroOnDeallocate>::address(T& x)
    const noexcept {
        return &x;
    }

template <typename T, size_t BlockSize, bool ZeroOnDeallocate>
    inline const T*
    MemoryPool<T,BlockSize,ZeroOnDeallocate>::address(const T& x) 
    const noexcept {
        return &x;
    }


// 分配内存
template <typename T, size_t BlockSize, bool ZeroOnDeallocate>
    inline T* MemoryPool<T,BlockSize,ZeroOnDeallocate>::allocate(size_t n, const T* hint) {
        std::lock_guard<std::recursive_mutex> lock(m_mt);

        if (m_free_element != nullptr) {
            char* body = reinterpret_cast<char*>(reinterpret_cast<char*>(m_free_element) + sizeof(ElementListNode));

            size_t bodyPadding = padPointer(body,alignof(ElementListNode));

            T* res = reinterpret_cast<T*>(reinterpret_cast<char*>(body + bodyPadding));

            ElementListNode* tmp = m_free_element;

            m_free_element = m_free_element->next;

            if (m_free_element) {
                m_free_element->pre = nullptr;
            }

            tmp->next = m_data_element;

            if (m_data_element) 
                m_data_element->pre = tmp;

            tmp->pre = nullptr;
            m_data_element = tmp;

            return res;

        }  else {

            allocateBlock();

            char* body = reinterpret_cast<char*>(reinterpret_cast<char*>(m_data_element) + sizeof(ElementListNode));

            size_t bodyPadding = padPointer(body,alignof(ElementListNode));

            T* res = reinterpret_cast<T*>(reinterpret_cast<char*>(body + bodyPadding));

            return res;
        }
    }


// 取消分配
template <typename T, size_t BlockSize, bool ZeroOnDeallocate>
    inline void
    MemoryPool<T, BlockSize,ZeroOnDeallocate>::deallocate(T* ptr,size_t n) {
        std::lock_guard<std::recursive_mutex> lock(m_mt);

        if (ptr != nullptr) {
            ElementListNode* p = reinterpret_cast<ElementListNode*>(reinterpret_cast<char*>(ptr) - sizeof(ElementListNode));

            if (ZeroOnDeallocate) {
                memset(reinterpret_cast<char*>(ptr), 0, BlockSize - sizeof(ElementListNode));
            }

            if (p->pre) {
                p->pre->next = p->next;
            }

            if (p->next) {
                p->next->pre = p->pre;
            }

            if (p->pre == nullptr) {
                m_data_element = p->next;
            }

            p->pre = nullptr;

            if (m_free_element) {
                p->next = m_free_element;
                m_free_element->pre = p;
            } else {
                p->next = nullptr;
            }
            m_free_element = p;
        }
    }

template <typename T, size_t BlockSize, bool ZeroOnDeallocate>
    inline size_t 
    MemoryPool<T, BlockSize, ZeroOnDeallocate>::max_size() 
    const noexcept {
        size_t maxBlcoks = -1 / BlockSize;
        return (BlockSize - sizeof(char*)) / sizeof(ElementListNode) * maxBlcoks;
    }

template <typename T, size_t BlockSize, bool ZeroOnDeallocate>
    template <class U, class... Args>
    inline void
    MemoryPool<T, BlockSize, ZeroOnDeallocate>::construct(U* p, Args&&... args) {
        new (p) U (std::forward<Args>(args)...);
    }


template <typename T, size_t BlockSize, bool ZeroOnDeallocate>
    template <class U>
    inline void
    MemoryPool<T, BlockSize, ZeroOnDeallocate>::destroy(U* p) {
        p->~U();
    }

template <typename T, size_t BlockSize, bool ZeroOnDeallocate>
    template <class... Args>
    inline T*
    MemoryPool<T, BlockSize,ZeroOnDeallocate>::newElement(Args&&... args) {
        std::lock_guard<std::recursive_mutex> lock(m_mt);
        T* res = allocate();
        construct<T>(res, std::forward<Args>(args)...);
        return res;
    }

template <typename T, size_t BlockSize, bool ZeroOnDeallocate>
    inline void
    MemoryPool<T, BlockSize, ZeroOnDeallocate>::deleteElement(T* p) {
        std::lock_guard<std::recursive_mutex> lock(m_mt);
        if (p != nullptr) {
            p->~T();
            deallocate(p);
        }
    }

template <typename T, size_t BlockSize, bool ZeroOnDeallocate>
    inline size_t 
    MemoryPool<T,BlockSize,ZeroOnDeallocate>::padPointer(char* ptr, size_t align)
    const noexcept {
        uintptr_t res = reinterpret_cast<uintptr_t>(ptr);
        return ((align - res) % align);
    }

template <typename T, size_t BlockSize, bool ZeroOnDeallocate>
    void 
    MemoryPool<T, BlockSize, ZeroOnDeallocate>::allocateBlock() {
        // 为新块分配空间并存储指向上一个块的指针
        char* newBlock = reinterpret_cast<char*>(operator new(BlockSize));

        ElementListNode* new_ele_ptr = reinterpret_cast<ElementListNode*>(newBlock);

        new_ele_ptr->pre = nullptr;
        new_ele_ptr->next = nullptr;

        if (m_data_element) {
            m_data_element->pre = new_ele_ptr;
        }
        new_ele_ptr->next = m_data_element;
        m_data_element = new_ele_ptr;
    }

}

#endif // INC_MEMEORY_POOL_H_
```

## 使用示例
```c++
#include <iostream>
#include <thread>
#include "MemoryPool.h"

using namespace std;
using namespace base;


class Object {
public:

    Object() : m_id(0) {
        cout << "Object()" << endl;
    }

     ~Object() {
        cout << "~Object()" << endl;
    }

    Object(int id) : m_id(id) {
        cout << "Object("<< id << ")" << endl;
    }

    void setId(int id) {
        m_id = id;
    }

    int getId() {
        return m_id;
    }

private:
    int m_id;
};


void ThreadProc(base::MemoryPool<char> *mp) {
    int i = 0;
    while (i++ < 100000) {
        char* p0 = (char*)mp->allocate();

        char* p1 = (char*)mp->allocate();

        mp->deallocate(p0);

        char* p2 = (char*)mp->allocate();

        mp->deallocate(p1);

        mp->deallocate(p2);
    }
}

int main()
{
    base::MemoryPool<char> mp;

    int i = 0;

    while(i++ < 10000000) {
        char* p0 = (char*)mp.allocate();

        char* p1 = (char*)mp.allocate();

        mp.deallocate(p0);

        char* p2 = (char*)mp.allocate();

        mp.deallocate(p1);

        mp.deallocate(p2);
    }

    std::thread th0(ThreadProc, &mp);
    std::thread th1(ThreadProc, &mp);
    std::thread th2(ThreadProc, &mp);

    th0.join();
    th1.join();
    th2.join();


    Object *object = nullptr;
    {
        base::MemoryPool<Object> mp2;
        object = mp2.newElement(10);
        int a = object->getId();
        object->setId(10);
        a = object->getId();

        mp2.deleteElement(object);
    }

    object->setId(12);
    int b = -4 % 4;

    int *a = nullptr;
    {
        base::MemoryPool<int, 180> mp3;
        a =  mp3.allocate();
        *a = 100;
        //mp3.deallocate(a);

        int *b =  mp3.allocate();
        *b = 200;
        //mp3.deallocate(b);

        mp3.deallocate(a);
        mp3.deallocate(b);

        int *c = mp3.allocate();
        *c = 300;
    }

    getchar();
    
    return 0;
}
```
执行:
```bash
g++ main.cpp -lpthread -o memory_test

./memory_test
```
输出结果:
```bash
Object(10)
~Object()
```

**[⬆ 返回顶部](#目录)**
