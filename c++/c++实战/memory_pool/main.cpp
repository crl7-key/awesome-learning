#include <iostream>
#include <thread>
#include "MemoryPool.h"

using namespace std;
using namespace base;


class Object {
public:

    Object() : m_id(0) {
        cout << "Object()" << endl;
    }

     ~Object() {
        cout << "~Object()" << endl;
    }

    Object(int id) : m_id(id) {
        cout << "Object( "<< id << " )" << endl;
    }

    void setId(int id) {
        m_id = id;
    }

    int getId() {
        return m_id;
    }

private:
    int m_id;
};


void ThreadProc(base::MemoryPool<char> *mp) {
    int i = 0;
    while (i++ < 100000) {
        char* p0 = (char*)mp->allocate();

        char* p1 = (char*)mp->allocate();

        mp->deallocate(p0);

        char* p2 = (char*)mp->allocate();

        mp->deallocate(p1);

        mp->deallocate(p2);
    }
}

int main()
{
    base::MemoryPool<char> mp;

    int i = 0;

    while(i++ < 10000000) {
        char* p0 = (char*)mp.allocate();

        char* p1 = (char*)mp.allocate();

        mp.deallocate(p0);

        char* p2 = (char*)mp.allocate();

        mp.deallocate(p1);

        mp.deallocate(p2);
    }

    std::thread th0(ThreadProc, &mp);
    std::thread th1(ThreadProc, &mp);
    std::thread th2(ThreadProc, &mp);

    th0.join();
    th1.join();
    th2.join();


    Object *object = nullptr;
    {
        base::MemoryPool<Object> mp2;
        object = mp2.newElement(10);
        int a = object->getId();
        object->setId(10);
        a = object->getId();

        mp2.deleteElement(object);
    }

    object->setId(12);
    int b = -4 % 4;

    int *a = nullptr;
    {
        base::MemoryPool<int, 180> mp3;
        a =  mp3.allocate();
        *a = 100;
        //mp3.deallocate(a);

        int *b =  mp3.allocate();
        *b = 200;
        //mp3.deallocate(b);

        mp3.deallocate(a);
        mp3.deallocate(b);

        int *c = mp3.allocate();
        *c = 300;
    }

    getchar();
    return 0;
}

