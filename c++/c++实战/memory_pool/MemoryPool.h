#ifndef INC_MEMEORY_POOL_H_
#define INC_MEMEORY_POOL_H_

#include <climits>
#include <cstddef>
#include <mutex>
#include <string.h>

namespace base {
template<typename T, size_t BlockSize = 4096,bool ZeroOnDeallocate = true>
    class MemoryPool {
    public:
        // 构造函数
        MemoryPool() noexcept;

        // 拷贝构造函数
        MemoryPool(const MemoryPool&)noexcept;

        // 移动构造
        MemoryPool(MemoryPool&&)noexcept;

        template<typename U> MemoryPool(const MemoryPool<U>&) noexcept;

        virtual ~MemoryPool()noexcept;

        MemoryPool& operator=(const MemoryPool& memoryPool) = delete;

        MemoryPool& operator=(MemoryPool&& memoryPool) noexcept;

        T* address(T& x) const noexcept;

        const T* address(const T& x)const noexcept;

        // 一次只能分配一个对象。 
        T* allocate(size_t n = 1, const T* hint = nullptr);

        // 取消分配,一只取消一个对象
        void deallocate(T* ptr, size_t n = 1);

        size_t max_size() const noexcept;

        template <class U, class... Args> void construct(U* p, Args&&... args);
        template <class U> void destroy(U* p);

		// 新增元素
        template <class... Args> T* newElement(Args&&... args);
		
		// 删除元素
        void deleteElement(T* p);

    private:
        // 双向链表节点
        struct ElementListNode {
            ElementListNode *pre;
            ElementListNode *next;
        };

        ElementListNode* m_data_element; // 记录分配出去的内存片
        ElementListNode* m_free_element; // 记录未分配出去的内存片

        // 递归锁,主要用在可能被 连续多次上锁（期间未解锁）的情形
        std::recursive_mutex m_mt;

        size_t padPointer(char* ptr, size_t align) const noexcept;
        void allocateBlock();

        // 静态断言
        static_assert(BlockSize >= 2 * sizeof(ElementListNode), "BlockSize too small.");
    };

// 构造函数实现
template <typename T,size_t BlockSize, bool ZeroOnDeallocate>
    MemoryPool<T,BlockSize,ZeroOnDeallocate>::MemoryPool() 
    noexcept {
        m_data_element = nullptr;
        m_free_element = nullptr;
    }

// 拷贝构造函数实现
template <typename T,size_t BlockSize, bool ZeroOnDeallocate>
    MemoryPool<T,BlockSize,ZeroOnDeallocate>::MemoryPool(const MemoryPool&) 
    noexcept :
    MemoryPool() 
    {}

// 移动构造函数实现
template <typename T, size_t BlockSize, bool ZeroOnDeallocate>
    MemoryPool<T, BlockSize, ZeroOnDeallocate>::MemoryPool(MemoryPool&& memoryPool) 
    noexcept {
        std::lock_guard<std::recursive_mutex> lock(m_mt);

        m_data_element = memoryPool.m_data_element;
        memoryPool.m_data_element = nullptr;
        m_free_element = memoryPool.m_free_element;
        memoryPool.m_free_element = nullptr;
    }

template <typename T, size_t BlockSize, bool ZeroOnDeallocate>
    template<class U>
    MemoryPool<T, BlockSize,ZeroOnDeallocate>::MemoryPool(const MemoryPool<U>&)
    noexcept :
    MemoryPool()
    {}

// 析构函数实现
template <typename T, size_t BlockSize, bool ZeroOnDeallocate>
    MemoryPool<T, BlockSize, ZeroOnDeallocate>::~MemoryPool()
    noexcept {
        std::lock_guard<std::recursive_mutex> lock(m_mt);

        ElementListNode* pcurr = m_data_element;
        while(pcurr != nullptr) {
            ElementListNode* prev = pcurr->next;
            operator delete(reinterpret_cast<void*>(pcurr));
            pcurr = nullptr;
            pcurr = prev;
        }

        pcurr = m_free_element;
        while(pcurr != nullptr) {
            ElementListNode* prev = pcurr->next;
            operator delete(reinterpret_cast<void*>(pcurr));
            pcurr = nullptr;
            pcurr = prev;

        }
    }

// 赋值函数实现
template <typename T, size_t BlockSize, bool ZeroOnDeallocate>
    MemoryPool<T, BlockSize,ZeroOnDeallocate>&
    MemoryPool<T, BlockSize, ZeroOnDeallocate>::operator=(MemoryPool&& memoryPool) 
    noexcept{
        std::lock_guard<std::recursive_mutex> lock(m_mt);

        if (this != &memoryPool) {
            std::swap(m_data_element,memoryPool.m_data_element);
            std::swap(m_free_element,memoryPool.m_free_element);
        }
        return *this;
    }


template <typename T, size_t BlockSize, bool ZeroOnDeallocate>
    inline T*
    MemoryPool<T, BlockSize,ZeroOnDeallocate>::address(T& x)
    const noexcept {
        return &x;
    }

template <typename T, size_t BlockSize, bool ZeroOnDeallocate>
    inline const T*
    MemoryPool<T,BlockSize,ZeroOnDeallocate>::address(const T& x) 
    const noexcept {
        return &x;
    }


// 分配内存
template <typename T, size_t BlockSize, bool ZeroOnDeallocate>
    inline T* MemoryPool<T,BlockSize,ZeroOnDeallocate>::allocate(size_t n, const T* hint) {
        std::lock_guard<std::recursive_mutex> lock(m_mt);

        if (m_free_element != nullptr) {
            char* body = reinterpret_cast<char*>(reinterpret_cast<char*>(m_free_element) + sizeof(ElementListNode));

            size_t bodyPadding = padPointer(body,alignof(ElementListNode));

            T* res = reinterpret_cast<T*>(reinterpret_cast<char*>(body + bodyPadding));

            ElementListNode* tmp = m_free_element;

            m_free_element = m_free_element->next;

            if (m_free_element) {
                m_free_element->pre = nullptr;
            }

            tmp->next = m_data_element;

            if (m_data_element) 
                m_data_element->pre = tmp;

            tmp->pre = nullptr;
            m_data_element = tmp;

            return res;

        }  else {

            allocateBlock();

            char* body = reinterpret_cast<char*>(reinterpret_cast<char*>(m_data_element) + sizeof(ElementListNode));

            size_t bodyPadding = padPointer(body,alignof(ElementListNode));

            T* res = reinterpret_cast<T*>(reinterpret_cast<char*>(body + bodyPadding));

            return res;
        }
    }


// 取消分配
template <typename T, size_t BlockSize, bool ZeroOnDeallocate>
    inline void
    MemoryPool<T, BlockSize,ZeroOnDeallocate>::deallocate(T* ptr,size_t n) {
        std::lock_guard<std::recursive_mutex> lock(m_mt);

        if (ptr != nullptr) {
            ElementListNode* p = reinterpret_cast<ElementListNode*>(reinterpret_cast<char*>(ptr) - sizeof(ElementListNode));

            if (ZeroOnDeallocate) {
                memset(reinterpret_cast<char*>(ptr), 0, BlockSize - sizeof(ElementListNode));
            }

            if (p->pre) {
                p->pre->next = p->next;
            }

            if (p->next) {
                p->next->pre = p->pre;
            }

            if (p->pre == nullptr) {
                m_data_element = p->next;
            }

            p->pre = nullptr;

            if (m_free_element) {
                p->next = m_free_element;
                m_free_element->pre = p;
            } else {
                p->next = nullptr;
            }
            m_free_element = p;
        }
    }

template <typename T, size_t BlockSize, bool ZeroOnDeallocate>
    inline size_t 
    MemoryPool<T, BlockSize, ZeroOnDeallocate>::max_size() 
    const noexcept {
        size_t maxBlcoks = -1 / BlockSize;
        return (BlockSize - sizeof(char*)) / sizeof(ElementListNode) * maxBlcoks;
    }

template <typename T, size_t BlockSize, bool ZeroOnDeallocate>
    template <class U, class... Args>
    inline void
    MemoryPool<T, BlockSize, ZeroOnDeallocate>::construct(U* p, Args&&... args) {
        new (p) U (std::forward<Args>(args)...);
    }


template <typename T, size_t BlockSize, bool ZeroOnDeallocate>
    template <class U>
    inline void
    MemoryPool<T, BlockSize, ZeroOnDeallocate>::destroy(U* p) {
        p->~U();
    }

template <typename T, size_t BlockSize, bool ZeroOnDeallocate>
    template <class... Args>
    inline T*
    MemoryPool<T, BlockSize,ZeroOnDeallocate>::newElement(Args&&... args) {
        std::lock_guard<std::recursive_mutex> lock(m_mt);
        T* res = allocate();
        construct<T>(res, std::forward<Args>(args)...);
        return res;
    }

template <typename T, size_t BlockSize, bool ZeroOnDeallocate>
    inline void
    MemoryPool<T, BlockSize, ZeroOnDeallocate>::deleteElement(T* p) {
        std::lock_guard<std::recursive_mutex> lock(m_mt);
        if (p != nullptr) {
            p->~T();
            deallocate(p);
        }
    }

template <typename T, size_t BlockSize, bool ZeroOnDeallocate>
    inline size_t 
    MemoryPool<T,BlockSize,ZeroOnDeallocate>::padPointer(char* ptr, size_t align)
    const noexcept {
        uintptr_t res = reinterpret_cast<uintptr_t>(ptr);
        return ((align - res) % align);
    }

template <typename T, size_t BlockSize, bool ZeroOnDeallocate>
    void 
    MemoryPool<T, BlockSize, ZeroOnDeallocate>::allocateBlock() {
        // 为新块分配空间并存储指向上一个块的指针
        char* newBlock = reinterpret_cast<char*>(operator new(BlockSize));

        ElementListNode* new_ele_ptr = reinterpret_cast<ElementListNode*>(newBlock);

        new_ele_ptr->pre = nullptr;
        new_ele_ptr->next = nullptr;

        if (m_data_element) {
            m_data_element->pre = new_ele_ptr;
        }
        new_ele_ptr->next = m_data_element;
        m_data_element = new_ele_ptr;
    }

}

#endif // INC_MEMEORY_POOL_H_
