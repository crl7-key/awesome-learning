## 基于`channel`信号量来实现互斥锁

### 实现原理
当把`channel`的容量设置为`1`时,进行`P`操作的协程只能有一个，在该协程执行完`P`操作没有执行`V`操作时，其他协程只能等待，这样就实现了只能有一个协程访问临界区的功能,`P`和`V`操作就变成了`lock`和`unlock`操作。
```golang
/* mutexes */
func (s semaphore) Lock() {
    s.P(1)
}

func (s semaphore) Unlock() {
    s.V(1)
}
```
当`channel`容量时`1`时，就只能有一个协程能完成`P`操作，直到该协程完成了`V`操作，其他协程才能进行`P`操作，这样就实现了互斥锁。

### 代码实现
```golang
package main

import (
	"fmt"
	"time"
)

type Empty interface{}

type semaphore chan Empty

// acquire n resources
func (s semaphore) P(n int) {
	e := new(Empty)
	for i := 0; i < n; i++ {
		s <- e
	}
}

// release n resources
func (s semaphore) V(n int) {
	for i := 0; i < n; i++ {
		<-s
	}
}

/* mutexes */
func (s semaphore) Lock() {
	s.P(1)
}

func (s semaphore) Unlock() {
	s.V(1)
}

// 工作协程，打印0-99的整数
func printInt(sem semaphore) {
	sem.Lock()
	for i := 0; i < 100; i++ {
		fmt.Println(i)
	}
	sem.Unlock()
}

func main() {
	sem := make(semaphore, 1)

	go printInt(sem)
	go printInt(sem)

	// 等待两个goroutine结束
	time.Sleep(10e9)
	fmt.Println("endl")
}
```
