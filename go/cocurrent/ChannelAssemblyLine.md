## 通过`channel`实现流水线作业模型

### 概述
流水线作业,是多个线程协作,就像加工车间的传送带,每个线程完成一项任务,然后把结果发送给下一个线程,直到所有线程的任务完成,一个“产品”的加工过程就完成了。

通过`Golang`的`channel`,可以很方便的实现流水线作业。

### 流水线作业的实现
在`linux`系统编程中,一般来说流水线作业的线程数是固定的,我们模拟的这个场景,假设有几个固定的`goroutine`共同协作来完成一个字符串的处理,`goroutine`之间通过`channel`来进行数据传递,各个`goroutine`主要进行的操作如下：
- 第`1`个`goroutine`：一个字符串全部改成小写

- 第`2`个：在该字符串前面添加`HELLO`: 字样

- 第`3`个：统计字数，并打印字符串

### 代码实现
```golang
package main

import (
	"fmt"
	"os"
	"strings"
	"time"
)

// 组装初始化数据,发送数据
func Starter(ch1 chan string) {
	for i := 0; i < 10; i++ {
		v := fmt.Sprintf("test: %d", i)
		fmt.Fprintf(os.Stderr, "starter send: %s\n", v)
		ch1 <- v
		time.Sleep(1e9)
	}
}

// 字符串全部改成小写
func LowString(ch1 chan string, ch2 chan string) {
	for {
		str := <-ch1
		res := strings.ToLower(str)
		ch2 <- res
	}
}

// 在该字符串前面添加HELLO: 字样
func AddString(ch2 chan string, ch3 chan string) {
	for {
		str := <-ch2
		res := fmt.Sprintf("HELLO %s\n", str)
		ch3 <- res
	}
}

// 获取结果
func GetResult(ch3 chan string) {
	for {
		v := <-ch3
		len := len(v)
		fmt.Fprintf(os.Stderr, "len=%d, result=%s\n", len, v)
	}
}

func main() {
	ch := make(chan string)
	go Starter(ch)

	ch1 := make(chan string)
	go LowString(ch, ch1)

	ch2 := make(chan string)
	go AddString(ch1, ch2)
	go GetResult(ch2)

	time.Sleep(100e9)
}
```
