## 通过`channel`来实现信号量原语

### 概述
信号量是一种用于提供不同进程间或一个给定进程的不同线程间同步手段的原语。对于一个信号量来说，基本的操作有三个：

- 创建一个信号量

创建一个信号量会给该信号量一个初始值，对于二元信号量来说它是`1`。

- 等待（`wait`）一个信号量

该操作会测试这个信号量的值，如果其值小于或等于`0`，就等待（阻塞），一旦其值变为大于0就将它减1。

该操作也被称为P操作，或递减(`down`)，或上锁(`lock`)。该操作必须是原子的。

- 发送（`signal`）一个信号量

该操作将信号量加`1`。若有一些进程阻塞着等待该信号量的值变为大于`0`，其中一个进程/线程被唤醒。同样，该操作必须是原子的。

该操作也被称为`V`操作，或递增(`up`)，或解锁(`unlock`)发送信号(`singal`)。

### 通过`channel`来实现信号量原语
信号量是非常常用的同步机制，可以用来实现互斥锁，多个资源的互斥访问，解决`reader-writer`的问题。在`Golang`中没有实现信号量，但可以通过`channel`来实现。通过`channel`来实现，基于以下事实：

- `buffered channel`的容量大小是我们希望同步的资源数

- `channel`目前的长度，是目前的可用资源数

- `channel`的容量减去`channel`目前的长度，是释放的资源数

我们并不关心`channel中`的值，仅关心`channel`的长度。因此，我们可以先定义一个长度为`0`的空`channel`。

```golang
type Empty interface {}
type semaphore chan Empty
```
现在创建一个有`N`个元素的`channel`：
```golang
sem = make(semaphore, N)
```

同步原语操作设计如下：
```golang
// acquire n resources
func (s semaphore) P(n int) {
    e := new(Empty)
    for i := 0; i < n; i++ {
        s <- e
    }
}

// release n resources
func (s semaphore) V(n int) {
    for i := 0; i < n; i++ {
        <-s
    }
}
```

实现`P`操作时，其实是不断的往`channel`中放入一个数据，当`channel`满时，其他协程就不能再往`channel`放数据了，而只能阻塞，直到有协程释放一个资源，也就是执行`V`操作。

而`V`操作其实就是从`channel`中取出资源。

