## `Golang`中`select`的使用

## 目录
* [概述](#概述)
* [`select`的要点](#select的要点)
* [`select`的基本格式](#select的基本格式)
* [通过`select`来检测`channel`的关闭事件](#通过select来检测channel的关闭事件)
* [多个`channel`同时准备好读的情况](#多个channel同时准备好读的情况)
* [没有任何`channel`准备好处理超时](#没有任何channel准备好处理超时)
* [没有任何`channel`准备好处理默认事件](#没有任何channel准备好处理默认事件)
* [通过`channel`通知从而退出死循环](#通过channel通知从而退出死循环)
* [永远等待](#永远等待)

### 概述
主要描述`select`的用法,通过`select`可以监听多个`channel`的读写事件。这很类似于`linux`系统编程的`select`函数。但在`Golang`中,实现的机制明显是不同的。`linux`系统编程的`select`是轮训的机制,而且监控的是文件描述符,且有数量的限制。`Golang`中的`select`和`channel`配合使用,监控的是`channel`的读写状态。


### `select`的要点
- `select`会阻塞在多个`channel`上,对多个`channel`的读/写事件进行监控。

- `select`中对`case`语句的判断不是顺序进行的。

- 在`select`中执行`case`语句时,不会自动的`fall through`。

- 在`select`中所有`channel`的读和写都被认为是同时进行的。

- `case`中的`channel`的事件包括：读取的时候,`channel`被`close`,或写入时`channel`没有空间。

- 当所有`channel`都没有数据读取时,`select`阻塞,当其中有一个`channel`有数据时则进行处理。

- 可以为`select`设置一个超时时间,当`select`超时时,可以完成一些其他工作。

### `select`的基本格式
```golang
var c1, c2 <-chan interface{}
var c3 chan<- interface{}
select {
case <- c1:              // 监听channel的读事件
    // Do something
case <- c2:             // 读事件
    // Do something
case c3<- struct{}{}:   // 监控channel的写事件
    // Do something
}
```

### 通过`select`来检测`channel`的关闭事件
```golang
package main

import (
	"fmt"
	"time"
)

func main() {

	start := time.Now()
	done := make(chan interface{})

	go func() {
		time.Sleep(2 * time.Second)
		close(done)
	}()

	fmt.Println("Blocking on read...")
	select {
	case <-done:
		fmt.Printf("Unblocked %v later.\n", time.Since(start))
	}
}

```
>tips: 当`close channel`时,读取`channel`的一方会从`channel`中读取到`value`,`false`,此时的`value`一般情况下为`nil`。
该例子也可以用来通知当不使用`channel`时,关闭`channel`的情况。

### 多个`channel`同时准备好读的情况

```golang
package main

import (
	"fmt"
)

func main() {

	c1 := make(chan interface{})
	close(c1)
	c2 := make(chan interface{})
	close(c2)
	c3 := make(chan interface{})
	close(c3)

	var c1Count, c2Count, c3Count int
	for i := 1000; i >= 0; i-- {
		select {
		case <-c1:
			c1Count++
		case <-c2:
			c2Count++
		case <-c3:
			c3Count++
		}
	}
	fmt.Printf("c1Count: %d\nc2Count: %d\nc3Count: %d\n", c1Count, c2Count, c3Count)
}
````
输出如下:
```sh
c1Count: 348
c2Count: 320
c3Count: 333
```
多运行几次,可以看出,几个数字相差都不是很大。

以上例子,同时有`3`个`channel`可读取,从以上的输出可以看出,`select`对多个`channel`的读取调度是基本公平的。让每一个`channel`的数据都有机会被处理。

### 没有任何`channel`准备好,处理超时

在很多情况下,当`channel`没有准备好时,我们希望能够设置一个超时时间,并在等待`channel`超时时进行一些处理。此时就可以按以下方式来进行编码：
```golang
package main

import (
	"fmt"
	"time"
)

func main() {
	var c <-chan int
	for {
		select {
		case <-c:
		case <-time.After(1 * time.Second):
			fmt.Println("Timed out.Do something.")
		}
	}
}
```
该代码会每隔`1`秒钟,打印出：
```golang
Timed out.Do something.
Timed out.Do something.
Timed out.Do something.
...
```
这样`select`就变成了“非阻塞”模式,我们可以设定一个时间,当没有`channel`可处理时,可以处理超时时间。这也是后台服务器编程常用的处理方式。

### 没有任何`channel`准备好,处理默认事件
当没有任何`channel`准备好数据时,可以设置是执行默认的处理代码。
```golang
package main

import (
	"fmt"
	"time"
)

func main() {
	start := time.Now()
	var c1, c2 <-chan int
	select {
	case <-c1:
	case <-c2:

	default:
		fmt.Printf("In default after %v\n\n", time.Since(start))
	}
}
```
>注意:`default`和处理超时不同,当没有`channel`可读取时,会立即执行`default`分支。而超时的处理,必须要等到超时,才处理。


### 通过`channel`通知,从而退出死循环
```golang
package main

import (
	"fmt"
	"time"
)

func main() {
	done := make(chan interface{})

	go func() {
		time.Sleep(2 * time.Second)
		close(done)
	}()

	workCounter := 0
loop:
	for {
		select {
		case <-done:
			break loop
		default:
		}

		// Simulate work
		workCounter++
		time.Sleep(1 * time.Second)
	}

	fmt.Printf("在通知退出循环时,执行了%d次.\n", workCounter)
}
```
启动一个`goroutine`,该`goroutine`在`2s`后关闭`channel`。此时主协程会在`select`中的`case <-done`分支中得到通知,跳出死循环。而在此之前会执行`default`分支的代码,这里是什么都不做。

### 永远等待
```golang
select{}
```
以上的语句会永远等待,直到有信号中断

**[⬆ 返回顶部](#目录)**
