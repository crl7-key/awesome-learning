## `sync`包`WaitGroup`的使用

## 目录
* [概述](#概述)
* [主要函数](#主要函数)
* [实践](#实践)
    * [等待某个协程结束](#等待某个协程结束)
    * [等待协程组结束](#等待协程组结束)

### 概述

`WaitGroup`用于等待一组线程的结束。父线程调用`Add`方法来设定应等待的线程的数量。每个被等待的线程在结束时应调用`Done`方法。同时,主线程里可以调用`Wait`方法阻塞至所有线程结束。

### 主要函数
```go
func (*WaitGroup) Add
```
`Add`方法向内部计数加上`delta`,`delta`可以是负数;如果内部计数器变为`0`,`Wait`方法阻塞等待的所有协程都会释放,如果计数器小于`0`,则调用`panic`;注意`Add`加上正数的调用应在`Wait`之前,否则`Wait`可能只会等待很少的协程。一般来说本方法应在创建新的协程或者其他应等待的事件之前调用。

```go
func (wg *WaitGroup) Done()
```

`Done`方法减少`WaitGroup`计数器的值,应在协程的最后执行。
```go
func (wg *WaitGroup) Wait() 
```
`Wait`方法阻塞直到`WaitGroup`计数器减为`0`

### 实践

#### 等待某个协程结束
```go
package main

import (
	"fmt"
	"sync"
	"time"
)

func main() {
	var wg sync.WaitGroup

	wg.Add(1)
	go func() {
		defer wg.Done()
		fmt.Println("1 goroutine sleep ...")
		time.Sleep(2e9)
		fmt.Println("1 goroutine exit ...")
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		fmt.Println("2 goroutine sleep ...")
		time.Sleep(4e9)
		fmt.Println("2 goroutine exit ...")
	}()

	fmt.Println("waiting for all goroutine ")
	wg.Wait()
	fmt.Println("All goroutines finished!")
}
```
要注意`Add`和`Done`函数一定要配对,否则可能发生死锁,所报的错误信息如下:
```sh
fatal error: all goroutines are asleep - deadlock!
```
#### 等待协程组结束
```go
package main

import (
	"fmt"
	"sync"
	"time"
)

func main() {
	sayHello := func(wg *sync.WaitGroup, id int) {
		defer wg.Done()
		fmt.Printf("%v goroutine say hello start ...\n", id)
		time.Sleep(2)
		fmt.Printf("%v goroutine say hello end ...\n", id)
	}

	var wg sync.WaitGroup
	const N = 5
	wg.Add(N)
	for i := 0; i < N; i++ {
		go sayHello(&wg, i)
	}

	fmt.Println("waiting for all goroutine ")
	wg.Wait()
	fmt.Println("All goroutines finished!")
}
```
无论运行多少次,都能保证`All goroutines finished`这一句在最后一行输出,这说明`Wait()`函数等了所有协程都结束自己才返回。

以上程序通过`WaitGroup`提供的三个同步接口,实现了等待一个协程组完成的同步操作。在实现时要注意：

- `Add`的参数`N`必须和创建的`goroutine`的数量相等,否则会报出死锁的错误信息

- `sayHello()`函数中要传递`WaitGroup`的指针,这是因为在该结构的实现源码中已经有说到:
```sh
A WaitGroup must not be copied after first use.
```
就是说,该结构定义后就不能被复制,所以这里要使用指针。


`WaitGroup`在需要等待多个任务结束再返回的业务来说还是很有用的,但现实中用的更多的可能是,先等待一个协程组,若所有协程组都正确完成,则一直等到所有协程组结束;若其中有一个协程发生错误,则告诉协程组的其他协程,全部停止运行（本次任务失败）以免浪费系统资源。

该场景`WaitGroup`是无法实现的,该场景要实现就需要用到通知机制,其实也可以用`channel`来实现.

**[⬆ 返回顶部](#目录)**
