package logger

type (
	// Fields use to pass to 'WithFields'
	Fields map[string]interface{}

	// Logger defines the logging interface.
	Logger interface {
		WithField(key string, value interface{}) Logger
		WithFields(fields Fields) Logger
		Print(i ...interface{})
		Printf(format string, args ...interface{})
		Println(i ...interface{})
		Debug(i ...interface{})
		Debugf(format string, args ...interface{})
		Debugln(i ...interface{})
		Info(i ...interface{})
		Infof(format string, args ...interface{})
		Infoln(i ...interface{})
		Warn(i ...interface{})
		Warnf(format string, args ...interface{})
		Warnln(i ...interface{})
		Error(i ...interface{})
		Errorf(format string, args ...interface{})
		Errorln(i ...interface{})
		Fatal(i ...interface{})
		Fatalf(format string, args ...interface{})
		Fatalln(i ...interface{})
		Panic(i ...interface{})
		Panicf(format string, args ...interface{})
		Panicln(i ...interface{})
	}
)

// std 提供模块级别的日志操作接口
// 接口实现由应用主入口提供, 此处不提供日志实现
var std Logger

// SetLogger 设置日志实现, 通常由应用主入口调用
func SetLogger(l Logger) {
	std = l
}

// StandardLogger 提供与标准库兼容的 Print/Fatal/Panic, 支持使用标准日志库的模块
func StandardLogger() Logger {
	return std
}

// WithField 向日志中增加结构化字段
func WithField(key string, value interface{}) Logger {
	return std.WithField(key, value)
}

// WithFields 向日志中增加结构化字段
func WithFields(fields Fields) Logger {
	return std.WithFields(fields)
}

// Print ...
func Print(i ...interface{}) {
	std.Print(i)
}

// Printf ...
func Printf(format string, args ...interface{}) {
	std.Printf(format, args...)
}

// Println ...
func Println(i ...interface{}) {
	std.Println(i)
}

// Debug ...
func Debug(i ...interface{}) {
	std.Debug(i)
}

// Debugf ...
func Debugf(format string, args ...interface{}) {
	std.Debugf(format, args...)
}

// Debugln ...
func Debugln(i ...interface{}) {
	std.Debugln(i)
}

// Info ...
func Info(i ...interface{}) {
	std.Info(i)
}

// Infof ...
func Infof(format string, args ...interface{}) {
	std.Infof(format, args...)
}

// Infoln ...
func Infoln(i ...interface{}) {
	std.Info(i)
}

// Warn ...
func Warn(i ...interface{}) {
	std.Warn(i)
}

// Warnf ...
func Warnf(format string, args ...interface{}) {
	std.Warnf(format, args...)
}

// Warnln ...
func Warnln(i ...interface{}) {
	std.Warnln(i)
}

// Error ...
func Error(i ...interface{}) {
	std.Error(i)
}

// Errorf ...
func Errorf(format string, args ...interface{}) {
	std.Errorf(format, args...)
}

// Errorln ...
func Errorln(i ...interface{}) {
	std.Errorln(i)
}
