package api

import (
	"encoding/json"
	"fmt"
)

type (
	// Request API 标准请求公共字段
	Request struct {
		RequestID     string `json:"RequestId"`
		Action        string `json:"Action"`
		AppID         uint64 `json:"AppId"`
		Uin           string `json:"Uin"`
		SubAccountUin string `json:"SubAccountUin"`
		ClientIP      string `json:"ClientIp"`
		APIModule     string `json:"ApiModule"`
		Region        string `json:"Region"`
		Token         string `json:"Token"`
		Version       string `json:"Version"`
		RequestSource string `json:"RequestSource"` // MC 和 API
		Language      string `json:"Language"`
	}

	// Error API 标准错误结构
	Error struct {
		Code    string `json:"Code"`
		Message string `json:"Message"`
	}

	// ResponseError API3标准错误返回
	// ResponseError struct {
	// 	Response struct {
	// 		Error     *Error `json:"Error"`
	// 		RequestID string `json:"RequestId"`
	// 	} `json:"Response"`
	// }

	// Response API3标准成功返回
	// Response struct {
	// 	Response struct {
	// 		RequestID string `json:"RequestId"`
	// 	} `json:"Response"`
	// }
)

// Error 实现 built-in#error
func (s Error) Error() string {
	return fmt.Sprintf("Code:%s Message:%s", s.Code, s.Message)
}

// NewError 创建错误信息，返回给调用方
func NewError(code Errorcode, format string, args ...interface{}) *Error {
	return &Error{
		Code:    code.message,
		Message: fmt.Sprintf(format, args...),
	}
}

// 工具函数：快速生成标准错误结构
func newAPIError(e *Error, requestID string) interface{} {
	r := new(struct {
		Response struct {
			Error     *Error `json:"Error"`
			RequestID string `json:"RequestId"`
		} `json:"Response"`
	})

	r.Response.Error = e
	r.Response.RequestID = requestID

	return r
}

// 工具函数：快速生成标准响应结构
// 利用 json --> map --> json 向结构中插入 RequestID
func newAPIResponse(response interface{}, requestID string) interface{} {
	m := map[string]interface{}{}

	bytes, err := json.Marshal(response)
	if err != nil {
		panic("response of struct can always be Marshaled. why here")
	}

	if err := json.Unmarshal(bytes, &m); err != nil {
		panic("struct can always be Unmarshaled into map. why here")
	}

	m["RequestId"] = requestID

	r := &struct {
		Response interface{} `json:"Response"`
	}{
		Response: m,
	}

	return r
}

// BoxingRequest 请求打包函数. 传递 API3 标准请求头
func BoxingRequest(c Context, version string, module string, action string, v interface{}) interface{} {
	m := map[string]interface{}{}

	// 利用 json 将结构体转换成 map
	bytes, err := json.Marshal(v)
	if err != nil {
		panic("response of struct can always be Marshaled. why here")
	}
	if err := json.Unmarshal(bytes, &m); err != nil {
		panic("struct can always be Unmarshaled into map. why here")
	}

	header := c.GetRequestHeader()

	m["RequestId"] = header.RequestID
	m["Action"] = action
	m["AppId"] = header.AppID
	m["Uin"] = header.Uin
	m["SubAccountUin"] = header.SubAccountUin
	m["ClientIp"] = header.ClientIP
	m["ApiModule"] = module
	m["Region"] = header.Region
	m["Token"] = header.Token
	m["Version"] = version
	m["RequestSource"] = header.RequestSource
	m["Language"] = header.Language

	return m
}

// UnboxingRequestStandard 解包 API3 标准请求头
// API3 组件使用： POST + body 的方式，路由信息存放在 body 中
// 业务组件通过 action，version 字段来路由请求
func UnboxingRequestStandard(data []byte) (*Request, *Error) {
	request := new(Request)

	if err := json.Unmarshal(data, request); err != nil {
		return nil, NewError(BadRequest, fmt.Sprintf("decode request error. %s", err))
	}

	return request, nil
}

// UnboxingResponse 响应的解包函数, 仅返回业务信息
func UnboxingResponse(data []byte, v interface{}) *Error {
	resp := new(struct {
		Response json.RawMessage `json:"Response"`
	})

	// 移除外层 Response 包装
	if err := json.Unmarshal(data, resp); err != nil {
		return NewError(BadResponse, fmt.Sprintf("response not json format, %s", err))
	}

	// 解析业务响应结构
	if err := json.Unmarshal(resp.Response, v); err != nil {
		return NewError(BadResponse, fmt.Sprintf("response not json format, %s", err))
	}

	return nil
}

// UnboxingResponseError 响应解包函数, 仅返回API3标准错误
func UnboxingResponseError(data []byte) *Error {
	resp := new(struct {
		Response struct {
			Error     *Error `json:"Error"`
			RequestID string `json:"RequestId"`
		} `json:"Response"`
	})

	if err := json.Unmarshal(data, resp); err != nil {
		return NewError(BadResponse, fmt.Sprintf("response not json format, %s", err))
	}

	return resp.Response.Error
}
