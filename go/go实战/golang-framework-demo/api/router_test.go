package api

import (
	"fmt"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

func RouterFunc() error {
	fmt.Println("func called")
	return nil
}

// TestRouter 测试基本路由接口
func TestRouter(t *testing.T) {
	r := new(router)

	obj := reflect.ValueOf(RouterFunc)
	r.Add("RouterFunc", "version", obj)

	funcObj := r.Find("RouterFunc", "version")
	// funcObj.Call(nil)

	assert.EqualValues(t, funcObj, obj)
}

func TestSupportFuncOrPanic(t *testing.T) {
	// <set up>

	// type func1 func(ctx Context, input *STRUCT) (err *Error
	t.Run("success", func(t *testing.T) {
		assert.NotPanics(t, func() {
			func1 := func(ctx Context, input *struct{}) (err *Error) { return nil }
			supportFuncOrPanic(func1)
		})
	})
	// type func2 func(ctx Context, input *STRUCT) (output *STRUCT, err *Error)
	t.Run("success", func(t *testing.T) {
		assert.NotPanics(t, func() {
			func1 := func(ctx Context, input *struct{}) (output *struct{}, err *Error) { return nil, nil }
			supportFuncOrPanic(func1)
		})
	})

	// panic: 输入参数个数不为 2
	t.Run("input_error", func(t *testing.T) {
		assert.Panics(t, func() {
			func1 := func(ctx Context, input *struct{}, third int) (err *Error) { return nil }
			supportFuncOrPanic(func1)
		})
	})
	// panic: 输入参数 1 不是 Context 类型
	t.Run("input_error", func(t *testing.T) {
		assert.Panics(t, func() {
			func1 := func(ctx *Context, input *struct{}) (err *Error) { return nil }
			supportFuncOrPanic(func1)
		})
	})
	// panic: 输入参数 2 不是 struct 指针类型
	t.Run("input_error", func(t *testing.T) {
		assert.Panics(t, func() {
			func1 := func(ctx Context, input struct{}) (err *Error) { return nil }
			supportFuncOrPanic(func1)
		})
	})

	// panic: 输出参数超过 2 个
	t.Run("output_error", func(t *testing.T) {
		assert.Panics(t, func() {
			func1 := func(ctx Context, input *struct{}) (other *struct{}, output *struct{}, err *Error) {
				return nil, nil, nil
			}
			supportFuncOrPanic(func1)
		})
	})
	// panic: 输出参数最后一个参数不是 *Error 类型
	t.Run("output_error", func(t *testing.T) {
		assert.Panics(t, func() {
			func1 := func(ctx Context, input *struct{}) (err Error) {
				return *NewError(Errorcode{}, "")
			}
			supportFuncOrPanic(func1)
		})
	})
	// panic: 输出参数最后一个参数不是 *Error 类型
	t.Run("output_error", func(t *testing.T) {
		assert.Panics(t, func() {
			func1 := func(ctx Context, input *struct{}) (output *struct{}, err Error) {
				return nil, *NewError(Errorcode{}, "")
			}
			supportFuncOrPanic(func1)
		})
	})
	// panic: 输出参数不是 *struct 类型
	t.Run("output_error", func(t *testing.T) {
		assert.Panics(t, func() {
			func1 := func(ctx Context, input *struct{}) (output struct{}, err *Error) {
				return struct{}{}, nil
			}
			supportFuncOrPanic(func1)
		})
	})
	// panic: 输出参数不是 *struct 类型
	t.Run("output_error", func(t *testing.T) {
		assert.Panics(t, func() {
			func1 := func(ctx Context, input *struct{}) (output map[string]interface{}, err *Error) {
				return make(map[string]interface{}), nil
			}
			supportFuncOrPanic(func1)
		})
	})
	// panic: 输出参数不是 *struct 类型
	t.Run("output_error", func(t *testing.T) {
		assert.Panics(t, func() {
			func1 := func(ctx Context, input *struct{}) (output int, err *Error) {
				return 0, nil
			}
			supportFuncOrPanic(func1)
		})
	})

	// <tear down>
}
