package api

import (
	"golang-framework/logger"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
)

type (
	// Context 持有请求上下文信息, 提供 context log 能力
	Context interface {
		logger.Logger
		GetLogger() logger.Logger
		GetEchoContext() echo.Context
		GetRequestHeader() *Request
		SetRequestHeader(*Request)
	}

	acontext struct {
		logger.Logger
		Server      *Server
		EchoContext echo.Context
	}
)

func newAcontext(s *Server, c echo.Context, e *logrus.Entry) Context {
	v := &acontext{
		Logger:      &logAdaptor{e},
		Server:      s,
		EchoContext: c,
	}
	return v
}

// GetLogger 提供 logger.Logger, 用于替换其它组件的日志输出
func (s *acontext) GetLogger() logger.Logger {
	return s
}

// GetEchoContext 返回底层 echo.Context 对象, 不建议使用
func (s *acontext) GetEchoContext() echo.Context {
	return s.EchoContext
}

// reqeustKey 用于在context中保存API标准头部
const requestKey string = "7611fc29e1d54e847dc477e3a4286a37cd74875f"

// GetRequestHeader 获取API标准请求头. 主要用于:client 填充后续请求
func (s *acontext) GetRequestHeader() *Request {
	return s.EchoContext.Get(requestKey).(*Request)
}

// SetRequestHeader 设置API标准请求头. 主要用于:框架向context中注入信息
func (s *acontext) SetRequestHeader(r *Request) {
	s.EchoContext.Set(requestKey, r)
	return
}
