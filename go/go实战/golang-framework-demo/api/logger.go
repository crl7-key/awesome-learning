package api

import (
	"encoding/json"
	"golang-framework/logger"
	"io"
	"strings"

	"github.com/labstack/echo/v4"
	echolog "github.com/labstack/gommon/log"
	"github.com/sirupsen/logrus"
)

func toLogrusLevel(level string) logrus.Level {

	switch strings.ToLower(strings.TrimSpace(level)) {
	case "err", "error":
		return logrus.ErrorLevel
	case "warn", "warning":
		return logrus.WarnLevel
	case "info":
		return logrus.InfoLevel
	case "debug":
		return logrus.DebugLevel
	default:
		return logrus.DebugLevel
	}
}

// logAdaptor 适配器, 引入 logrus 作为底层日志框架
// logrus.Logger 实例采用 mutex 将写请求穿行, 以此保证日志完整性.
// TCE 的 golang 框架将 echo框架日志 / api 框架日志 / 配置中心日志 都输出到相同文件,
// 从而, 一个应用中只能有一个日志实例.
// 实现上, 通过 api.server 持有 logrus 实例 && 控制实例属性, 并通过 logAdaptor 提供 echo.Logger 和 logger.Logger.
//
// 1. echo.Logger 接口适配, 用于 echo 框架的日志输出;
// 2. logger.Logger 接口适配, 用于 api框架 / 配置中心 以及周边框架;

type logAdaptor struct {
	*logrus.Entry
}

var _ echo.Logger = new(logAdaptor)
var _ logger.Logger = new(logAdaptor)

// WithField 添加字段, 结构化日志接口
func (l *logAdaptor) WithField(key string, value interface{}) logger.Logger {
	return &logAdaptor{l.Entry.WithField(key, value)}
}

// WithFields 添加字段, 结构化日志接口
func (l *logAdaptor) WithFields(fields logger.Fields) logger.Logger {
	return &logAdaptor{l.Entry.WithFields(logrus.Fields(fields))}
}

// Output 兼容 echo.Logger 接口, 无实际作用
func (l *logAdaptor) Output() io.Writer {
	return l.Logger.Out
}

// SetOutput 兼容 echo.Logger 接口, 无实际作用
func (l *logAdaptor) SetOutput(w io.Writer) {
	return
}

// Prefix 兼容 echo.Logger 接口, 无实际作用
func (l *logAdaptor) Prefix() string {
	return ""
}

// SetPrefix 兼容 echo.Logger 接口, 无实际作用
func (l *logAdaptor) SetPrefix(p string) {
	return
}

// SetHeader 兼容 echo.Logger 接口, 无实际作用
func (l *logAdaptor) SetHeader(h string) {
	return
}

// SetLevel 兼容 echo.Logger 接口, 无实际作用
func (l *logAdaptor) Level() echolog.Lvl {
	return echolog.OFF
}

// SetLevel 兼容 echo.Logger 接口, 无实际作用
func (l *logAdaptor) SetLevel(v echolog.Lvl) {
	return
}

// Printj 兼容 echo.Logger 接口
func (l *logAdaptor) Printj(j echolog.JSON) {
	b, err := json.Marshal(j)
	if err != nil {
		panic(err)
	}
	l.Print(string(b))
}

// Debugj 兼容 echo.Logger 接口
func (l *logAdaptor) Debugj(j echolog.JSON) {
	b, err := json.Marshal(j)
	if err != nil {
		panic(err)
	}
	l.Debug(string(b))
}

// Infoj 兼容 echo.Logger 接口
func (l *logAdaptor) Infoj(j echolog.JSON) {
	b, err := json.Marshal(j)
	if err != nil {
		panic(err)
	}
	l.Info(string(b))
}

// Warnj 兼容 echo.Logger 接口
func (l *logAdaptor) Warnj(j echolog.JSON) {
	b, err := json.Marshal(j)
	if err != nil {
		panic(err)
	}
	l.Warn(string(b))
}

// Errorj 兼容 echo.Logger 接口
func (l *logAdaptor) Errorj(j echolog.JSON) {
	b, err := json.Marshal(j)
	if err != nil {
		panic(err)
	}
	l.Error(string(b))
}

// Fatalj 兼容 echo.Logger 接口
func (l *logAdaptor) Fatalj(j echolog.JSON) {
	b, err := json.Marshal(j)
	if err != nil {
		panic(err)
	}
	l.Fatal(string(b))
}

// Panicj 兼容 echo.Logger 接口
func (l *logAdaptor) Panicj(j echolog.JSON) {
	b, err := json.Marshal(j)
	if err != nil {
		panic(err)
	}
	l.Panic(string(b))
}
