package api

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"reflect"

	"github.com/labstack/echo/v4"
)

// NoRequestID RequestID 占位符，用于响应请求。表示：无法从请求中获取ReqeustID
const NoRequestID string = "unknown-request-id"

func handler(s *Server, c echo.Context) error {
	body, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		return c.JSON(http.StatusBadRequest, "count read request body")
	}

	return c.JSON(http.StatusOK, callRequestHandler(s, c, body))
}

func callRequestHandler(s *Server, c echo.Context, body []byte) interface{} {
	r, err := UnboxingRequestStandard(body)
	if err != nil {
		s.Logger.Errorf("%s", err)
		return err
	}

	if r.RequestID == "" {
		return newAPIError(NewError(BadRequest, "count find requestid param"), NoRequestID)
	}
	if r.Action == "" {
		return newAPIError(NewError(BadRequest, "count find action param"), r.RequestID)
	}
	if r.Version == "" {
		return newAPIError(NewError(BadRequest, "count find action param"), r.RequestID)
	}

	handler := globalRouter.Find(r.Action, r.Version)
	if !handler.IsValid() {
		s.Logger.Error("count find handler for action: ", r.Action)
		return newAPIError(NewError(NoActionHandler, fmt.Sprint("count find handler for action ", r.Action)), r.RequestID)
	}

	input := reflect.New(handler.Type().In(1).Elem()).Interface()
	if err := json.Unmarshal([]byte(body), input); err != nil {
		return newAPIError(NewError(BadRequest, fmt.Sprintf("count decode json reqeust. %s", err)), r.RequestID)
	}

	ctx := newAcontext(s, c, s.Logger.WithField("RequestID", r.RequestID))
	ctx.SetRequestHeader(r)
	rets := handler.Call([]reflect.Value{reflect.ValueOf(ctx), reflect.ValueOf(input)})

	var funcRsp, funcErr reflect.Value
	if handler.Type().NumOut() == 2 {
		funcRsp = rets[0]
		funcErr = rets[1]
	} else {
		funcRsp = reflect.ValueOf(nil)
		funcErr = rets[0]
	}

	var response interface{}
	if !funcErr.IsNil() {
		response = newAPIError(funcErr.Interface().(*Error), r.RequestID)
	} else {
		response = newAPIResponse(funcRsp.Interface(), r.RequestID)
	}

	return response
}
