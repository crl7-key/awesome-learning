package api

import (
	"reflect"
	"runtime"
	"strings"

	"github.com/labstack/gommon/log"
)

var globalRouter router

// route 接口路由表项
type route struct {
	action  string
	funcObj reflect.Value
}

// router 调度器
type router struct {
	routeMap map[string]map[string]*route
}

// Add 添加路由
func (s *router) Add(action string, version string, funcObj reflect.Value) {
	if s.routeMap == nil {
		s.routeMap = make(map[string]map[string]*route)
	}
	if _, ok := s.routeMap[version]; !ok {
		s.routeMap[version] = make(map[string]*route)
	}
	s.routeMap[version][action] = &route{action: action, funcObj: funcObj}
}

// Find 查找路由
func (s *router) Find(action string, version string) (funcObj reflect.Value) {
	if s.routeMap != nil {
		if _, ok := s.routeMap[version]; ok {
			if _, ok := s.routeMap[version][action]; ok {
				return s.routeMap[version][action].funcObj
			}
		}
	}
	return reflect.ValueOf(nil)
}

// 检查函数类型是否被框架支持
func supportFuncOrPanic(funcObj interface{}) {
	help := `

RegisterAction and RegisterActionWithName support only two function signature

type func1 func(ctx Context, input *STRUCT) (err *Error)
type func2 func(ctx Context, input *STRUCT) (output *STRUCT, err *Error)

STRUCE: pointer to user-defined struct`

	// struct 指针
	structPointerOrPanic := func(t reflect.Type) {
		if t.Kind() != reflect.Ptr || t.Elem().Kind() != reflect.Struct {
			panic(help)
		}
	}

	if reflect.TypeOf(funcObj).Kind() != reflect.Func {
		panic("funcObj should be func")
	}

	funcType := reflect.TypeOf(funcObj)

	// 检查入参类型
	if funcType.NumIn() != 2 {
		panic(help)
	}
	if funcType.In(0) != reflect.TypeOf((*Context)(nil)).Elem() {
		panic(help)
	}
	structPointerOrPanic(funcType.In(1))

	// 检查出参
	switch funcType.NumOut() {
	case 2:
		structPointerOrPanic(funcType.Out(0))
		if funcType.Out(1) != reflect.TypeOf(&Error{}) {
			panic(help)
		}
	case 1:
		if funcType.Out(0) != reflect.TypeOf(&Error{}) {
			panic(help)
		}
	default:
		panic(help)
	}
}

// RegisterAction 提取函数名作为 Action 名称， 并调用 RegisterActionWithName 注册接口
func RegisterAction(version string, funcObj interface{}) {
	supportFuncOrPanic(funcObj)
	funcFullName := runtime.FuncForPC(reflect.ValueOf(funcObj).Pointer()).Name()
	funcNameArr := strings.Split(funcFullName, ".")
	funcName := funcNameArr[len(funcNameArr)-1]
	RegisterActionWithName(funcName, version, funcObj)
}

// RegisterActionWithName 为 Action 绑定处理函数
// RegisterAction and RegisterActionWithName support only two function signature
// - type func1 func(ctx Context, input *STRUCT) (err *Error)
// - type func2 func(ctx Context, input *STRUCT) (output *STRUCT, err *Error)
// - *注意*：STRUCE: pointer to user-defined struct
func RegisterActionWithName(action string, version string, funcObj interface{}) {
	supportFuncOrPanic(funcObj)
	funcValue := reflect.ValueOf(funcObj)
	globalRouter.Add(action, version, funcValue)
	log.Infof("register action:%s version:%s", action, version)
}
