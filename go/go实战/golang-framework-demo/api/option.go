package api

import (
	"strings"
)

// Options 服务实例配置选项
type Options struct {
	// Path http 资源路径
	Path string
	// Graceful 优雅地停止服务
	Graceful bool
	// Address 服务地址
	Address struct {
		IP   string
		Port int
	}
	// Log 日志参数
	Log struct {
		Path  string
		Name  string
		Level string
	}
}

// Option ...
type Option func(*Options)

// SetPath 设置 http 资源路径
func SetPath(path string) Option {
	return func(options *Options) {
		options.Path = path
	}
}

// Graceful 设置优雅地停止服务
func Graceful() Option {
	return func(options *Options) {
		options.Graceful = true
	}
}

// SetAddress 设置服务地址
func SetAddress(ip string, port int) Option {
	return func(options *Options) {
		options.Address.IP = ip
		options.Address.Port = port
	}
}

// SetLog 设置日志属性
func SetLog(path string, name string, level string) Option {
	return func(options *Options) {
		options.Log.Path = path
		options.Log.Name = name
		options.Log.Level = strings.ToLower(level)
	}
}
