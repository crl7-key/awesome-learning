package api

import (
	"context"
	"fmt"
	"golang-framework/logger"
	"os"
	"os/signal"
	"path/filepath"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
	"github.com/sirupsen/logrus"
)

// Server ...
type Server struct {
	Echo    *echo.Echo
	Options *Options

	Logger *logrus.Logger
}

// NewServer 返回标准 API 实例
func NewServer(opts ...Option) *Server {

	// 默认参数设置
	options := &Options{}
	options.Log.Path = "."
	options.Log.Name = "api.log"
	options.Log.Level = "debug"

	for _, o := range opts {
		o(options)
	}

	return newServer(options)
}

func newServer(options *Options) *Server {
	s := &Server{
		Echo:    echo.New(),
		Options: options,
	}

	file, err := os.OpenFile(filepath.Join(options.Log.Path, options.Log.Name), os.O_RDWR|os.O_CREATE, os.ModePerm)
	if err != nil {
		panic(fmt.Errorf("open log file error. %s", err))
	}

	s.Logger = &logrus.Logger{
		Out: file,
		Formatter: &logrus.TextFormatter{
			FullTimestamp: true,
		},
		Level: toLogrusLevel(options.Log.Level),
	}
	s.Echo.Logger = &logAdaptor{logrus.NewEntry(s.Logger)}

	// 为外部组件提供日志能力
	logger.SetLogger(&logAdaptor{logrus.NewEntry(s.Logger)})

	return s
}

// Run 启动 http 服务
func (s *Server) Run() {
	s.Echo.POST(s.Options.Path, func(c echo.Context) error {
		return handler(s, c)
	})

	if !s.Options.Graceful {
		log.Fatal(s.Echo.Start(fmt.Sprintf("%s:%d", s.Options.Address.IP, s.Options.Address.Port)))
	} else {
		// Graceful Shutdown
		// Start server
		go func() {
			if err := s.Echo.Start(fmt.Sprintf("%s:%d", s.Options.Address.IP, s.Options.Address.Port)); err != nil {
				log.Errorf("Shutting down the server with error:%v", err)
			}
		}()

		// Wait for interrupt signal to gracefully shutdown the server with
		// a timeout of 10 seconds.
		quit := make(chan os.Signal)
		signal.Notify(quit, os.Interrupt)
		<-quit
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()
		if err := s.Echo.Shutdown(ctx); err != nil {
			log.Fatal(err)
		}
	}

	return
}
