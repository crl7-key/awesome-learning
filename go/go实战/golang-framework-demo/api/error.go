package api

import "fmt"

var (
	// BadRequest 无法解析请求信息
	BadRequest = CreateParameterError("BadReqeust")
	// BadResponse 无法解析响应信息
	BadResponse = CreateParameterError("BadResponse")
	// NoActionHandler Action 未绑定处理函数
	NoActionHandler = CreateParameterError("NoActionHandlerFound")
)

// Errorcode 二级错误码包装. 通过 CreateXXXXError() 函数创建
type Errorcode struct {
	message string
}

// CreateInternalError 创建二级错误码：内部错误
func CreateInternalError(second string) Errorcode {
	return Errorcode{message: fmt.Sprintf("%s.%s", "InternalError", second)}
}

// CreateParameterError 创建二级错误码：参数错误
func CreateParameterError(second string) Errorcode {
	return Errorcode{message: fmt.Sprintf("%s.%s", "ParameterError", second)}
}

// CreateAuthorizationError 创建二级错误码:授权错误
func CreateAuthorizationError(second string) Errorcode {
	return Errorcode{message: fmt.Sprintf("%s.%s", "AuthorizationError", second)}
}

// CreateOperationError 创建二级错误码：操作错误
func CreateOperationError(second string) Errorcode {
	return Errorcode{message: fmt.Sprintf("%s.%s", "OperationError", second)}
}

// CreateResourceError 创建二级错误码：资源错误
func CreateResourceError(second string, message string) Errorcode {
	return Errorcode{message: fmt.Sprintf("%s.%s", "ResourceError", second)}
}
