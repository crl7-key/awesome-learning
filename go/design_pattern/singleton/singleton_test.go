package singleton

import (
	"fmt"
	"sync"
	"testing"
)

type singleton map[string]string

var (
	once     sync.Once
	instance singleton
)

func GetSingleInstance() singleton {
	once.Do(func() {
		instance = make(singleton)
	})
	return instance
}

func TestSingleton(t *testing.T) {
	s := GetSingleInstance()

	s["this"] = "that"

	s2 := GetSingleInstance()
	fmt.Println("s1 is : ", s)
	fmt.Println("s2 This is ", s2["this"])
}
