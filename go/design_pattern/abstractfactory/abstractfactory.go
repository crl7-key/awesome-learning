package abstractfactory

import (
	"fmt"
)

// AbstractProductA 抽象产品接口A
type AbstractProductA interface {
	Show()
}

// AbstractProductB 抽象产品接口B
type AbstractProductB interface {
	Show()
}

// AbstractFactory 抽象模式工厂接口
type AbstractFactory interface {
	CreateProductA() AbstractProductA
	CreateProductB() AbstractProductB
}

// -------------------产品A

// ConcreteProudctA1 具体产品A1
type ConcreteProudctA1 struct {
}

func (c *ConcreteProudctA1) Show() {
	fmt.Println("ConcreteProudctA1")
}

// ConcreteProudctA2 具体产品A2
type ConcreteProudctA2 struct {
}

func (c *ConcreteProudctA2) Show() {
	fmt.Println("ConcreteProudctA2")
}

// -------------------产品B

// ConcreteProudctB1 具体产品B1
type ConcreteProudctB1 struct {
}

func (c *ConcreteProudctB1) Show() {
	fmt.Println("ConcreteProudctB1")
}

// ConcreteProudctB2 具体产品B2
type ConcreteProudctB2 struct {
}

func (c *ConcreteProudctB2) Show() {
	fmt.Println("ConcreteProudctB2")
}

// Factory1 具体工厂1
type Factory1 struct{}

func (f *Factory1) CreateProductA() AbstractProductA {
	return new(ConcreteProudctA1)
}

func (f *Factory1) CreateProductB() AbstractProductB {
	return new(ConcreteProudctB1)
}

// Factory2 具体工厂2
type Factory2 struct{}

func (f *Factory2) CreateProductA() AbstractProductA {
	return new(ConcreteProudctA2)
}

func (f *Factory2) CreateProductB() AbstractProductB {
	return new(ConcreteProudctB2)
}
