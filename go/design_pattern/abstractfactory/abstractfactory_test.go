package abstractfactory

import "testing"

func TestAbstractFactory(t *testing.T) {
	var factory AbstractFactory
	var p1 AbstractProductA
	var p2 AbstractProductB
	factory = new(Factory1)
	p1 = factory.CreateProductA()
	p2 = factory.CreateProductB()
	p1.Show()
	p2.Show()

	factory = new(Factory2)
	p1 = factory.CreateProductA()
	p2 = factory.CreateProductB()
	p1.Show()
	p2.Show()
}
