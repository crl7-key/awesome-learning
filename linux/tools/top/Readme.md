## linux 监控网络IO、磁盘、CPU、内存


- CPU：vmstat ，sar -u，top
- 磁盘IO：iostat -xd，sar -d，top
- 网络IO：iftop -n，ifstat，dstat -nt，sar -n DEV 2 3
- 磁盘容量：df -h 
- 内存使用：free –m，top

- 查看什么进程占用端口：  netstat -antp | fgrep <port>

- 查看进程资源：
  - jps -l
  - jmap -heap 21046


### ps aux
`ps`命令用于查看系统中的进程状态

```
USER       PID     %CPU    %MEM     VSZ           RSS              TTY     STAT    START    TIME        COMMAND
进程的     进程   运算器   内存占  虚拟内存使用量  占用的固定内存量  所在    进程    被启动  实际使用    命令名称与参数
所有者     ID号   占用率   用率    (单位是KB)      (单位是KB)       终端    状态    的时间   CPU的时间
root         1     0.0     0.2   167948           5772              ?       Ss     Feb14   0:31       /sbin/init noibrs
root         2     0.0     0.0        0              0              ?       S      Feb14   0:00       [kthreadd]
root         3     0.0     0.0        0              0              ?       S      Feb14   0:32       [ksoftirqd/0]
root         5     0.0     0.0        0              0              ?       S<     Feb14   0:00       [kworker/0:0H]
root         7     0.0     0.0        0              0              ?       S      Feb14   9:50       [rcu_sched]
```

USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
- `SER`，进程所有者的用户名。

- `PID`，进程号，可以唯一标识该进程。

- `%CPU`，进程自最近一次刷新以来所占用的CPU时间和总时间的百分比。

- `%MEM`，进程使用内存的百分比。

- `VSZ`，进程使用的虚拟内存大小，以K为单位。

- `RSS`，进程占用的物理内存的总数量，以K为单位。

- `TTY`，进程相关的终端名。

- `STAT`，进程状态，用(`R`--运行或准备运行；`S`--睡眠状态；`I`--空闲；`Z`--冻结；`D`--不间断睡眠；`W`-进程没有驻留页；`T`停止或跟踪。)这些字母来表示。

- `START`，进程开始运行时间。

- `TIME`，进程使用的总CPU时间。

- `COMMAND`，被执行的命令行。

## Top
`top`命令是`Linux`下常用的性能分析工具，能够实时显示系统中各个进程的资源占用状况，类似于`Windows`的任务管理器。

```
top - 15:48:26 up 86 days,  7:31,  2 users,  load average: 0.06, 0.03, 0.00
Tasks:  79 total,   2 running,  77 sleeping,   0 stopped,   0 zombie
%Cpu(s):  0.3 us,  0.0 sy,  0.0 ni, 99.7 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
KiB Mem :  1969336 total,   503448 free,   418056 used,  1047832 buff/cache
KiB Swap:        0 total,        0 free,        0 used.  1384420 avail Mem 

PID USER      PR  NI    VIRT    RES    SHR S %CPU %MEM     TIME+ COMMAND                                                                                         
713 mysql     20   0 1283976 349580   8396 S  0.3 17.8  98:05.39 mysqld                                                                                   
 1 root      20   0  167948   5772   3640 S  0.0  0.3   0:31.95 systemd                                               
 2 root      20   0       0      0      0 S  0.0  0.0   0:00.06 kthreadd                                              
 3 root      20   0       0      0      0 S  0.0  0.0   0:32.92 ksoftirqd/0                                           
 5 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 kworker/0:0H                                          
 7 root      20   0       0      0      0 R  0.0  0.0   9:50.49 rcu_sched                                             
 8 root      20   0       0      0      0 S  0.0  0.0   0:00.00 rcu_bh            
```



上半部分显示了整体系统负载情：
- `top`一行：从左到右依次为当前系统时间，系统运行的时间，系统在之前`1min`、`5min`和`15min`内`cpu`的平均负载值

- `Tasks`一行：该行给出进程整体的统计信息，包括统计周期内进程总数、运行状态进程数、休眠状态进程数、停止状态进程数和僵死状态进程数

- `Cpu(s)`一行：cpu整体统计信息，包括用户态下进程、系统态下进程占用`cpu`时间比，`nice`值大于`0`的进程在用户态下占用`cpu`时间比，`cpu`处于`idle`状态、`wait`状态的时间比，以及处理硬中断、软中断的时间比

- `Mem`一行：该行提供了内存统计信息，包括物理内存总量、已用内存、空闲内存以及用作缓冲区的内存量

- `Swap`一行：虚存统计信息，包括交换空间总量、已用交换区大小、空闲交换区大小以及用作缓存的交换空间大小


下半部分显示了各个进程的运行情况：
- `PID`: 进程pid

- `USER`: 拉起进程的用户

- `PR`: 该列值加`100`为进程优先级，若优先级小于`100`，则该进程为实时(`real-time`)进程，否则为普通(`normal`)进程，实时进程的优先级更高，更容易获得`cpu`调度，以上输出结果中，`java`进程优先级为`120`，是普通进程，`had`进程优先级为`2`，为实时进程，`migration` 进程的优先级RT对应于`0`，为最高优先级

- `NI`: 进程的`nice`优先级值，该列中，实时进程的`nice`值为`0`，普通进程的`nice`值范围为`-20~19`

- `VIRT`: 进程所占虚拟内存大小（默认单位`kB`）

- `RES`: 进程所占物理内存大小（默认单位`kB`）

- `SHR`: 进程所占共享内存大小（默认单位`kB`）

- `S`: 进程的运行状态

- `%CPU`: 采样周期内进程所占`cpu`百分比

- `%MEM`: 采样周期内进程所占内存百分比

- `TIME+`: 进程使用的`cpu`时间总计

- `COMMAND`: 拉起进程的命令



