
## awk 命令

## 目录
* [简介](#简介)
* [特殊要点](#特殊要点)
* [print](#print--0)
* [-f指定脚本文件](#-f指定脚本文件)
* [-F指定分隔符](#-f指定分隔符)
* [if语句](#if语句)
* [条件表达式](#条件表达式)
* [逻辑运算符](#逻辑运算符)
* [数值运算](#数值运算)
* [输出分隔符ofs](#输出分隔符ofs)
* [输出处理结果到文件](#输出处理结果到文件)
* [格式化输出](#格式化输出)
* [while语句](#while语句)
* [for语句](#for语句)
* [break语句](#break语句)
* [数组](#数组)
* [应用](#应用)

### 简介
- `awk`是行处理器: 相比较屏幕处理的优点，在处理庞大文件时不会出现内存溢出或是处理缓慢的问题，通常用来格式化文本信息

- awk处理过程: 依次对每一行进行处理，然后输出

- awk命令形式:
```shell
awk [-F|-f|-v] ‘BEGIN{} //{command1; command2} END{}’ file
```
- `[-F|-f|-v]` :   大参数，`-F`指定分隔符，`-f`调用脚本，`-v`定义变量 `var=value`

- `' '` :         引用代码块

- `BEGIN`:   初始化代码块，在对每一行进行处理之前，初始化代码，主要是引用全局变量，设置FS分隔符

- `//` :          匹配代码块，可以是字符串或正则表达式

- `{}`:           命令代码块，包含一条或多条命令

- `;`:          多条命令使用分号分隔

- `END`:      结尾代码块，在对每一行进行处理之后再执行的代码块，主要是进行最终计算或输出结尾摘要信息

## 特殊要点:

- `$0`: 表示整个当前行

- `$1`: 每行第一个字段,即第一列

- `NF`: 字段数量变量

- `NR`: 每行的记录号，多文件记录递增

- `FNR`: 与NR类似，不过多文件记录不递增，每个文件都从1开始

- `\t`:  制表符

- `\n`: 换行符

- `FS`: `BEGIN`时定义分隔符

- `RS`: 输入的记录分隔符， 默认为换行符(即文本是按一行一行输入)

- `~`: 匹配，与`==`相比不是精确比较

- `!~`:  不匹配，不精确比较

- `==` :等于，必须全部相等，精确比较

- `!=` : 不等于，精确比较

- `&&`:逻辑与

- `||`:逻辑或

- `+`: 匹配时表示1个或1个以上

- `/[0-9][0-9]+/`: 两个或两个以上数字

- `/[0-9][0-9]*/` :一个或一个以上数字

- `FILENAME`: 文件名

- `OFS`: 输出字段分隔符， 默认也是空格，可以改为制表符等

- `ORS`: 输出的记录分隔符，默认为换行符,即处理结果也是一行一行输出到屏幕

- `-F'[:#/]'`:定义三个分隔符

### print & $0

`print` 是`awk`打印指定内容的主要命令

例如: 打印`/etc/passwd`的内容:
```shell
awk '{print}'  /etc/passwd   ==   awk '{print $0}'  /etc/passwd  
```
不输出`passwd`的内容，而是输出相同个数的空行，进一步解释了`awk`是一行一行处理文本:
```sh
awk '{print " "}' /etc/passwd
```                                        
输出相同个数的`a`行，一行只有一个`a`字母:
 ```sh
 awk '{print "a"}'   /etc/passwd
 ```                                      
只输出每一行的第第一个字段,分行输出:
```sh
awk -F":" '{print $1}'  /etc/passwd
```

将每一行的前二个字段，分行输出，进一步理解一行一行处理文本:
```sh
awk -F: '{print $1; print $2}'   /etc/passwd
```                 
输出字段1,3,6，以制表符作为分隔符:
```sh
awk  -F: '{print $1,$3,$6}' OFS="\t" /etc/passwd 
```


 

 ## -f指定脚本文件
```sh
awk -f script.awk  file
```
 `script.awk`中内容:
```awk
BEGIN{
FS=":"

}

{print $1}    
```
效果与`awk -F":" '{print $1}' `相同,只是分隔符使用`FS`在代码自身中指定


寻找空白行:
```sh
awk 'BEGIN{X=0} /^$/{ X+=1 } END{print "I find",X,"blank lines."}' test 
```

计算文件大小:
```sh
ls -l|awk 'BEGIN{sum=0} !/^d/{sum+=$5} END{print "total size is",sum}'                   
```


 

### -F指定分隔符

- `$1`: 指指定分隔符后，第一个字段，`$3`第三个字段，`\t`是制表符

一个或多个连续的空格或制表符看做一个定界符，即多个空格看做一个空格
```sh
awk -F":" '{print $1}'  /etc/passwd
```

`$1`与`$3`相连输出，不分隔:
```sh
awk -F":" '{print $1 $3}'  /etc/passwd                      
```
`$1`与`$3`使用空格分隔:
```sh
awk -F":" '{print $1,$3}'  /etc/passwd                      
```
`$1`与`$3`之间手动添加空格分隔:
```sh
awk -F":" '{print $1 " " $3}'  /etc/passwd                  
```
自定义输出:
```sh
awk -F":" '{print "Username:" $1 "\t\t Uid:" $3 }' /etc/passwd     
```
显示每行有多少字段:
```sh
awk -F: '{print NF}' /etc/passwd                               
```
将每行第`NF`个字段的值打印出来:
```sh
awk -F: '{print $NF}' /etc/passwd                              
```
显示只有`4`个字段的行:
```sh
awk -F: 'NF==4 {print }' /etc/passwd                     
```
显示每行字段数量大于`2`的行
```sh
awk -F: 'NF>2{print $0}' /etc/passwd                       
```
输出每行的行号:
```sh
awk '{print NR,$0}' /etc/passwd                                 
```
依次打印行号，字段数，最后字段值，制表符，每行内容
```sh
awk -F: '{print NR,NF,$NF,"\t",$0}' /etc/passwd      
```
显示第`5`行:
```sh
awk -F: 'NR==5{print}'  /etc/passwd                         
```
显示第`5`行和第`6`行:
```sh
awk -F: 'NR==5 || NR==6{print}'  /etc/passwd     
```
不显示第一行
```sh
route -n|awk 'NR!=1{print}'                                      
```
 

 ### 匹配代码块

-  `//`纯字符匹配  `!//`纯字符不匹配  `~//`字段值匹配   `!~//`字段值不匹配  `~/a1|a2/`字段值匹配`a1`或`a2`  

 输出匹配`mysql`的行
```sh
awk '/mysql/' /etc/passwd
```
```sh
awk '/mysql/{print }' /etc/passwd
```
```sh
awk '/mysql/{print $0}' /etc/passwd                   
```
 以上三条指令结果一样
 
 输出不匹配`mysql`的行:
 ```sh
awk '!/mysql/{print $0}' /etc/passwd                  
```
```sh
awk '/mysql|mail/{print}' /etc/passwd
```
```sh
awk '!/mysql|mail/{print}' /etc/passwd
```

区间匹配:
```sh
awk -F: '/mail/,/mysql/{print}' /etc/passwd         
```
匹配包含`27`为数字开头的行，如27，277，2777...:
```sh
awk '/[2][7][7]*/{print $0}' /etc/passwd               
```
`$1`匹配指定内容才显示:
```sh
awk -F: '$1~/mail/{print $1}' /etc/passwd         
```
与上面相同
```sh
awk -F: '{if($1~/mail/) print $1}' /etc/passwd     
```
不匹配`$1`指定内容
```sh
awk -F: '$1!~/mail/{print $1}' /etc/passwd          
```

```sh
awk -F: '$1!~/mail|mysql/{print $1}' /etc/passwd        
```
 

### IF语句

- 必须用在`{}`中，且比较内容用`()`扩起来
简写:
```sh
awk -F: '{if($1~/mail/) print $1}' /etc/passwd                                       
```
全写:
```sh
awk -F: '{if($1~/mail/) {print $1}}'  /etc/passwd                                  
```

`if...else...`:
```sh
awk -F: '{if($1~/mail/) {print $1} else {print $2}}' /etc/passwd            
```
 

 

### 条件表达式
```
==   !=   >   >=  
```
过滤第一列等于`mysql`的行:
```sh
awk -F":" '$1=="mysql"{print $3}' /etc/passwd  
```
与上面相同:
```sh
awk -F":" '{if($1=="mysql") print $3}' /etc/passwd          
```
不等于:
```sh
awk -F":" '$1!="mysql"{print $3}' /etc/passwd                 
```
大于:
```sh
awk -F":" '$3>1000{print $3}' /etc/passwd                      
```
大于等于:
```
awk -F":" '$3>=100{print $3}' /etc/passwd                     
```
小于:
```sh
awk -F":" '$3<1{print $3}' /etc/passwd                           
```
小于等于:
```sh
awk -F":" '$3<=1{print $3}' /etc/passwd                        
```
 

 ###  逻辑运算符
```
&&　|| 
```
逻辑与， `$1`匹配`mail`，并且`$3`>`8`:
```sh
awk -F: '$1~/mail/ && $3>8 {print }' /etc/passwd         
```

```sh
awk -F: '{if($1~/mail/ && $3>8) print }' /etc/passwd
```
逻辑或:
```sh
awk -F: '$1~/mail/ || $3>1000 {print }' /etc/passwd       
```

```sh
awk -F: '{if($1~/mail/ || $3>1000) print }' /etc/passwd 
```
 

 ### 数值运算
```sh
awk -F: '$3 > 100' /etc/passwd    
```
```sh
awk -F: '$3 > 100 || $3 < 5' /etc/passwd  
```
```sh
awk -F: '$3+$4 > 200' /etc/passwd
```
第三个字段加`10`打印 :
```sh
awk -F: '/mysql|mail/{print $3+10}' /etc/passwd                    
```
减法:
```sh
awk -F: '/mysql/{print $3-$4}' /etc/passwd                             
```
求乘积:
```sh
awk -F: '/mysql/{print $3*$4}' /etc/passwd                             
```
除法:
```sh
awk '/MemFree/{print $2/1024}' /proc/meminfo                 
```
取整:
```sh
awk '/MemFree/{print int($2/1024)}' /proc/meminfo           
```
 

 ### 输出分隔符OFS
```sh
awk '$6 ~ /FIN/ || NR==1 {print NR,$4,$5,$6}' OFS="\t" netstat.txt
```

输出字段`6`匹配`WAIT`的行，其中输出每行行号，字段`4`，`5`,`6`，并使用制表符分割字段:
```sh
awk '$6 ~ /WAIT/ || NR==1 {print NR,$4,$5,$6}' OFS="\t" netstat.txt        
```
 

### 输出处理结果到文件

- 在命令代码块中直接输出    
```sh
route -n|awk 'NR!=1{print > "./fs"}'   
```

 -  使用重定向进行输出    
 ```sh
 route -n|awk 'NR!=1{print}'  > ./fs
```
 

 ### 格式化输出
```sh
netstat -anp|awk '{printf "%-8s %-8s %-10s\n",$1,$2,$3}' 
```

-  `printf`表示格式输出

- `%`格式化输出分隔符

-  `-8`长度为`8`个字符

-  `s`表示字符串类型

打印每行前三个字段，指定第一个字段输出字符串类型(长度为 `8`)，第二个字段输出字符串类型(长度为`8`),

第三个字段输出字符串类型(长度为`10`)
```sh
netstat -anp|awk '$6=="LISTEN" || NR==1 {printf "%-10s %-10s %-10s \n",$1,$2,$3}'
```
```sh
netstat -anp|awk '$6=="LISTEN" || NR==1 {printf "%-3s %-10s %-10s %-10s \n",NR,$1,$2,$3}'
```
 

### IF语句
如果每行第三列大于`100`则打印`large`,否则打印`samll`:
```sh
awk -F: '{if($3>100) print "large"; else print "small"}' /etc/passwd
```
如果每行第三列大于`100`则打印`large`并且`A`加`1`,否则打印`samll`, `B`加`1`;最后打印`A`和`B`的值:
```sh
awk -F: 'BEGIN{A=0;B=0} {if($3>100) {A++; print "large"} else {B++; print "small"}} END{print A,"\t",B}' /etc/passwd 
```
小于`100`跳过，否则显示:
```sh
awk -F: '{if($3<100) next; else print}' /etc/passwd                        
```

另一种形式
```sh
awk -F: '{print ($3>100 ? "yes":"no")}'  /etc/passwd 
```
```sh
awk -F: '{print ($3>100 ? $3":\tyes":$3":\tno")}'  /etc/passwd
```
 

### while语句
使用`While`循环输出数字`1`到`5`:
```sh
 awk 'BEGIN {i = 1; while (i < 6) { print i; ++i } }'
```
 
### For语句
使用`For`循环输出数字`1`至`5`:
```sh
awk 'BEGIN { for (i = 1; i <= 5; ++i) print i }'
```


### Continue 
例子输出`1`到`20`之间的偶数：
```sh
awk 'BEGIN {for (i = 1; i <= 20; ++i) {if (i % 2 == 1) continue ; else print i} }'
```

### Break语句
当计算的和大于`50`的时候使用`break`结束循环:
```sh
awk 'BEGIN {i = 1; while (i < 6) { print i; ++i } }'
```


###  数组

```sh
netstat -anp|awk 'NR!=1{a[$6]++} END{for (i in a) print i,"\t",a[i]}'
```
```sh
netstat -anp|awk 'NR!=1{a[$6]++} END{for (i in a) printf "%-20s %-10s %-5s \n", i,"\t",a[i]}'
```
## 应用

### 应用1:
输出文件每行有多少字段
```sh
awk -F: '{print NF}' filename
```
输出前`5`个字段:
```sh
awk -F: '{print $1,$2,$3,$4,$5}' filename
```

输出前`5`个字段并使用制表符分隔输出:
```
awk -F: '{print $1,$2,$3,$4,$5}' OFS='\t' filename 
```

制表符分隔输出前`5`个字段，并打印行号:
```sh
awk -F: '{print NR,$1,$2,$3,$4,$5}' OFS='\t'  filename  
```

### 应用2
指定多个分隔符:`#`，输出每行多少字段
```sh
awk -F'[:#]' '{print NF}'  filename                                                  
```
制表符分隔输出多字段:
```sh
awk -F'[:#]' '{print $1,$2,$3,$4,$5,$6,$7}' OFS='\t' filename    
```

### 应用3
指定三个分隔符，并输出每行字段数:
```sh
awk -F'[:#/]' '{print NF}' filename                                               
```
制表符分隔输出多字段
```sh
awk -F'[:#/]' '{print $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12}' filename     
```

### 应用4
计算当前目录下，普通文件的大小，使用`KB`作为单位:
```sh
ls -l|awk 'BEGIN{sum=0} !/^d/{sum+=$5} END{print "total size is:",sum/1024,"KB"}'
```
取整:
```sh
ls -l|awk 'BEGIN{sum=0} !/^d/{sum+=$5} END{print "total size is:",int(sum/1024),"KB"}'        
```
`int`是取整的意思

 ### 应用5

统计`netstat -anp`状态为`LISTEN`和`CONNECT`的连接数量分别是多少:
```sh
netstat -anp|awk '$6~/LISTEN|CONNECTED/{sum[$6]++} END{for (i in sum) printf "%-10s %-6s %-3s \n", i," ",sum[i]}'
```

### 应用6

统计当前目录下不同用户的普通文件的总数是多少:
```sh
ls -l|awk 'NR!=1 && !/^d/{sum[$3]++} END{for (i in sum) printf "%-6s %-5s %-3s \n",i," ",sum[i]}'   
```
统计当前目录下不同用户的普通文件的大小总`size`是多少:
```sh
ls -l|awk 'NR!=1 && !/^d/{sum[$3]+=$5} END{for (i in sum) printf "%-6s %-5s %-3s %-2s \n",i," ",sum[i]/1024/1024,"MB"}'
```

### 应用7
输出成绩表:
```sh
awk 'BEGIN{math=0;eng=0;com=0;printf "Lineno.   Name    No.    Math   English   Computer    Total\n";printf "------------------------------------------------------------\n"}{math+=$3; eng+=$4; com+=$5;printf "%-8s %-7s %-7s %-7s %-9s %-10s %-7s \n",NR,$1,$2,$3,$4,$5,$3+$4+$5} END{printf "------------------------------------------------------------\n";printf "%-24s %-7s %-9s %-20s \n","Total:",math,eng,com;printf "%-24s %-7s %-9s %-20s \n","Avg:",math/NR,eng/NR,com/NR}' Transcript
```

其中`Transcript`内容如下:

```
Mary 2143 78 84 77
Jack 2321 66 78 45
Tom 2122 48 77 71
Mike 2537 87 97 95
Bob 2415 40 57 62
```

**[⬆ 返回顶部](#目录)**
