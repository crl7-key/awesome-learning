## 在`vim`中轻松阅读代码

有时候在`vim`中阅读代码十分的方便,特别是阅读`c/c++`代码。通过一定的配置,可以使`vim`具有`ide`的阅读体验。

### 安装`ctags`和`cscope`
第一步安装`vim`需要的工具。

`ctags`和`cscope`这是两个古老而又强大的为代码建立索引的工具,在`redhat`系列的`linux`系统下(例如:`redhat`,`centos`,`fedra`等)安装非常方便。

```bash
sudo yum install ctags
sudo yum install cscope
```
### 创建自己的插件函数
创建一些常用的插件函数方便以后的映射。这些函数包括：查看函数定义,查看函数调用情况,查找字符串出现的地方,等等。

在`/home/username/`下建立一个文件夹 `.vim/plugins` 或 `.vim/plugin`（`vim`的版本不同,目录不同,一般是在`.vim/plugin`下）,注意:是在用户的`home`目录下进行创建,而且是隐藏文件夹。

#### `cs_search_string.vim`
该文件的内容如下：
```vim
"cs search word 
func CS_Search_Word()
    let w = expand("<cword>")
    exe "cs f s" w
    exe "copen"
endfunc

" find the define
func CS_Search_define()
    let w = expand("<cword>")
    exe "cs f g" w
    exe "copen"
endfunc

" find call what funcs 
func CS_Search_whatfunc()
    let w = expand("<cword>")
    exe "cs f d" w
endfunc

" find called func
func CS_Search_calledfunc()
    let w = expand("<cword>")
    exe "cs f c" w
endfunc


" find where file the func is
func CS_Search_funcfiles()
    let w = expand("<cword>")
    exe "cs f i" w
endfunc
```

#### `search_word.vim`

```golang
"search word
func Search_Word()
    let w = expand("<cword>")
    exe "vimgrep" w "./**/*.c ./**/*.h"
    exe "copen"
endfunc
```
### 创建`.vimrc`文件
在用户自己的`home`目录下创建自己的`.vimrc`文件。
```vim
set nu
set tabstop=4
set et
set fo-=r
set noautoindent
syntax on
" 把光标放到一个函数上,按f5就可以打开该函数的定义窗口
map <F5> <C-w>]
" 按f6关闭定义窗口
map <F6> <C-w>c
" 按f4可以查看括号的匹配
map <F4> <S-%>
" 在插入模式下输入zz,即可退出编辑状态
imap zz <Esc>
set sw=4
"map <F2> :s/$/\=   strftime("    %Y-%m-%d %H:%M:%S")<Enter>
" 在光标所在行的末尾插入日期,这个可以按自己的需要去留
map <F2> :s/$/\=   strftime(" %Y-%m-%d")<Enter>
" 在项目级别查找单词所在的地方,并弹出窗口
map <F9> :call Search_Word()<CR>
" 按<查看前一项内容
map < :cprevious<CR>
" 按>查看后一项内容
map > :cnext<CR>
" 只是查看函数被调用的地方,而不是字符串搜素
map <F7> :call CS_Search_Word()<CR>
" 关闭弹出窗口
map <F8> :cclose<CR>

if has("cscope")
        set csprg=/usr/bin/cscope
        set csto=0
        set cst
        set nocsverb
        " add any database in current directory
        if filereadable("./cscope.out")
            cs add ./cscope.out
        " else add database pointed to by environment
        elseif $CSCOPE_DB != ""
        "if $CSCOPE_DB != ""
            cs add $CSCOPE_DB
        endif
        set csverb
endif

set cscopequickfix=e-,i-,s-,t-
" 查看函数被调用的地方
map <F11> :call CS_Search_calledfunc()<CR>
" 查看函数定义
map <F12> :call CS_Search_whatfunc()<CR>
hi Comment ctermfg=6

set nocompatible
"filetype indent on
"set autoindent
"set ic
set hls 
"set lbr 
colorscheme delek
```

### 创建建立索引命令
需要建立索引时可以通过`ctags`命令和`cscope`命令单独建立,这里可以写了一个脚本,例如该脚本叫：`ctcs`。脚本创建完成后,把脚本放到`/usr/local/bin/`下即可。
```bash
#!/bin/bash
#
# ctcs

star()
{
    ctags -R *   
    if [ $? -eq 0 ]; then
        echo "ctags successfully!"
    fi  
    cscope -qbR 
    if [ $? -eq 0 ]; then
        echo "cscope successfully!"
    fi  
}

del()
{
    if [ -f tags -o -f cscope.out -o -f cscope.po.out -o -f cscope.in.out ]; then
        rm -f tags  && echo "clean tags ok!"
        rm -f cscope.* && echo "clean cscope.* files ok!"
    fi  
}

case "$1" in
    -r) 
        del 
        star
        ;;  
    -d) 
        del 
        ;;  
    *)  
        echo "usage : ctcs -r|-d"   
        exit 1
esac

exit 0
```

### 使用方法
- 进入项目源码根目录

- 执行`ctcs -r`命令

- 使用`vim`打开源码:
```bash
vim xxx.c
```
- 把光标放到一个函数上,按`f7`试一试是否能看到函数被调用的列表
```bash
cd nginx
ctcs -r
vim src/core/nginx.c
```
