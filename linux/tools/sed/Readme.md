## Linux Sed命令
## 目录
* [简介](#简介)
* [删除](#删除)
* [替换](#替换)
* [内容的插入](#内容的插入)
* [文本的打印](#文本的打印)

### 简介
`sed`: 是一种在线编辑器，它一次处理一行内容。
处理时，把当前处理的行存储在临时缓冲区中,称为“模式空间”（`pattern space`）,接着用`sed`命令处理缓冲区中的内容，处理完成后，把缓冲区的内容送往屏幕。接着处理下一行，这样不断重复，直到文件末尾。
文件内容并没有改变，除非你使用重定向存储输出。`Sed`通常用来自动编辑一个或多个文件;简化对文件的反复操作;编写转换程序等.

- `sed`的处理过程: 逐行读取文件内容,读到匹配的行就根据指令做操作,不匹配就跳过。

- `sed`总是以行对输入进行处理

- `sed`处理的不是原文件而是原文件的拷贝

- `sed`命令形式:
```sh
sed [-hnV][-e<script>][-f<script file>][filename]
```
参数说明:
- `-e<script>`或`--expression=<script>` 将其后跟的脚本命令添加到已有的命令中。

- `-f<script文件>`或`--file=<script文件> `将其后文件中的脚本命令添加到已有的命令中。

-  `-h`或`--help` 显示帮助。

 - `-n`或`--quiet`或`--silent` 仅显示`script`处理后的结果。

 - `-V`或`--version` 显示版本信息。

动作说明：

- `a` ：向匹配行后面插入内容

- `c` ：更改匹配行的内容

- `d` ：删除,删除匹配的内容

- `i` ：向匹配行前插入内容

- `p` ：打印出匹配的内容,通常 p 会与参数 sed -n 一起使用

- `s` ：替换掉匹配的内容

### 删除
删除第一行:
```sh
sed -e '1d' filename
```
删除第x行:
```sh
sed -e 'xd' filename
```

删除第x1,x2,x3行:
```sh
sed -e 'x1d' -e 'x2d' -e 'x3d' filename
```
删除第一到第三行:
```sh
sed -e '1,3d' filename
```
删除第`n`行到第`m`行?也就是:
```sh
 sed -e 'n,md' filename
```
 删除第一行到最后一行:
 ```sh
 sed -e '1,$d' filename
 ```
 删除含有`'#'`号的行:
 ```sh
 sed -e '/#/d' filename
 ```
 删除含有字母`xx`的行:
 ```sh
 sed -e '/xx/d' filename
 ```
 删除除含有字符串`xx`的所有行:
 ```sh
 sed -e '/xx/!d' filename
 ```
 删除从含有单词`word1`到含有单词`word2`的行:
 ```sh
 sed -e '/word1/, /word2/d' filename
 ```
 删除文件中从第`10`行到含有`word1`的行:
 ```10
 sed -e '10,/word1/d' filename
 ```
 删除从含有`word1`的行到第`10`行:
 ```sh
 sed -e '/word1/,10/d' filename
 ```
 删除含有两个`t`的行:
 ```sh
 sed -e '/t.*t/d' filename
 ```
 
 ### 替换   
 `Sed` 可替换文件中的字串、资料行、甚至资料区。其中,表示替换字串的指令中的函数参数为`s`;表示替换资料行、或资料区>的指令中的函数参数为`c`
 
 #### 行的替换
 把第一行替换成`/#!/bin/test`
 ```sh
 sed -e '1c/#!/bin/test' filename 
 ```
 把第`n`行替换成`test`:
 ```sh
 sed -e 'nctest' filename
 ```
 把`1`到`10`行替换成一行:`test`
 ```sh
 sed -e '1,10c test' filename
 ```
 换成两行:
 ```
 sed -e '1,10c test01 \ntest02' filename
 ```
 
 
 ### 内容的插入
 在`#`字符的前面插入一行`words`:

 ```sh
 sed -e '/#/i words' filename 
 ```
 在第一行前加一行`words`:
 ```sh
 sed -e '1i words' filename
 ```
 含有`unix`的行后添加"test":
 ```sh
 sed -e '/unix/a/ test' filename
 ```
 在第一行后添加`test`字符:
 ```sh
 sed -e '1 a/ hh' filename
 ```
 ### 文本的打印:
 
 打印所有行并重复打印含有`then`的行:
 ```
 sed -e '/then/ p' filename
 ```
 只打印含有`then`的行:
 ```sh
 sed -n '/then/ p' filename
 ```
 打印所有行并重复`1-3`行:
 ```sh
 sed -e '1,3 p' filename
 ```
 打印`1-3`行:
 ```sh
 sed -n '1,3 p' filename
 ```
 打印字符`if`和`fi`之间的内容:
 ```
 sed -n '/if/,/fi/ p' filename
 ```
 打印奇数行:
 ```sh
 sed -n 'p;n' filename
 ```
 ```sh
 sed -n '1~2p' filename
 ```
 打印偶数行:
 ```sh
 sed -n 'n;p' filename
 ```
 ```sh
 sed -n '2~2p' filename
 ```
 
 
**[⬆ 返回顶部](#目录)**
