#  大小端概念
`Big-Endian`和`Little-Endian`的定义如下：

- `Little-Endian`(小端) : 数据的低位字节位存放在内存的低地址端,高位字节存放在内存的高地址端。

- `Big-Endian`(大端) : 数据的高位字节位存放在内存的低地址端,低位字节存放在内存的高地址端。

## 实例分析
比如十六进制数字`0x12345678`,它总共占`4`个字节（`1`个字节`8`位,`2`个`16`进制占`8`位,所以`1`个字节最大表示`0xFF`）。这个数据分别在大小端模式下的内存存储布局为：

大端模式:

低地址 -----------------> 高地址

`0x12 | 0x34 | 0x56 | 0x78`




小端模式:

低地址 ------------------> 高地址

`0x78 | 0x56 | 0x34 | 0x12`

## 程序判断大小端
```C++
bool IsLittleEndian() {
    int a = 0x1234;
    char c = *(char *)&a;
    if (c == 0x34) {
        return true;
    }
    return false;
}
```

## 常用系统的大小端
目前`Intel`的`80x86`系列芯片是唯一还在坚持使用小端的芯片（`windows`系统就是基于该架构）,`ARM`芯片默认采用小端,但可以切换为大端。另外对于大小端的处理也和编译器的实现有关,在`C`语言中默认是小端（但在一些对于单片机的实现中却是基于大端,比如`Keil 51C`）,`Java`是平台无关的默认是大端。在网络上传输数据普遍采用的都是大端模式。


