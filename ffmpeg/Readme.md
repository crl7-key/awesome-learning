#  :balloon:`ffmpeg`

## 目录
* [介绍](#介绍)  
  * [什么是视频?](#什么是视频)
  * [什么是音频?](#什么是音频)
  * [编解码 - 压缩/解压数据](#编解码---压缩解压数据)
  * [容器 - 整合音视频的地方](#容器---整合音视频的地方)
* [ `FFmpeg` - 命令行](#ffmpeg---命令行)
  * [`ffmpeg`命令行工具](#ffmpeg命令行工具) 
* [通用的视频操作](#通用的视频操作)
  * [转码](#转码)
  * [转封装](#转封装)
  * [转码率](#转码率)
  * [转分辨率](#转分辨率)
  * [自适应流](#自适应流)

### 介绍
#### 什么是视频?

如果在一个特定的时间内播放一组图片（比如每秒`24`张图片）,将有一个运动错觉. 总而言之这是视频的基础概念: **一组图片/特定运行速率的帧**

#### 什么是音频?

声音是指**压力波通过空气或者任何其他介质（例如气体、液体或者固体）传播的震动**

在数字音频系统中,麦克风将声音转换为模拟电信号,然后通常使用**脉冲编码调制（`PCM`）的模数转换器（`ADC`）将模拟信号转换为数字信号**。

#### 编解码 - 压缩/解压数据

`CODEC`是一种压缩或解压缩数字音频/视频的电子软件。 它将原始（未压缩的）数字音频/视频转换为压缩格式,反之亦然。

如果我们打包数百万张图片到一个电影时,我们会获得一个很大的文件.

例如:

假如创建一个`1080x1920` (高x宽)的视频,每个像素有`3 bytes`,每秒`24`帧(每秒播放`24`张图片，这些图片给我们 `16,777,216`种不同的颜色),视频时长为`30`分钟。

```c
toppf = 1080 * 1920 // 每帧所有的像素点 (高 x 宽)
cpp = 3             // 每个像素的大小 bytes
tis = 30 * 60       // 时长 秒
fps = 24            // 每秒帧数

required_storage = tis * fps * toppf * cpp
```
这个视频需要大约`250.28G`的存储空间,`1.11Gbps`的带宽播放才能不卡顿。这也是为什么需要编解码的原因。

#### 容器 - 整合音视频的地方

容器或者封装格式描述了不同的数据元素和元数据是如何结合一起的

**一个文件包含了所有的流**（有音频和视频）,并且也提供了**音视频同步和通用元数据同步的方式**,比如标题、分辨率等等。

通常可以通过文件的后缀来判断文件格式:比如`video.webm`是一个`webm`容器格式。


### `FFmpeg` - 命令行

> 一个完整的并且跨平台的解决方法,包括对音视频流的转换等。

可以使用一个非常好的工具`FFmpeg`去播放多媒体文件:

命令行`ffmpeg`是一个简单而强大的二进制可执行程序。例如,下面的命令可以转换一个`mp4`格式到`avi`的格式:
```bash
ffmpeg -i input.mp4 output.avi
```
以上仅仅做了重新封装,把一个容器转换为另外一个容器。`FFmpeg`也可以做一些编解码的工作

#### `FFmpeg`命令行工具 

`FFmpeg`有一个非常完善的[文档](https://www.ffmpeg.org/ffmpeg.html)来说明它如何使用和工作的。
`FFmpeg`命令行格式如下:
```bash
ffmpeg <global-options> <input-options> -i <input> <output-options> <output>
```

为了更简单的理解,`FFmpeg`命令行格式如下:
```bash
ffmpeg {1} {2} -i {3} {4} {5}:
```
1. 全局参数

2. 输入参数

3. 输入内容

4. 输出选项

5. 输出内容

选项 `2`、`3`、`4`、`5`可以可以根据自己的需求添加参数。 下面有一个非常好理解的示例：
```bash
# WARNING: 这个文件大约 300MB
wget -O bunny_1080p_60fps.mp4 http://distribution.bbb3d.renderfarming.net/video/mp4/bbb_sunflower_1080p_60fps_normal.mp4

ffmpeg \
-y \ # 全局参数
-c:a libfdk_aac -c:v libx264 \ # 输入选项
-i bunny_1080p_60fps.mp4 \ # 输入内容
-c:v libvpx-vp9 -c:a libvorbis \ # 输出选项
bunny_1080p_60fps_vp9.webm # 输出内容
```
这个命令行作用是转换一个`mp4`文件（包含了`aac`格式的音频流,`h264`编码格式的视频流）,将它转换为`webm`,并且改变了音视频的编码格式。

可以简化命令行,因为`FFmpeg`会猜测你的意图。例如仅仅输入`ffmpeg -i input.avi output.mp4`,`FFmpeg`意图要编码为 `output.mp4`

### 通用的视频操作
当对音视频做编解码的时候,其实会做一系列的操作。
#### 转码
将一个视频流或者音频流从一个编码格式转换到另一个格式

有时候有些设备（`TV`,智能手机,游戏机等等）不支持`X` ,但是支持`Y`和一些更新的编码方式,这些方式能提供更好的压缩比

例如:

转换 `H264`（`AVC`）到 `H265`（`HEVC`）
```bash
ffmpeg \
-i bunny_1080p_60fps.mp4 \
-c:v libx265 \
bunny_1080p_60fps_h265.mp4
```

#### 转封装
将视频的从某一格式（容器）转换成另外一个

有时候有些设备（`TV`,智能手机,游戏机等等）不支持`X` ,但是支持`Y`和一些新的容器提供了更现代的特征

例如:

转换一个`mp4`为 `webm`
```bash
$ ffmpeg \
-i bunny_1080p_60fps.mp4 \
-c copy \ # 告诉ffmpeg跳过编解码的过程
bunny_1080p_60fps.webm
```

#### 转码率
改变视频码率

人们尝试用手机`2G`的网络来观看视频,也有用`4K`的电视来观看视频,需要提供不同的码率来满足不同的需求

例如:

生成视频码率在`3856k`和`964k`之间浮动。
```bash
ffmpeg \
-i bunny_1080p_60fps.mp4 \
-minrate 964K -maxrate 3856K -bufsize 2000K \
bunny_1080p_60fps_transrating_964_3856.mp4
```
通常使用改变码率和改变大小来做编解码

#### 转分辨率
将视频从一个分辨率转为不同的分辨率。

例如:

从`1080p`转成`480p`
```bash
ffmpeg \
-i bunny_1080p_60fps.mp4 \
-vf scale=480:-1 \
bunny_1080p_60fps_transsizing_480.mp4
```

#### 自适应流
生成很多不同分辨率的视频,并且把视频切分成块文件,最终通过`http`来分发不同分辨率的视频块。

为了提供一个更加灵活的观看体验在不同的终端和网络环境,比如用智能手机或者`4K`电视都能轻松的调整码率观看。

例如:

用`DASH`创建一个自适应的`WebM`。
```bash
# 视频流
$ ffmpeg -i bunny_1080p_60fps.mp4 -c:v libvpx-vp9 -s 160x90 -b:v 250k -keyint_min 150 -g 150 -an -f webm -dash 1 video_160x90_250k.webm

$ ffmpeg -i bunny_1080p_60fps.mp4 -c:v libvpx-vp9 -s 320x180 -b:v 500k -keyint_min 150 -g 150 -an -f webm -dash 1 video_320x180_500k.webm

$ ffmpeg -i bunny_1080p_60fps.mp4 -c:v libvpx-vp9 -s 640x360 -b:v 750k -keyint_min 150 -g 150 -an -f webm -dash 1 video_640x360_750k.webm

$ ffmpeg -i bunny_1080p_60fps.mp4 -c:v libvpx-vp9 -s 640x360 -b:v 1000k -keyint_min 150 -g 150 -an -f webm -dash 1 video_640x360_1000k.webm

$ ffmpeg -i bunny_1080p_60fps.mp4 -c:v libvpx-vp9 -s 1280x720 -b:v 1500k -keyint_min 150 -g 150 -an -f webm -dash 1 video_1280x720_1500k.webm

# 音频流
$ ffmpeg -i bunny_1080p_60fps.mp4 -c:a libvorbis -b:a 128k -vn -f webm -dash 1 audio_128k.webm

# DASH 格式
$ ffmpeg \
 -f webm_dash_manifest -i video_160x90_250k.webm \
 -f webm_dash_manifest -i video_320x180_500k.webm \
 -f webm_dash_manifest -i video_640x360_750k.webm \
 -f webm_dash_manifest -i video_640x360_1000k.webm \
 -f webm_dash_manifest -i video_1280x720_500k.webm \
 -f webm_dash_manifest -i audio_128k.webm \
 -c copy -map 0 -map 1 -map 2 -map 3 -map 4 -map 5 \
 -f webm_dash_manifest \
 -adaptation_sets "id=0,streams=0,1,2,3,4 id=1,streams=5" \
 manifest.mpd
```


**[⬆ 返回顶部](#目录)**
