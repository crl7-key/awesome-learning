## Dos常用指令

## 目录
* [目录操作指令](#目录操作指令)
* [文件操作指令](#文件操作指令)
* [其它指令](#其它指令)

`DOS`：`Disk Operating System`，磁盘操作系统，即`Windows`目录结构

## 目录操作指令

- `dir` : 查看当前目录

- `cd 盘符`: 切换到其它盘符
例如:切换至`E`盘
```dos
cd /d E:
```

- `cd ..` : 切换至上一级目录

- `cd \:`: 切换至根目录

- `md (make directory) `: 新建目录
例如:
新建多个目录
```dos
md test01 test02
```

- `rd` : 删除目录
带询问删除目录及下面的子目录和文件:
```dos
rd /s test02
```

## 文件操作指令
- 新建一个空文件
```
type nul>test.txt
```
- 新建或追加内容到文件
```
echo test > test.txt
```
- 显示文件内容
```
type test.txt
```
- 复制文件并重命名文件名
```
copy test.txt e:\test02.txt
```
- 移动文件
```
move e:\test02.txt d:\
```
- 删除指定文件
```
del e:\test02
```
- 删除指定目录中`txt`结尾所有文件
```
del *.txt
```

## 其它指令
- 清屏
```
cls
```
- 退出`dos`
```
exit
```
**[⬆ 返回顶部](#目录)**
