## 内存地址和堆栈

## 目录
* [内存地址](#内存地址)
   * [寻址公式一：[立即数]](#寻址公式一立即数)
   * [寻址公式二：[寄存器]](#寻址公式二寄存器)
   * [寻址公式三：[reg+立即数]](#寻址公式三reg立即数)
   * [寻址公式四：[reg+reg*{1,2,4,8}]](#寻址公式四regreg1248)
   * [寻址公式五：[reg+reg*{1,2,4,8}+立即数]](#寻址公式五regreg1248立即数)


## 内存地址

`lea` 和 `mov`的区别：`lea`是将源操作数的地址传递给目标操作数；`mov`是将源操作数的值传递给目标操作数；

### 寻址公式一：[立即数]

```c
读取内存的值：

MOV EAX,DWORD PTR  DS:[0x13FFC4]

MOV EAX,DWORD PTR  DS:[0x13FFC8]

向内存中写入数据：

MOV DWORD PTR  DS:[0x13FFC4],eax

MOV DWORD PTR  DS:[0x13FFC8],ebx

获取内存编号：

LEA EAX,DWORD PTR  DS:[0X13FFC4]

LEA EAX,DWORD PTR  DS:[ESP+8]

```
### 寻址公式二：[寄存器]
```c
reg代表寄存器  可以是8个通用寄存器中的任意一个

读取内存的值：

MOV ECX,0x13FFD0

MOV EAX,DWORD PTR  DS:[ECX]

向内存中写入数据：

MOV EDX,0x13FFD8

MOV DWORD PTR  DS:[EDX],0x87654321

获取内存编号：

LEA EAX,DWORD PTR DS:[EDX]
```

### 寻址公式三：[reg+立即数]
```c
读取内存的值：

MOV ECX,0x13FFD0

MOV EAX,DWORD PTR  DS:[ECX+4]

向内存中写入数据：

MOV EDX,0x13FFD8

MOV DWORD PTR  DS:[EDX+0xC],0x87654321

获取内存编号：

LEA EAX,DWORD PTR DS:[EDX+4]
```

### 寻址公式四：[reg+reg*{1,2,4,8}]
```c
读取内存的值：

MOV EAX,13FFC4

MOV ECX,2

MOV EDX,DWORD PTR  DS:[EAX+ECX*4]

向内存中写入数据：

MOV EAX,13FFC4

MOV ECX,2

MOV DWORD PTR  DS:[EAX+ECX*4],87654321

获取内存编号：

LEA EAX,DWORD PTR  DS:[EAX+ECX*4]
```

## 寻址公式五：[reg+reg*{1,2,4,8}+立即数]
```c
读取内存的值：

MOV EAX,13FFC4

MOV ECX,2

MOV EDX,DWORD PTR  DS:[EAX+ECX*4+4]

向内存中写入数据：

MOV EAX,13FFC4

MOV ECX,2

MOV DWORD PTR  DS:[EAX+ECX*4+4],87654321

获取内存编号：

LEA EAX,DWORD PTR  DS:[EAX+ECX*4+2]
```

## 堆栈

### 堆栈是什么？
一块区域

用于：

临时存储一些数据，如果数量很少就放到寄存器中

### 堆栈需要具备的功能

能够记录存了多少数据

能够非常快速地找到某个数据

### 堆栈的优点
临时存储大量数据，便于查找

## 

- `BASE`,`TOP`是`2`个`32`位的通用寄存器，里面存储的是内存单元编号(内存地址).

- `BASE`里面存储了一个地址，记录的起始地址.

- `TOP`里面也存储了一个地址，记录的是结束的地址.

- 存入数据的时候，`TOP`的值减`4`(为方便演示，每次存取都是`4`个字节)

- 释放数据的时候，`TOP`的值加`4`(为方便演示，每次存取都是4个字节)

- 如果要读取中间的某个数据的时候可以通过`TOP`  或者 `BASE` 加上偏移的方式去读取

- 这种内存的读写方式有个学名：堆栈
