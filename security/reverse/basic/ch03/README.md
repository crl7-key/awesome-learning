## 通用寄存器和内存读写


## 目录
* [通用寄存器](#通用寄存器)
    * [32位通用寄存器](#32位通用寄存器)
    * [汇编的模式](#汇编的模式)
* [寄存器操作语法](#寄存器操作语法)
    * [1.`MOV`目标操作数, 源操作数](#1mov-目标操作数-源操作数)
    * [2.`ADD`目标操作数, 源操作数](#2add-目标操作数-源操作数)
    * [3.SUB 目标操作数, 源操作数](#3sub-目标操作数-源操作数)
    * [4.`AND` 目标操作数, 源操作数](#4and-目标操作数-源操作数)
    * [5.`OR` 目标操作数, 源操作数](#5or-目标操作数-源操作数)
    * [6.`XOR` 目标操作数, 源操作数](#6xor-目标操作数-源操作数)
    * [7.`NOT` 目标操作数](#7not-目标操作数)
    * [8.`LEA`指令](#8lea指令)
* [寄存器的知识](#寄存器的知识)
   * [寄存器与内存的区别](#寄存器与内存的区别)
   * [内存格式](#内存格式)
   * [从指定内存中写入/读取数据](#从指定内存中写入读取数据)
## 通用寄存器

含义：
- 通用寄存器是寄存器中的一种，有些寄存器是专用的，而没有指定专门用途的寄存器，就是通用寄存器

- 汇编中：不停的移动数据，在内存和寄存器之间，寄存器和寄存器之间，移动得越复杂，程序越复杂。如果弄清楚这些数据的流动，就可以知道程序的流程，逆向就是要知道数据如何移动的；


### 32位通用寄存器
`32`位通用寄存器的指定用途如下


- 数据寄存器

作用 : 主要用例保存操作数和运算结果等信息

| 寄存器 | 主要用途 | 编号 | 存储数据的范围 |
|:--:|:--:| :--:|:--:| 
| `EAX` | `累加器(accumulator)` | `0` |`0 - 0xFFFFFFFF` |
| `ECX` |`计数器(counter)` | `1` | `	0 - 0xFFFFFFFF` |
| `EDX` | `数据寄存器(Data register)	I/O指针` | `2` |`	0 - 0xFFFFFFFF` |
| `EBX` |`基址寄存器(Base register) DS段的数据指针` | `3` | `	0 - 0xFFFFFFFF` |
- 指针寄存器

作用 : 存放堆栈内存储单元的偏移量,用它们可实现存储操作数的寻址方式

| 寄存器 | 主要用途 | 编号 | 存储数据的范围 |
|:--:|:--:| :--:|:--:| 
| `ESP` | `堆栈指针(Stack pointer)	` | `4` |`	0 - 0xFFFFFFFF` |
| `EBP` |`基指针(Base pointer)SS段的数据指针	` | `5` | `	0 - 0xFFFFFFFF` |

- 变地寄存器

作用 : 主要用于存放存储单元在段内的偏移量,用它们可实现多种存储器操作数的寻址方式

| 寄存器 | 主要用途 | 编号 | 存储数据的范围 |
|:--:|:--:| :--:|:--:| 
| `ESI` | `字符串操作的源指针;SS段的数据指针` | `6` |`	0 - 0xFFFFFFFF` |
| `EDI` |`字符串操作的目标指针;ES段的数据指针` | `7` | `0 - 0xFFFFFFFF` |

- 段寄存器

作用 : 段寄存器是根据内存分段的管理模式而设置的,内存单元的物理地址由段寄存器的值和一个偏移量组合而成


| 寄存器 | 名称 |
|:--:|:--:|
| `CS` | `代码段;(Code Segment)` | 
| `DS` |`数据段;(Data Segment)` |
| `ES` |`附加段;(Extra Segment)` |
| `SS` |`堆栈段;(Stack Segment)` |
| `FS` |`附加段;(Extra Segment)` |
| `GS` |`附加码段;(Extra Segment)`|

 
-  指令指针寄存器

| 寄存器 | 用途 |
|:--:|:--:|
| `EIP` | `(Instrcution Poniter)  存放下次将要执行的指令在代码段的偏移量` | 


- 标志寄存器


| 寄存器 | 用途 |
|:--:|:--:|
| `CF` | `进制标识` | 
| `PF` | `奇偶标识` | 
| `AF` | `辅助进位标识` | 
| `ZF` | `零标识` | 
| `SF` | `符号标识` | 
| `OF` | `溢出标识` | 


### 汇编的模式
- `16`位汇编：实模式，`16`位处理器内的内部，最多可以处理存储的长度为`16`位。

- `32`位汇编：保护模式，`32`位处理器内的内部，最多可以处理存储的长度为`32`位。

- `64`位汇编：保护模式，`64`位处理器的内部，最多可以处理存储的长度位`64`位。



| 位数 | 通用寄存器 |扩展 |
|:--:|:--:|:--:|
| 16位通用寄存器 | `AL`、`BL`、 `CL`、`DL`、 `AH`、 `BH`、 `CH`、 `DH` |  | 
| 16位通用寄存器 | `AX`、`BX`、 `CX`、`DX`、 `SI`、 `DI`、 `BP`、 `SP` |  `R8W`、`R9W`、`R10W`、`R11W`、`R12W`、 `R13W`、`R14W`、`R15W` | 
| 32位通用寄存器 | `EAX`、`EBX`、`ECX`、`EDX`、`ESI`、`EDI`、`EBP`、`ESP` |  `R8D`、`R9D`、`R10D`、`R11D`、`R12D`、 `R13D`、`R14D`、`R15D` | 
| 64位通用寄存器 | `RAX`、`RBX`、`RCX`、`RDX`、`RSI`、`RDI`、`RBP`、`RSP` |  `R8`、`R9`、`R10`、`R11`、`R12`、 `R13`、`R14`、`R15` | 

基本执行环境

- `32`位 ：`8`个`32`位通用寄存器，标志寄存器`EFLAGS` ，指令指针寄存器`EIP`

- 64位：`16`个`64`位通用寄存器，标志寄存器`RFLAGS`，指令指针寄存器`RIP`

## 寄存器操作语法

### 1.`MOV` 目标操作数, 源操作数
作用：拷贝源操作数到目标操作数
```c
#include <stdio.h>

int main()
{
   unsigned char m8 = 0;
   short m16 = 0;
   int m32 = 0;

  // r:通用寄存器    m：内存    imm：立即数    
  // r8：8位通用寄存器    
  // m8：8位内存    
  // imm8：8位立即数  imm16: 16位立即数
   __asm {
      mov ah, 0x12                              // MOV r8, imm8
      mov ax, 0xabcf                            // MOV r16, imm16
      mov eax, 0x13579BDF                       // MOV r32, imm32

      mov al,ah                                 // MOV r/m8, r8
      mov m8,ah
      mov bx,ax                                 // MOV r/m16, r16
      mov m16,ax
      mov ebx,eax                               // MOV r/m32, r32
      mov m32,eax

      mov bl,al                                 // MOV r8, r/m8
      mov al,m8
      mov bx,ax                                 // MOV r16, r/m16
      mov ax,m16
      mov ebx,eax                               // MOV r32, r/m32
      mov eax,m32
   }
   return 0;
}  

```
>1.源操作数可以是立即数、通用寄存器、段寄存器、内存单元   
2.目标操作数可以是通用寄存器、段寄存器或者内存单元   
3.操作数的宽度必须一样   
4.源操作数和目标操作数不能同时为内存单元

### 2.`ADD` 目标操作数, 源操作数
作用：将源操作数值加到目标操作数上
```c
#include <stdio.h>

int main()
{
   unsigned char m8 = 5;
   short m16 = 4;
   int m32 = 3;

   // r:通用寄存器    m：内存    imm：立即数   
   // r8：8位通用寄存器    
   // m8：8位内存    
   // imm8：8位立即数
   __asm {
      add al, 0x10                          // ADD r/m8, imm8
      add m8, al
      add ax, 0x1000                        // ADD r/m16, imm16
      add m16, ax
      add eax, 0x10000000                   // ADD r/m32, imm32
      add m32, eax

      add ax, 0x10                          // ADD r/m16, imm8
      add m16, 0x10
      add eax, 0x10                         // ADD r/m32, imm8
      add m32, 0x10

      add ah, al                            // ADD r/m8, r8
      add m8, al
      add bx, ax                            // ADD r/m16, r16
      add m16, ax
      add ebx, eax                          // ADD r/m32, r32
      add m32, eax

      add bh, m8                            // ADD r8, r/m8
      add bx, m16                           // ADD r16, r/m16
      add ebx, m32                          // ADD r32, r/m32
   }
   return 0;
}
```
>1.目标操作数的宽度和源操作数的宽度必须一样，当源操作数为立即数时，可不一致。   
2.立即数作为源操作数，可以比目标数小，也可以比目标数大。超出的位丢失，缺少的位补0。

## 3.`SUB` 目标操作数, 源操作数
作用：目标操作数减去源操作数
```c
#include <stdio.h>

int main()
{
   unsigned char m8 = 5;
   short m16 = 4;
   int m32 = 3;

   // r:通用寄存器    m：内存    
   // imm：立即数    r8：8位通用寄存器    
   // m8：8位内存    imm8：8位立即数
   __asm {
      sub al, 0x10                          // SUB r/m8, imm8
      sub m8, al
      sub ax, 0x1000                        // SUB r/m16, imm16
      sub m16, ax
      sub eax, 0x10000000                   // SUB r/m32, imm32
      sub m32, eax

      sub ax, 0x10                          // SUB r/m16, imm8
      sub m16, 0x10
      sub eax, 0x10                         // SUB r/m32, imm8
      sub m32, 0x10

      sub ah, al                            // SUB r/m8, r8
      sub m8, al
      sub bx, ax                            // SUB r/m16, r16
      sub m16, ax
      sub ebx, eax                          // SUB r/m32, r32
      sub m32, eax

      sub bh, m8                            // SUB r8, r/m8
      sub bx, m16                           // SUB r16, r/m16
      sub ebx, m32                          // SUB r32, r/m32
       }
   return 0;
}
```
>1.目标操作数与源操作数的宽度一定要一致，立即数除外；   
2.立即数作为源操作数，可以比目标数小，也可以比目标数大。超出的位丢失，缺少的位补0

## 4.`AND` 目标操作数, 源操作数
作用：位运算`&`，将目标操作数和源操作数进行`&`运算，并将值存给目标操作数
```c
#include <stdio.h>

int main()
{
   unsigned char m8 = 5;
   short m16 = 4;
   int m32 = 3;

   // r:通用寄存器    m：内存    imm：立即数   
   // r8：8位通用寄存器    
   // m8：8位内存    
   // imm8：8位立即数
   __asm {
      mov eax,0xFFFFFFFF    

      and al, 0x11                          // AND r/m8, imm8
      and m8, al
      and ax, 0x1111                        // AND r/m16, imm16
      and m16, ax
      and eax, 0x11111111                   // AND r/m32, imm32
      and m32, eax

      /*源操作数为立即数时，其宽度可大于可小于目标操作数的宽度*/
      and ax, 0x10                          // AND r/m16, imm8
      and m16, 0x10
      and eax, 0x10                         // AND r/m32, imm8
      and m32, 0x10

      and ah, al                            // AND r/m8, r8
      and m8, al
      and bx, ax                            // AND r/m16, r16
      and m16, ax
      and ebx, eax                          //AND r/m32, r32
      and m32, eax

      and bh, m8                            // AND r8, r/m8
      and bx, m16                           // AND r16, r/m16
      and ebx, m32                          // AND r32, r/m32
     }
   return 0;
}
```

## 5.`OR` 目标操作数, 源操作数
作用：位运算`|`，将目标操作数和源操作数进行 `|` 运算，并将值存给目标操作数
```c
#include <stdio.h>

int main()
{
   unsigned char m8 = 5;
   short m16 = 4;
   int m32 = 3;

   // r:通用寄存器    m：内存    imm：立即数   
   // r8：8位通用寄存器    
   // m8：8位内存    
   // imm8：8位立即数
   __asm {
     mov eax, 0x00000000

     or al, 0x11                            // OR r/m8, imm8
     or m8, al
     or ax, 0x1111                          // OR r/m16, imm16
     or m16, ax
     or eax, 0x11111111                    // OR r/m32, imm32
     or m32, eax

     /*源操作数为立即数时，其宽度可大于可小于目标操作数的宽度*/
     or ax, 0x10                           // OR r/m16, imm8
     or m16, 0x10
     or eax, 0x10                          // OR r/m32, imm8
     or m32, 0x10
     or ax, 0x1000
     or m8, 0x1000

     or ah, al                             // OR r/m8, r8
     or m8, al
     or bx, ax                             // OR r/m16, r16
     or m16, ax
     or ebx, eax                          // OR r/m32, r32
     or m32, eax

     or bh, m8                           // OR r8, r/m8
     or bx, m16                          // OR r16, r/m16
     or ebx, m32                         // OR r32, r/m32
   }
   return 0;
} 
```
## 6.`XOR` 目标操作数, 源操作数
作用：位运算`^`，将目标操作数和源操作数进行 `^` 运算，并将值存给目标操作数
```c
#include <stdio.h>

int main()
{
   unsigned char m8 = 5;
   short m16 = 4;
   int m32 = 3;

   // r:通用寄存器    m：内存    imm：立即数   
   // r8：8位通用寄存器    
   // m8：8位内存    
   // imm8：8位立即数
   __asm {
     mov eax, 0x00000000

     xor al, 0x11                           // XOR r/m8, imm8
     xor m8, al
     xor ax, 0x1111                         // XOR r/m16, imm16
     xor m16, ax
     xor eax, 0x11111111                    // XOR r/m32, imm32
     xor m32, eax

     /*源操作数为立即数时，其宽度可大于可小于目标操作数的宽度*/
     xor ax, 0x10                           // XOR r/m16, imm8
     xor m16, 0x10
     xor eax, 0x10                          // XOR r/m32, imm8
     xor m32, 0x10
     xor ax, 0x1000
     xor m8, 0x1000

     xor ah, al                             // XOR r/m8, r8
     xor m8, al
     xor bx, ax                             // XOR r/m16, r16
     xor m16, ax
     xor ebx, eax                           // XOR r/m32, r32
     xor m32, eax

     xor bh, m8                             // XOR r8, r/m8
     xor bx, m16                            // XOR r16, r/m16
     xor ebx, m32                           // XOR r32, r/m32
   }
   return 0;
}
```

## 7.`NOT` 目标操作数
作用：对目标操作数进行取反
```c
#include <stdio.h>

int main()
{
   unsigned char m8 = 5;
   short m16 = 4;
   int m32 = 3;

   __asm {
      mov eax, 0x00000000

      not al                // NOT r/m8
      not m8
      not ax                // NOT r/m16
      not m16
      not eax               // NOT r/m32
      not m32
   }
   return 0;
}
```
## 8.`LEA`指令

`lea`:`Load Effective Address`，即装入有效地址的意思，它的操作数就是地址
```c
lea r32，dword ptr  ds:[内存编号(地址)]
```
将内存地址赋值给`32`位通用寄存器

**Tip**: `lea`是传址，`mov`是传值

## 寄存器的知识
## 寄存器与内存的区别

- 寄存器位于`CPU`内部，执行速度快，但成本高

- 内存速度相对较慢，但成本低，可以大量使用；

- 寄存器和内存并无本质区别，都是用于存储数据的容器，都是定宽的

- 基层其常用的有`8`个：`EAX`、`ECX`、`EDX`、`EBX`、`ESP`、`EBP`、`ESI`、`EDI`

- 计算机中的几个常用计量单位：`BYTE`、`WORD`、`DWORD`
   - `BYTE`　字节　   =　`8`(`BIT`)  = `1``BYTE`                 `1KB = `1024` `BYTE`  = `2`的`10`次方`BYTE`        
   - `WORD`  字      =   `16`(`BIT`)  = `2``BYTE`                 `1MB` = `1024`  `KB`    = `2`的`20`次方`BYTE`        
   - `DWORD` 双字    =   `32`(`BIT`)  = `4``BYTE`               `1GB` = `1024` `MB`    = `2`的`30`次方`BYTE`

### 内存
内存的数量特别庞大，无法每个内存单元都起一个名字，所以用编号来代替，我们称计算机`CPU`是`32`位或者`64`位，主要指的就是内存编号的宽度，而不是寄存器的宽度。

有很多书上说之所以叫`32`位计算机是因为寄存器的宽度是`32`位，是不准确的，因为还有很多寄存器是大于`32`位的

通常所说的`32`位计算机是指`CPU`字长为`32`位

计算机内存的每一个字节会有一个编号(即内存编号的单位是字节)
###  内存格式
|`0x00000000` | 
|:--:|
| `0x00000001` |
| `0x00000002` |
| `0x00000003` |
| `....` |
| `....` |
| `....` |
| `....` |
| `0xFFFFFFFF` |


`32`位计算机的编号最大是`32`位，也就是`32`个`1`  换成`16`进制为`FFFFFFFF`，也就是说，`32`位计算机内存寻址的最大范围是`FFFFFFFF+1`

内存的单位是字节，那内存中能存储的信息最多为：`FFFFFFFF+1` 字节 即`4G`，这也是为什么我们在一个`XP`的系统上面如果物理内存超过`4G`是没有意义的原因

#### 只要是`32`位的计算机，那么最多识别的内存为`4G`？

不是。可以通过打补丁，或者拓展操作系统，寻址方式是由操作系统决定的

- 每个内存单元的宽度为`8`

- `[编号]`称为地址，用`[]`来区分立即数和内存地址

- 地址的作用：当想从内存中读取数据或者想向内存中写入数据，首先应该找到要读、写的位置。就像写信要写地址一样。

### 从指定内存中写入/读取数据
只要是涉及到内存读写的，一定要指定内存的宽度

`mov` 读/写的数据宽度 `ptr` `ds`:`[地址]`,`XXXX`
```c
#include <stdio.h>

int main()
{   
   __asm {
      mov eax,esp                                   // 将esp的值移到eax
      mov dword ptr ds:[eax],0xabcdabcd             // 使用esp的值作为地址
      mov eax,dword ptr ds:[eax]
   }

   __asm {
      mov dword ptr ds:[0x012FF9AC],0xabcdabcd      // 此处报错，无法写入
      mov eax,dword ptr ds:[0x012FF9AC]             
   }
   return 0;
}
```

- `word` ：要读/写多少 此时是`32``bit`  (`byte` 字节 `8bit`  `word`字 `16bit`  `dword`双字 `32bit`)

- `ptr`： 表示后面是一个指针（即里面存的是一个地址，不是一个普通的数值）；

- `ds`：段寄存器  这里为数据段

- `0x0012FF34` 内存编号，必须是`32`位的，前面的`0`可以省略

注意：内存编号不要随便写，因为内存是有保护的，并不是所有的内存都可以直接读写(需要特别处理)


**[⬆ 返回顶部](#目录)**
